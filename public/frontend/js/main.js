$( document ).ready(function() {
    // Grouping List Home Page
    $('.group-type-item-list').click(function () {
        $(this).addClass('active-list').siblings().removeClass('active-list');
        $('.latest-adds-items').children().addClass('group-list-active');
    });
    // Grouping grid Home Page
    $('.group-type-item-grid').click(function () {
        $(this).addClass('active-list').siblings().removeClass('active-list');
        $('.latest-adds-items').children().removeClass('group-list-active');
    });

    //  Home Tabs
    $('.latest-adds-tabs-botton span').on('click', function(){
        $(this).addClass('activeTab').siblings().removeClass('activeTab');
        if ($(this).data('class') === 'all') {
            $('.latest-adds-items .shuffle-item').css('display', 'block');
        }else{
            $('.latest-adds-items .shuffle-item').css('display', 'none');
            $($(this).data('class')).css('display', 'block');
        }
    });
    // slider ad details
    $('.inner-slider').slick({
        centerMode: true,
        slidesToShow: 3,
        prevArrow:'.slider .ad-d-arrow-left',
        nextArrow:'.slider .ad-d-arrow-right',
        centerPadding: '160px',
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    centerMode: true,
                    centerPadding: '120px',
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '90px',
                    slidesToShow: 2
                }
            },
            {
                breakpoint: 576,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

});
<?php

return [
  'gcm' => [
      'priority' => 'normal',
      'dry_run' => false,
      'apiKey' => 'My_ApiKey',
  ],
  'fcm' => [
        'priority' => 'normal',
        'dry_run' => false,
        'apiKey' => 'AAAAqr32MbY:APA91bHE6_szC3qRMbjjjJ9W9Z0xspkmfzobulP72VW5D9ZYpUe9B18GT0qklx6XgmvB22G-xWjqJospaGBLeEuzHynyWh9RPamRCaICaxgA4FM-ADnDYa5AKiO09fOQpNxClqh9Mdc5',
  ],
  'apn' => [
      'certificate' => __DIR__ . '/iosCertificates/apns-dev-cert.pem',
      'passPhrase' => '1234', //Optional
      'passFile' => __DIR__ . '/iosCertificates/yourKey.pem', //Optional
      'dry_run' => true
  ]
];
<!DOCTYPE html>
@php  $lang = LaravelLocalization::getCurrentLocale();  @endphp
<html>
<head>
    <title>{{trans('frontend.Multisport')}}</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="HTML, CSS, JavaScript,Jquery,php">
    <meta name="description" content="Multisport">
    <meta name="author" content="Multisport">

    <!-- start css files-->

<link rel="shortcut icon" type="image/png" href="{{asset('frontend/images/logo.png')}}"/>
{{Html::style('frontend/css/thumbnail-slider.css')}}
{{Html::style('frontend/css/ninja-slider.css')}}


{{Html::style('frontend/css/font-awesome.min.css')}}
{{Html::style('frontend/css/animate.css')}}
{{Html::style('frontend/css/style.css')}}
{{Html::style('frontend/css/layerslider.css')}}

@if($lang == "en")
{{Html::style('frontend/css/style_en.css')}}
{{Html::style('frontend/css/bootstrap.min.css')}}
@else
    {{Html::style('frontend/css/bootstrap-arabic.min.css')}}
@endif
    <!-- end css files-->

</head>
<body class="content">

@yield('content')

<!-- start footer -->

<!-- end footer -->
<!-- copy right-->
<section class="copy-right">
    {{trans('frontend.copy_right_for')}}@2017
</section>

<!-- end copy right-->
{{ Html::script('frontend/js/thumbnail-slider.js') }}
{{ Html::script('frontend/js/ninja-slider.js') }}
{{ Html::script('frontend/js/jquery-1.12.2.min.js') }}
{{ Html::script('frontend/js/jquery.nicescroll.min.js') }}
{{ Html::script('frontend/js/greensock.js') }}
<!-- LayerSlider script files -->
{{ Html::script('frontend/js/layerslider.transitions.js') }}
{{ Html::script('frontend/js/layerslider.kreaturamedia.jquery.js') }}
{{ Html::script('frontend/js/wow.min.j') }}



<script> new WOW().init();</script>
<script src=""></script>
{{ Html::script('frontend/js/bootstrap-arabic.min.js') }}
<!-- Initializing the slider -->
<script>
    jQuery("#layerslider").layerSlider({
        pauseOnHover: false,
        skin: 'borderlessdark3d',
        hoverBottomNav: true,
        skinsPath: 'images/skins/'
    });
    // plugin scroll nice


    $(".exe .block").niceScroll();
    $(".block-match").niceScroll();
</script>
 @yield('js')

</body>
</html>
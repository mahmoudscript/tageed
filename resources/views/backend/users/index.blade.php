@extends('backend.app')

@section('page_title' , trans('backend.users'))

@section('breadcrumb')


         <li>
           <a href="{{route('users.index')}}">{{trans('backend.users')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.users')}}</span>
         </li>


@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp

@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
            <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
           {{--<link href="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />--}}

            <!-- END PAGE LEVEL PLUGINS -->
@endsection


@section('content')

  <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> {{trans('backend.users')}}</span>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">

                                                    <a href="{{route('users.create')}}" id="sample_editable_1_new" class="btn sbold green"> {{trans('backend.create_user')}}
                                                        <i class="fa fa-plus"></i>
                                                    </a>

                                                    <a  class="btn red" href="#form_modal2" data-toggle="modal" > {{trans('backend.send_msg_all')}}
                                                        <i class="fa fa-reply"></i>
                                                    </a>
                                                    {{--<a href="javascript:;" class="btn blue"> {{trans('backend.send_msg_city')}}--}}
                                                        {{--<i class="fa fa-reply"></i>--}}
                                                    {{--</a>--}}

                                               
                                            </div>




                                        </div>
                                    </div>


                                    <div id="form_modal2" class="modal fade" role="dialog" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                    <h4 class="modal-title">{{trans('backend.send_msg')}}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{url('sendmessage')}}" method="get" class="form-horizontal">

                                                        <div class="form-group">
                                                            <label class="control-label col-md-4">{{trans('backend.msg')}}</label>
                                                            <div class="col-md-8">
                                                                <textarea name="message" class="form-control input-large" placeholder="{{trans('backend.enter_your_msg')}}" size="16" type="text" value="" >
                                                                </textarea>
                                                            </div>
                                                        </div>
                                                       



                                                </div>
                                                <div class="modal-footer">
                                                    <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">{{trans('backend.close')}}</button>
                                                    <button class="btn green" >{{trans('backend.send')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>

                                                <th>#</th>
                                                <th>{{trans('backend.user_name')}} </th>


                                                {{--<th>{{trans('backend.user_name_en')}} </th>--}}


                                                <th>{{trans('backend.user_email')}} </th>


                                                <th>{{trans('backend.user_phone')}} </th>


                                                {{--<th>{{trans('backend.user_adress')}} </th>--}}

                                                {{--<th>{{trans('backend.user_gender')}} </th>--}}



                                                <th> {{trans('backend.added_date')}}  </th>
                                                <th> {{trans('backend.actions')}} </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                          @if($users->count())

                                      		@foreach($users as $user)
                                            <tr class="odd gradeX">

                                                <td> {{ $loop->iteration }} </td>

                                                {{-- name of tranier --}}
                                                <td>
                                                    {{  $user->name }}
                                                </td>

                                                {{-- name of tranier English --}}
                                                {{--<td>--}}
                                                    {{--{{  $user->Name_en }}--}}
                                                {{--</td>--}}

                                                {{-- email of tranier  --}}
                                                <td>
                                                    {{  $user->email }}
                                                </td>

                                                {{-- email of phone  --}}
                                                <td>
                                                    {{  $user->phone }}
                                                </td>
                                                {{-- email of address   --}}
                                                {{--<td>--}}
                                                    {{--{{  $user->address }}--}}
                                                {{--</td>--}}
                                                {{-- email of gender   --}}
                                                {{--<td>--}}
                                                    {{--{{  $user->gender }}--}}
                                                {{--</td>--}}




                                                <td>
                                                    {{--<span class="label label-sm label-success"> Approved </span>--}}
                                                  {{ date('Y-m-d' , strtotime($user->created_at)) }}
                                                </td>

                                                <td class="text-nowrap">


                                                    <a class="tooltips " href="{{ route('users.show' , $user->id) }}"  data-id="{{$user->id}}" data-toggle="modal"
                                                       data-original-title="{{ trans('backend.message') }}">
                                                        <i class="fa fa-reply sendMAIL "></i> </a>
                                                    &nbsp;   &nbsp; &nbsp;




                                                    <a class="tooltips"  href="{{ route('users.edit' , $user->id) }}" data-toggle="tooltip"
                                                        data-original-title="{{ trans('backend.edit') }}">
                                                        <i class="fa fa-pencil text-inverse m-r-10"></i> </a>

                                                    &nbsp;	&nbsp;  &nbsp;
                                                    <a class="tooltips" onclick="$('.users_{{ $user->id }}').submit();"
                                                       data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}">
                                                        <i class="fa fa-close text-danger"></i> </a>




                                                    {!! Form::open(['route'=>["users.destroy" , $user->id ] ,
                                                    'class'=>"users_$user->id" ]) !!}

                                                    {!! method_field('DELETE') !!}



                                                    {!! Form::close() !!}

                                                </td>


                                            </tr>
                                            @endforeach
                          @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
   <!-- END EXAMPLE TABLE PORTLET-->
  

@endsection


@section('extra_js')
<script src="{{Request::root()}}/backend/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <script src="{{Request::root()}}/backend/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        {{--<script src="{{Request::root()}}/backend/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
                <!-- END PAGE LEVEL SCRIPTS -->

<script src="{{Request::root()}}/backend/assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
@endsection
@extends('backend.app')

@section('page_title' ,trans('backend.users'))

@section('breadcrumb')


    <li>
        <a href="{{route('users.index')}}">{{trans('backend.users')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">{{trans('backend.message')}}</span>
    </li>


@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();
     $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;


@endphp


@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    @if($lang=='ar')
    <link href="{{Request::root()}}/backend/assets/pages/css/blog-rtl.min.css" rel="stylesheet" type="text/css" />
    @else
        <link href="{{Request::root()}}/backend/assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />

    @endif

@endsection

@section('content')


    <div class="portlet box blue">

        <div class="portlet-body">
            <div class="tabbable-custom nav-justified">
                <ul class="nav nav-tabs nav-justified">

                    <li class="active">
                        <a href="#ar" data-toggle="tab" aria-expanded="true">{{trans('backend.msg')}}</a>
                    </li>

                </ul>
                <form action="{{Request::root()}}/{{$lang}}/sendMsg/{{$id}}" method="post" >
                          {{csrf_field()}}
                <div class="tab-content">

                    <div class="tab-pane active" id="ar">



                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-paper-plane font-yellow-casablanca"></i>
                                        <span class="caption-subject bold font-yellow-casablanca uppercase"> {{trans('backend.msg')}} </span>
                                    </div>

                                </div>
                               
                            </div>
                            <!-- END Portlet PORTLET-->



                        <div class="form-group">

                            <div class="col-md-12">
                                <textarea name="message" class="form-control" rows="6"></textarea>
                            </div>
                        </div>




                            <br/>
                          <div class="form-group" >
                              <button type="submit" class="btn green"> {{trans('backend.send_msg')}}</button>

                          </div>

                    </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>





@endsection

@section('extra_js')
    <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
@endsection
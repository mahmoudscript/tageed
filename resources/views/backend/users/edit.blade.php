@extends('backend.app')

@section('page_title' ,trans('backend.users'))

@section('breadcrumb')


    <li>
        <a href="{{route('users.index')}}">{{trans('backend.users')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">{{trans('backend.update_users')}}</span>
    </li>


@endsection

@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();

     $laguage = \App\Language::where(['label'=>$lang])->first();


             // 1 ->  Arabic
             // 2 -> English


     $language_id = $laguage->id ;

@endphp


@section('content')


    <div class="portlet box blue">

        <div class="portlet-body">
            <div class="tabbable-custom nav-justified">


                <form action="{{route('users.update',$user->id)}}" method="post" id="form_sample_2" enctype="multipart/form-data">
                    {{csrf_field()}}
                    {!! method_field('put') !!}
                    <div class="tab-content">



                       {{-- Arabic Name --}}
                        <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control" value="{{$user->name}}" name="name" id="form_control_1" >
                                    <label for="form_control_1">{{trans('backend.user_name')}}
                                        <span class="required">*</span>
                                    </label>
                                    <span class="help-block">{{trans('backend.user_name')}}</span>
                                </div>

                        {{-- Adress --}}

                        <div class="form-group form-md-line-input {{ $errors->has("phone") ? ' has-error' : '' }}">
                            <input type="text" class="form-control" name="phone" id="form_control_1" >
                            </input>
                            <label for="form_control_1">{{trans('backend.user_phone')}}
                                <span class="required">*</span>
                            </label>
                            @if ($errors->first("phone"))
                                <span class="help-block">
                                        <strong>{{ $errors->first("phone") }}</strong>
                                    </span>
                            @endif                   </div>
                        {{-- Email --}}

                        <div class="form-group form-md-line-input">
                            <input type="text" class="form-control" value="{{$user->email}}"  name="email" id="form_control_1" >
                            <label for="form_control_1">{{trans('backend.user_email')}}
                                <span class="required">*</span>
                            </label>
                            <span class="help-block">{{trans('backend.user_email')}}</span>
                        </div>
                        
                        {{-- Gender --}}

                        {{--<div class="form-group">--}}
                            {{--<label for="sel1"> {{trans('backend.user_gender')}} </label>--}}
                            {{--<select class="form-control" id="sel1" name="gender">--}}
                                {{--<option value="{{$user->gender}}" selected >--}}

                                    {{----}}

                                    {{---  hello world :p--}}
                                    {{--- this if to show gender for user in Two Language ( ar , en ) .--}}



                                   {{----}}

                                   {{--@php--}}
                                   {{--if($lang == "ar")--}}
                                   {{--{--}}
                                           {{--if($user->gender = "male")--}}
                                           {{--{--}}
                                           {{--$type = 'ذكر';--}}

                                           {{--}--}}
                                           {{--elseif ($user->gender = "female")--}}
                                           {{--{--}}
                                           {{--$type = 'انثي';--}}

                                           {{--}--}}
                                   {{--} elseif ($lang == "en")--}}
                                   {{--{--}}

                                           {{--if($user->gender = "male")--}}
                                           {{--{--}}
                                           {{--$type = 'male';--}}

                                           {{--}--}}
                                           {{--elseif ($user->gender = "female")--}}
                                           {{--{--}}
                                           {{--$type = 'female';--}}

                                           {{--}--}}

                                   {{--}--}}

                                   {{--echo $type ;--}}



                                   {{--@endphp--}}

                                {{--</option>--}}

                                {{--<option value="male">{{trans('backend.male')}}</option>--}}
                                {{--<option value="female">{{trans('backend.female')}}</option>--}}
                            {{--</select>--}}
                        {{--</div>--}}
                        {{-- Role --}}

                        {{--<div class="form-group">--}}
                            {{--<label for="sel1"> {{trans('backend.user_role')}} </label>--}}
                            {{--<select class="form-control" id="sel1" name="role">--}}


                                {{--@php--}}
                                    {{--if($lang == "ar")--}}
                                    {{--{--}}
                                            {{--if($user->role = "admin")--}}
                                            {{--{--}}
                                            {{--$type = 'المديرين';--}}

                                            {{--}--}}
                                            {{--elseif ($user->role = "user")--}}
                                            {{--{--}}
                                            {{--$type = 'الاعضاء';--}}

                                            {{--}--}}
                                            {{--elseif ($user->role = "trainer")--}}
                                            {{--{--}}
                                            {{--$type = 'المدربين';--}}

                                            {{--}--}}

                                    {{--} elseif ($lang == "en")--}}
                                    {{--{--}}

                                             {{--if($user->role = "admin")--}}
                                            {{--{--}}
                                            {{--$type = 'Admin';--}}

                                            {{--}--}}
                                            {{--elseif ($user->role = "user")--}}
                                            {{--{--}}
                                            {{--$type = 'User';--}}

                                            {{--}--}}
                                            {{--elseif ($user->role = "trainer")--}}
                                            {{--{--}}
                                            {{--$type = 'Trainer';--}}
                                            {{--}--}}


                                    {{--}--}}






                                {{--@endphp--}}


                                {{--<option value="{{$user->role}}" selected> {{$type}}</option>--}}

                                {{--<option value="admin">{{trans('backend.admin')}}</option>--}}
                                {{--<option value="trainer">{{trans('backend.trainer')}}</option>--}}
                                {{--<option value="user">{{trans('backend.user')}}</option>--}}

                            {{--</select>--}}
                        {{--</div>--}}


                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> {{trans('backend.select_image')}} </span>
                                                                            <span class="fileinput-exists"> {{trans('backend.change_image')}} </span>
                                                                            <input type="file" name="image"> </span>
                                    <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> {{trans('backend.remove')}}</a>
                                </div>
                            </div>

                        </div>

                        <div class="form-actions">

                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                    <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection
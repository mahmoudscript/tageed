@extends('backend.app')

@section('page_title' ,trans('backend.users'))

@section('breadcrumb')


         <li>
           <a href="{{route('users.index')}}">{{trans('backend.users')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.create_user')}}</span>
         </li>


@endsection

@section('extra_css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
 <!-- END PAGE LEVEL PLUGINS -->

@endsection


@section('content')


<div class="portlet box blue">

   <div class="portlet-body">
      <div class="tabbable-custom nav-justified">
         
         <form action="{{route('users.store')}}" method="post" id="form_sample_2" enctype="multipart/form-data">
         {{csrf_field()}}
            <div class="tab-content">

               <div class="tab-pane active" id="ar">

                  <div class="form-group form-md-line-input {{ $errors->has("name") ? ' has-error' : '' }}">
                     <input type="text" class="form-control" name="name" id="form_control_1" >
                     <label for="form_control_1">{{trans('backend.name')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("name"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("name") }}</strong>
                                    </span>
                      @endif
                  </div>




               </div>
               

                {{-- Adress --}}

                   {{--<div class="form-group form-md-line-input  {{ $errors->has("address") ? ' has-error' : '' }}">--}}
                     {{--<input type="text" class="form-control" name="address" id="form_control_1" >--}}
                     {{--</input>--}}
                       {{--<label for="form_control_1">{{trans('backend.user_adress')}}--}}
                           {{--<span class="required">*</span>--}}
                       {{--</label>--}}
                       {{--@if ($errors->first("address"))--}}
                           {{--<span class="help-block">--}}
                                        {{--<strong>{{ $errors->first("address") }}</strong>--}}
                                    {{--</span>--}}
                       {{--@endif--}}
                   {{--</div>--}}

                   {{-- Email --}}

                   <div class="form-group form-md-line-input  {{ $errors->has("email") ? ' has-error' : '' }}">
                     <input type="email" class="form-control" name="email" id="form_control_1" >
                     </input>
                       <label for="form_control_1">{{trans('backend.user_email')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("email"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("email") }}</strong>
                                    </span>
                       @endif                   </div>

                   {{-- Password --}}

                   <div class="form-group form-md-line-input {{ $errors->has("password") ? ' has-error' : '' }}">
                     <input type="password" class="form-control" name="password" id="form_control_1" >
                     </input>
                       <label for="form_control_1">{{trans('backend.user_password')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("password"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("password") }}</strong>
                                    </span>
                       @endif                   </div>
                   {{-- phone --}}

                   <div class="form-group form-md-line-input {{ $errors->has("phone") ? ' has-error' : '' }}">
                     <input type="text" class="form-control" name="phone" id="form_control_1" >
                     </input>
                       <label for="form_control_1">{{trans('backend.user_phone')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("phone"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("phone") }}</strong>
                                    </span>
                       @endif                   </div>

                   {{-- Gender --}}

                   {{--<div class="form-group">--}}
                       {{--<label for="sel1"> {{trans('backend.user_gender')}} </label>--}}
                       {{--<select class="form-control" id="sel1" name="gender">--}}
                           {{--<option value="male">{{trans('backend.male')}}</option>--}}
                           {{--<option value="female">{{trans('backend.female')}}</option>--}}
                       {{--</select>--}}
                   {{--</div>--}}



                   <div class="form-group {{ $errors->has("image") ? ' has-error' : '' }}">
                                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                    <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> {{trans('backend.select_image')}} </span>
                                                                            <span class="fileinput-exists"> {{trans('backend.change_image')}} </span>
                                                                            <input type="file" name="image"> </span>
                                                                        @if ($errors->first("image"))
                                                                            <span class="help-block">
                                        <strong>{{ $errors->first("image") }}</strong>
                                    </span>
                                                                        @endif
                                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> {{trans('backend.remove')}}</a>
                                                                    </div>
                                                                </div>

                                                            </div>

               <div class="form-actions">

                  <div class="row">
                     <div class="col-md-12">
                        <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                        <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>



@endsection
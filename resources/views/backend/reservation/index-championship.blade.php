@extends('backend.app')

@section('page_title' , trans('backend.champion_reservation'))

@section('breadcrumb')


         <li>
           <a href="#">{{trans('backend.reservations')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.champion_reservation')}}</span>
         </li>


@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp

@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
            <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
           {{--<link href="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />--}}
    <link href="{{Request::root()}}/backend/assets/global/plugins/cubeportfolio/css/cubeportfolio.css" rel="stylesheet" type="text/css" />

    <link href="{{Request::root()}}/backend/assets/pages/css/portfolio.min.css" rel="stylesheet" type="text/css" />
            <!-- END PAGE LEVEL PLUGINS -->
@endsection


@section('content')

  <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> {{trans('backend.champion_reservation')}}</span>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>

                                                <th>#</th>
                                                <th>{{trans('backend.championship_name')}} </th>
                                                <th>{{trans('backend.reservation_image')}} </th>
                                                <th>{{trans('backend.status')}} </th>
                                                <th> {{trans('backend.added_date')}}  </th>
                                                <th> {{trans('backend.actions')}} </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                          @if($championReservation->count())

                                      		@foreach($championReservation as $champion)
                                            <tr class="odd gradeX">

                                                <td> {{ $loop->iteration }} </td>
                                                <td>
                                                    @foreach($champion->championship->description as $sham)
                                                       @if($sham->language_id==$language_id)
                                                        {{ $sham->description }}
                                                        @endif
                                                     @endforeach

                                                </td>
                                                <td>

                                                    <div id="js-grid-mosaic" class="">
                                                        <div class="cbp-item">
                                                            <a href="{{asset("uploads/championConfirm_photos/$champion->receipt_image")}}" class="cbp-caption cbp-lightbox" >
                                                                <div class="">
                                                                    <img src="{{asset("uploads/championConfirm_photos/$champion->receipt_image")}}" alt=""> </div>

                                                            </a>
                                                        </div>
                                                    </div>





                                                </td>
                                                <td>
                                                    @if($champion->status=='pending')
                                                        <span class="label label-sm label-warning"> {{trans('backend.pending')}}</span>
                                                    @elseif($champion->status=='approve')
                                                        <span class="label label-sm label-success">  {{trans('backend.approve')}} </span>
                                                    @else
                                                        <span class="label label-sm label-danger">  {{trans('backend.reject')}}  </span>
                                                    @endif
                                                    
                                                </td>
                                                <td>
                                                    {{--<span class="label label-sm label-success"> Approved </span>--}}
                                                  {{ date('Y-m-d' , strtotime($champion->created_at)) }}
                                                </td>

                                                <td>
                                                    <div class="text-nowrap">

                                                        @if($champion->status=='pending')

                                                        <a   onclick="$('.champion_form_{{$champion->id}}').submit();" data-toggle="tooltip" data-original-title="{{ trans('backend.approve') }}"> <i class="glyphicon glyphicon-ok"></i> </a>

                                                        {!! Form::open(['url'=>'/accpetChampionshipReservation/'.$champion->id , 'class'=>"champion_form_$champion->id" , 'method' =>'get']) !!}




                                                        {!! Form::close() !!}



                                                         <a   onclick="$('.champion_form_{{$champion->id}}').submit();" data-toggle="tooltip" data-original-title="{{ trans('backend.approve') }}"> <i class="fa fa-close text-danger"></i> </a>

                                                        {!! Form::open(['url'=>'/rejectChampionshipReservation/'.$champion->id , 'class'=>"champion_form_$champion->id" , 'method' =>'get']) !!}




                                                        {!! Form::close() !!}

                                                         @endif



                                                 </div>
                                             </td>
                                         </tr>
                                         @endforeach
                       @endif
                                     </tbody>
                                 </table>
                             </div>
                         </div>
<!-- END EXAMPLE TABLE PORTLET-->


@endsection


@section('extra_js')
<script src="{{Request::root()}}/backend/assets/global/scripts/datatable.js" type="text/javascript"></script>
     <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
     <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
     <!-- END PAGE LEVEL PLUGINS -->
     <!-- BEGIN THEME GLOBAL SCRIPTS -->
     <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
     <!-- END THEME GLOBAL SCRIPTS -->
      <script src="{{Request::root()}}/backend/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>

     <!-- BEGIN PAGE LEVEL SCRIPTS -->
     {{--<script src="{{Request::root()}}/backend/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
<script src="{{Request::root()}}/backend/assets/global/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->

<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="{{Request::root()}}/backend/assets/pages/scripts/portfolio-2.min.js" type="text/javascript"></script>
@endsection
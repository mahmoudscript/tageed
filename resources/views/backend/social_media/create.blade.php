@extends('backend.app')

@section('page_title' ,trans('backend.social_media'))

@section('breadcrumb')


         <li>
           <a href="{{route('social-media.index')}}">{{trans('backend.social_media')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.add_social')}}</span>
         </li>


@endsection

@section('content')


<div class="portlet light portlet-fit portlet-form bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-red"></i>
                                        <span class="caption-subject font-red sbold uppercase">{{trans('backend.add_social')}}</span>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form action="{{route('social-media.store')}}" method="post" id="form_sample_3" novalidate="novalidate">
                                       {{csrf_field()}}
                                        <div class="form-body">
                                            <div class="form-group form-md-line-input form-md-floating-label {{ $errors->has("name") ? ' has-error' : '' }}">
                                                <input type="text" class="form-control" name="name" id="form_control_1">
                                                <label for="form_control_1">{{trans('backend.name')}}</label>
                                                @if ($errors->first("name"))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first("name") }}</strong>
                                    </span>
                                                @endif                                            </div>

                                            <div class="form-group form-md-line-input form-md-floating-label {{ $errors->has("url") ? ' has-error' : '' }}">
                                                <input type="text" class="form-control" name="url" id="form_control_1">

                                                <label for="form_control_1">{{trans('backend.url')}}</label>
                                                @if ($errors->first("url"))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first("url") }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                            <div class="form-group form-md-line-input form-md-floating-label {{ $errors->has("icon") ? ' has-error' : '' }}">
                                                <input type="text" class="form-control" name="icon" id="form_control_1">
                                                <label for="form_control_1">{{trans('backend.icon')}}</label>
                                                @if ($errors->first("icon"))
                                                    <span class="help-block">
                                        <strong>{{ $errors->first("icon") }}</strong>
                                    </span>
                                                @endif                                            </div>


                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn blue">{{trans('backend.save')}}</button>
                                                    <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>



@endsection

@include('backend.partials.header')


<!-- Navigation Shall Comes  Here -->

@include('backend.partials.navigation')

<!-- Sidebar Shall Comes Here  -->
@include('backend.partials.sidebar')

 <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">


                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->




                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <div class="page-title">
                            <h1>
                              @yield('page_title')
                            </h1>
                        </div>
                        <!-- END PAGE TITLE -->

                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                    <ul class="page-breadcrumb breadcrumb">

                       @yield('breadcrumb')

                    </ul>
                    <!-- END PAGE BREADCRUMB -->
                    @include('backend.partials.messages')
                     <div class="row">
                          <div class="col-md-12">
                             @yield('content')
                          </div>
                     </div>
                </div>



 @include('backend.partials.footer')
@extends('backend.app')

@section('page_title' ,trans('backend.about'))

@section('breadcrumb')


         <li>
           <a href="{{route('about')}}">{{trans('backend.about')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.about')}}</span>
         </li>


@endsection

@section('extra_css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
 <!-- END PAGE LEVEL PLUGINS -->

@endsection
 @php

     $lang = LaravelLocalization::getCurrentLocale();
     $laguage = \App\Language::where(['label'=>$lang])->first();
     $language_id = $laguage->id ;

 @endphp

@section('content')


<div class="portlet box blue">

   <div class="portlet-body">
      <div class="tabbable-custom nav-justified">
         <ul class="nav nav-tabs nav-justified">
            @foreach($languages as $lang)
            <li class="{{$loop->iteration == 1 ? 'active' : '' }}">
               <a href="#{{$lang->label}}" data-toggle="tab" aria-expanded="true"> {{strtoupper($lang->label)}}</a>
            </li>
            @endforeach
         </ul>
         <form action="{{route('about_update',$about->id)}}" method="post" id="form_sample_2" enctype="multipart/form-data">
         {{csrf_field()}}
             {!! method_field('put') !!}

            <div class="tab-content">
                @foreach($about->description as $description)
               <div class="tab-pane {{$loop->iteration == 1 ? 'active' : '' }} " id="{{$description->language->label}}">

               <div class="form-group form-md-line-input  {{ $errors->has("name_$description->languages->label") ? ' has-error' : '' }}">
                     <input type="text" value="{{$description->title}}" class="form-control" name="name_{{$description->language->label}}" id="form_control_1" placeholder="{{trans('backend.title')}}">
                     <label for="form_control_1">{{trans('backend.title')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("name_$description->languages->label"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("name_$description->languages->label") }}</strong>
                                    </span>
                      @endif                  </div>

               <div class="form-group form-md-line-input  {{ $errors->has("desc_$description->languages->label") ? ' has-error' : '' }}">
                     <input type="text" value="{{$description->description}}" class="form-control" name="desc_{{$description->language->label}}" id="form_control_1" >
                     </input>
                     <label for="form_control_1">{{trans('backend.content')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("desc_$description->languages->label"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("desc_$description->languages->label") }}</strong>
                                    </span>
                      @endif                  </div>

               <div class="form-group form-md-line-input  {{ $errors->has("address_$description->languages->label") ? ' has-error' : '' }}">
                     <input type="text" value="{{$description->address}}" class="form-control" name="address_{{$description->language->label}}" id="form_control_1" >
                     </input>
                       <label for="form_control_1">{{trans('backend.address')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("address_$description->languages->label"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("address_$description->languages->label") }}</strong>
                                    </span>
                       @endif                   </div>

                   <div class="form-group form-md-line-input  {{ $errors->has("why_do_i_need_it_$description->languages->label") ? ' has-error' : '' }}">
                       <input type="text" value="{{$description->why_do_i_need_it}}" class="form-control" name="why_do_i_need_it_{{$description->language->label}}" id="form_control_1" >
                       </input>
                       <label for="form_control_1">{{trans('backend.history')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("why_do_i_need_it_$description->languages->label"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("why_do_i_need_it_$description->languages->label") }}</strong>
                                    </span>
                       @endif                   </div>



               </div>
               @endforeach

                   <div class="form-group form-md-line-input  {{ $errors->has("phone") ? ' has-error' : '' }}">
                       <input type="text" value="{{$about->phone}}" class="form-control" name="phone" id="form_control_1" ">
                       <label for="form_control_1">{{trans('backend.phone')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("phone"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("phone") }}</strong>
                                    </span>
                       @endif                   </div>
                   <div class="form-group form-md-line-input {{ $errors->has("email") ? ' has-error' : '' }}">
                       <input type="text" value="{{$about->email}}" class="form-control" name="email" id="form_control_1" >
                       <label for="form_control_1">{{trans('backend.multi_email')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("email"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("email") }}</strong>
                                    </span>
                       @endif                   </div>
                   <div class="form-group form-md-line-input {{ $errors->has("ios_app") ? ' has-error' : '' }}">
                       <input type="text" value="{{$about->ios_app}}" class="form-control" name="ios_app" id="form_control_1" ">
                       <label for="form_control_1">{{trans('backend.ios_app')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("ios_app"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("ios_app") }}</strong>
                                    </span>
                       @endif                   </div>
                   <div class="form-group form-md-line-input {{ $errors->has("google_store") ? ' has-error' : '' }}">
                       <input type="text" value="{{$about->google_store}}" class="form-control" name="google_store" id="form_control_1" ">
                       <label for="form_control_1">{{trans('backend.google_store')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("google_store"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("google_store") }}</strong>
                                    </span>
                       @endif                   </div>




                   <div class="form-group {{ $errors->has("image") ? ' has-error' : '' }}">
                       <div class="fileinput fileinput-new" data-provides="fileinput">
                           <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                               <img src="{{asset("uploads/about/$about->image")}}" alt="" /> </div>
                           <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                           <div>
                                                                        <span class="btn default btn-file">
                                                                            <span class="fileinput-new"> {{trans('backend.select_image')}} </span>
                                                                            <span class="fileinput-exists"> {{trans('backend.change_image')}} </span>
                                                                            <input type="file" name="image"> </span>
                               @if ($errors->first("image"))
                                   <span class="help-block">
                                        <strong>{{ $errors->first("image") }}</strong>
                                    </span>
                               @endif
                               <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> {{trans('backend.remove')}}</a>
                           </div>
                       </div>

                   </div>


               <div class="form-actions">

                  <div class="row">
                     <div class="col-md-12">
                        <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                        <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>



@endsection
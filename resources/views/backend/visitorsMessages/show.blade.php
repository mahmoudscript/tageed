@extends('backend.app')

@section('page_title' , trans('backend.visitors_messages'))

@section('breadcrumb')


         <li>
           <a href="{{route('visitorsMessages.index')}}">{{trans('backend.visitors_messages')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.msg')}}</span>
         </li>


@endsection

@section('content')
    <div class="portlet box blue">

        <div class="portlet-body">
            <div class="tabbable-custom nav-justified">
                <ul class="nav nav-tabs nav-justified">

                        <li class="active">
                            <a href="#ar" data-toggle="tab" aria-expanded="true">{{trans('backend.msg')}}</a>
                        </li>

                </ul>

                    <div class="tab-content">

                            <div class="tab-pane active" id="ar">

                                

                                <div class="col-md-12">
                                    <!-- BEGIN Portlet PORTLET-->
                                    <div class="portlet light">
                                        <div class="portlet-title">
                                            <div class="caption">
                                                <i class="icon-paper-plane font-yellow-casablanca"></i>
                                                <span class="caption-subject bold font-yellow-casablanca uppercase"> {{trans('backend.msg')}} </span>
                                            </div>
                                           
                                        </div>
                                        <div class="portlet-body">
                                            <h1 class="font-green sbold uppercase">
                                                {{$visitorsMessage->Name}}
                                            </h1>
                                             <h3>
                                                 {{$visitorsMessage->email}}
                                             </h3>
                                            <p>
                                            <h3>{{trans('backend.msg')}}</h3>   {{$visitorsMessage->message}}
                                            </p>
                                        </div>
                                    </div>
                                    <!-- END Portlet PORTLET-->
                                </div>





                            </div>
                       


                        <div class="form-actions">

                            <div class="row">
                                <div class="col-md-12">
                                   
                                    <a  class="btn red" href="#form_modal2" data-toggle="modal" > {{trans('backend.reply')}}
                                        <i class="fa fa-reply"></i>
                                    </a>
                                </div>

                                <div id="form_modal2" class="modal fade" role="dialog" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">{{trans('backend.send_msg')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <form action="{{url('sendEmailMessage')}}" method="get" class="form-horizontal">

                                                    <div class="form-group">
                                                        <label class="control-label col-md-4">{{trans('backend.msg')}}</label>
                                                        <div class="col-md-8">
                                                                <textarea name="message" class="form-control input-large" placeholder="{{trans('backend.enter_your_msg')}}" size="16" type="text" value="" >
                                                                </textarea>

                                                                <input type="hidden" name="email" value="{{$visitorsMessage->email}}">
                                                                <input type="hidden" name="name" value="{{$visitorsMessage->Name}}">

                                                        </div>
                                                    </div>




                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">{{trans('backend.close')}}</button>
                                                <button class="btn green" >{{trans('backend.send')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

            </div>
        </div>
    </div>


@endsection
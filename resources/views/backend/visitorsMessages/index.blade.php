@extends('backend.app')

@section('page_title' , trans('backend.visitors_messages'))

@section('breadcrumb')


         <li>
           <a href="{{route('visitorsMessages.index')}}">{{trans('backend.visitors_messages')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.visitors_messages')}}</span>
         </li>


@endsection



@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
            <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
           {{--<link href="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />--}}

            <!-- END PAGE LEVEL PLUGINS -->

@endsection

@section('content')

 <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> {{trans('backend.visitors_messages')}}</span>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>

                                                <th>#</th>
                                                <th>{{trans('backend.name')}} </th>
                                                  <th>{{trans('backend.subject')}} </th>
                                                  <th>{{trans('backend.email')}} </th>
                                                  <th>{{trans('backend.message')}} </th>

                                                <th> {{trans('backend.added_date')}}  </th>
                                                <th> {{trans('backend.actions')}} </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                          @if($visitorsMessages->count()>0)

                                      		@foreach($visitorsMessages as $visitorsMessage)
                                            <tr class="odd gradeX">

                                                <td> {{ $loop->iteration }} </td>
                                                <td>
                                                   {{ $visitorsMessage->Name }}
                                                </td>
                                                 <td>
                                                   {{ $visitorsMessage->subject }}
                                                </td>
                                                 <td>
                                                   {{ $visitorsMessage->email }}
                                                </td>
                                                 <td>
                                                   {{ $visitorsMessage->message }}
                                                </td>


                                                <td>
                                                    {{--<span class="label label-sm label-success"> Approved </span>--}}
                                                  {{ date('Y-m-d' , strtotime($visitorsMessage->created_at)) }}
                                                </td>

                                                <td>
                                                    <div class="btn-group">
                                                 <a   class="tooltips"  href="{{ route('visitorsMessages.show' , $visitorsMessage->id) }}" data-toggle="tooltip" data-original-title="{{ trans('backend.show_message') }}">
                                                  <i class="fa fa-reply"></i> </a>

                                                        &nbsp;	&nbsp;  &nbsp;	&nbsp;

                                                        <a   class="tooltips"  onclick="$('.messages_form_{{$visitorsMessage->id}}').submit();" data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}"> <i class="fa fa-close text-danger"></i> </a>

                                                          {!! Form::open(['route'=>['visitorsMessages.destroy', $visitorsMessage->id] , 'class'=>"messages_form_$visitorsMessage->id" , 'method' =>'POST']) !!}

                                                           {!! method_field('DELETE') !!}


                                                           {!! Form::close() !!}

                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                          @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
   <!-- END EXAMPLE TABLE PORTLET-->



@endsection

@section('extra_js')
<script src="{{Request::root()}}/backend/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <script src="{{Request::root()}}/backend/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>


@endsection
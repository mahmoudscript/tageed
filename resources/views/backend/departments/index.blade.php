@extends('backend.app')

@section('page_title' , trans('backend.departments'))

@section('breadcrumb')


         <li>
           <a href="#">{{trans('backend.departments')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.main_department')}}</span>
         </li>


@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp

@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
            <link href="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
            <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
           {{--<link href="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />--}}

            <!-- END PAGE LEVEL PLUGINS -->
@endsection


@section('content')

  <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <i class="icon-settings font-dark"></i>
                                        <span class="caption-subject bold uppercase"> {{trans('backend.departments')}}</span>
                                    </div>

                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="btn-group">
                                                    <a href="{{route('departments.create')}}" id="sample_editable_1_new" class="btn sbold green"> {{trans('backend.add_main_department')}}
                                                        <i class="fa fa-plus"></i>
                                                    </a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>

                                                <th>#</th>
                                                <th>{{trans('backend.department_name')}} </th>
                                                <th> {{trans('backend.added_date')}}  </th>
                                                <th> {{trans('backend.actions')}} </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                          @if($departments->count())

                                      		@foreach($departments as $department)
                                            <tr class="odd gradeX">

                                                <td> {{ $loop->iteration }} </td>
                                                <td>
                                                  @foreach($department->description as $description)
                                                        @if($language_id==$description->language_id)
                                                      {{ $description->name }}
                                                        @endif
                                                   @endforeach
                                                </td>

                                                <td>
                                                    {{--<span class="label label-sm label-success"> Approved </span>--}}
                                                  {{ date('Y-m-d' , strtotime($department->created_at)) }}
                                                </td>

                                                <td>
                                                    <div class="btn-group">


                                                        <a class="tooltips" href="{{ route('showSubs' , $department->id) }}"
                                                           data-toggle="tooltip" data-original-title="{{ trans('backend.show_sub_departments') }}">
                                                            <i class="fa fa-align-justify text-inverse m-r-10 "></i> </a>
                                                        &nbsp;	&nbsp;	



                                                        <a class="tooltips" onclick="$('.sub_form_{{ $department->id }}').submit();"
                                                           data-toggle="tooltip" data-original-title="{{ trans('backend.add_sub_department') }}">
                                                            <i class="fa fa-plus-circle text-inverse m-r-10 "></i> </a>

                                                        &nbsp;	&nbsp;






                                                        <a class="tooltips"  href="{{ route('departments.edit' , $department->id) }}"
                                                           data-toggle="tooltip" data-original-title="{{ trans('backend.edit') }}">
                                                            <i class="fa fa-pencil text-inverse m-r-10 "></i> </a>
                                                        &nbsp;	&nbsp;



                                                      
                                                        <a  class="tooltips" onclick="$('.departments_form_{{ $department->id }}').submit();" data-toggle="tooltip" data-original-title="{{ trans('backend.delete') }}"> <i class="fa fa-close text-danger"></i> </a>

                                                          {!! Form::open(['route'=>['departments.destroy', $department->id] , 'class'=>"departments_form_$department->id" , 'method' =>'POST']) !!}

                                                           {!! method_field('DELETE') !!}


                                                           {!! Form::close() !!}


                                                        {!! Form::open(['route'=>['add_sub', $department->id] , 'class'=>"sub_form_$department->id" , 'method' =>'POST']) !!}
                                                        {!! csrf_field() !!}
                                                        {!! Form::close() !!}

                                                    </div>
                                                </td>
                                            </tr>
                                            @endforeach
                          @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->


@endsection


@section('extra_js')
<script src="{{Request::root()}}/backend/assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
         <script src="{{Request::root()}}/backend/assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>

        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        {{--<script src="{{Request::root()}}/backend/assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>--}}
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
                <!-- END PAGE LEVEL SCRIPTS -->
        <!-- END PAGE LEVEL SCRIPTS -->
@endsection
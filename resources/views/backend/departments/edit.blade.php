@extends('backend.app')

@section('page_title' ,trans('backend.departments'))

@section('breadcrumb')


    <li>
        <a href="{{route('departments.index')}}">{{trans('backend.departments')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">{{trans('backend.update_department')}}</span>
    </li>


@endsection

@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    {{--<link rel="stylesheet" href="{{Request::root()}}/backend/dropify/dist/css/demo.css">--}}

    <link href="{{Request::root()}}/backend/dropify/dist/css/dropify.css" rel="stylesheet" type="text/css" />

@endsection

@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp

@section('content')


    <div class="portlet box blue">

        <div class="portlet-body">
            <div class="tabbable-custom nav-justified">
                <ul class="nav nav-tabs nav-justified">
                    @foreach($languages as $lang)
                        <li class="{{$loop->iteration == 1 ? 'active' : '' }}">
                            <a href="#{{$lang->label}}" data-toggle="tab" aria-expanded="true"> {{strtoupper($lang->label)}}</a>
                        </li>
                    @endforeach
                </ul>
                <form action="{{route('departments.update',$department->id)}}" method="post" id="form_sample_2">
                    {{csrf_field()}}
                    {!! method_field('put') !!}
                    <div class="tab-content">
                        @foreach($department->description as $description)
                            <div class="tab-pane {{$loop->iteration == 1 ? 'active' : '' }} " id="{{$description->language->label}}">

                                <div class="form-group form-md-line-input {{ $errors->has("name_$description->language->label") ? ' has-error' : '' }}">
                                    <input type="text" class="form-control"  value="{{$description->name}}" name="name_{{$description->language->label}}" id="form_control_1" >
                                    <label for="form_control_1">{{trans('backend.department_name')}}
                                        <span class="required">*</span>
                                    </label>
                                    @if ($errors->first("name_$description->language->label"))
                                        <span class="help-block">
                                        <strong>{{ $errors->first("name_$description->language->label") }}</strong>
                                    </span>
                                    @endif                  </div>
                            </div>
                        @endforeach



                            <div class="form-group form-md-line-input  {{ $errors->has("images") ? ' has-error' : '' }}">
                                <label class="form_control_1"> {{trans('backend.product_images')}}</label>

                                <div class="fallback">
                                    <input class="dropify" data-default-file="{{trans('backend.product_images')}}" name="image" type="file" multiple />

                                </div>

                                @if ($errors->first("images"))
                                    <span class="help-block">
                                        <strong>{{ $errors->first("images") }}</strong>
                                    </span>
                                @endif
                            </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                                    <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>



@endsection

@section('extra_js')

    <script src="{{Request::root()}}/backend/dropify/dist/js/dropify.js" type="text/javascript"></script>

    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>



@endsection
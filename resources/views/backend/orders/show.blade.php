@extends('backend.app')

@section('page_title' ,trans('backend.order_info'))

@section('breadcrumb')


    <li>
        <a href="{{route('orders.index')}}">{{trans('backend.orders')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">{{trans('backend.order_info')}}</span>
    </li>


@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();
     $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;


@endphp


@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    @if($lang=='ar')
    <link href="{{Request::root()}}/backend/assets/pages/css/blog-rtl.min.css" rel="stylesheet" type="text/css" />
    @else
        <link href="{{Request::root()}}/backend/assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />

    @endif

@endsection

@section('content')


    <div class="portlet box blue">

        <div class="portlet-body">
            <div class="tabbable-custom nav-justified">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet yellow-crusta box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>{{trans('backend.order_details')}}  </div>

                                    </div>
                                    <div class="portlet-body">

                                        <div class="row static-info">
                                            <div class="col-md-5 name">{{trans('backend.order_name')}} : </div>
                                            <div class="col-md-7 value">
                                                <a href="{{url('ar/products/'.$order->id)}}">  {{ $order->title }}    </a>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>{{trans('backend.customer_info')}}</div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name">{{trans('backend.customer_name')}} : </div>
                                            <div class="col-md-7 value"> {{$order->user->name}} </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name">{{trans('backend.email')}} : </div>
                                            <div class="col-md-7 value"> {{$order->user->email}} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name">{{trans('backend.phone')}} : </div>
                                            <div class="col-md-7 value">{{$order->user->phone}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if($order->status == 'special')

                                <form action="{{route('make_special',$order->id)}}" method="post" id="form_sample_1" enctype="multipart/form-data">
                                    {{csrf_field()}}

                                    <div class="col-md-8">
                                        <!-- BEGIN VALIDATION STATES-->

                                        <!-- BEGIN FORM-->
                                        <div class="form-body">
                                            <div class="form-group form-md-line-input ">
                                                <input type="text" class="form-control" name="days" value="{{$order->days}}" id="form_control_1" placeholder="اكتب عدد الايام" aria-required="true" aria-describedby="form_control_1"><span id="form_control_1" class="help-block "></span>
                                                <label for="form_control_1">عدد الايام
                                                    <span class="required" aria-required="true">*</span>
                                                </label>
                                            </div>

                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="btn green">تميز</button>

                                                    </div>
                                                </div>
                                            </div>

                                            <!-- END FORM-->

                                        </div>
                                        <!-- END VALIDATION STATES-->
                                    </div>

                                </form>


                            @endif


                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>







@endsection

@section('extra_js')
    <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
@endsection
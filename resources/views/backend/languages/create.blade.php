@extends('backend.app')

@section('page_title' , trans('backend.languages'))

@section('breadcrumb')


         <li>
           <a href="{{route('languages.index')}}">{{trans('backend.languages')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.add_language')}}</span>
         </li>


@endsection

@section('content')

<div class="portlet box blue">

   <div class="portlet-body">
      <div class="tabbable-custom nav-justified">
         <ul class="nav nav-tabs nav-justified">
            @foreach($languages as $lang)
            <li class="{{$loop->iteration == 1 ? 'active' : '' }}">
               <a href="#{{$lang->label}}" data-toggle="tab" aria-expanded="true"> {{strtoupper($lang->label)}}</a>
            </li>
            @endforeach
         </ul>
         <form action="{{route('languages.store')}}" method="post" id="form_sample_2" enctype="multipart/form-data">
         {{csrf_field()}}
            <div class="tab-content">
               @foreach($languages as $lang)
               <div class="tab-pane {{$loop->iteration == 1 ? 'active' : '' }} " id="{{$lang->label}}">

                  <div class="form-group form-md-line-input">
                     <input type="text" class="form-control" name="name_{{$lang->label}}" id="form_control_1" placeholder="{{trans('backend.language_name')}}">
                     <label for="form_control_1">{{trans('backend.language_name')}}
                     <span class="required">*</span>
                     </label>
                     <span class="help-block">{{trans('backend.language_name')}}</span>
                  </div>

                  <div class="form-group form-md-line-input">
                     <input type="text" class="form-control" name="label_{{$lang->label}}" placeholder="{{trans('backend.language_label')}}" id="form_control_1" >

                     <label for="form_control_1">{{trans('backend.language_label')}}
                     <span class="required">*</span>
                     </label>
                     <span class="help-block">{{trans('backend.language_label')}}</span>
                  </div>




               </div>
               @endforeach


               <div class="form-actions">

                  <div class="row">
                     <div class="col-md-12">
                        <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                        <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>

@endsection
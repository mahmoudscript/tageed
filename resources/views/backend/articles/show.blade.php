@extends('backend.app')

@section('page_title' ,trans('backend.adverts'))

@section('breadcrumb')


    <li>
        <a href="{{route('articles.index')}}">{{trans('backend.adverts')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">{{trans('backend.add_advert')}}</span>
    </li>


@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();
     $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;





@endphp


@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    @if($lang=='ar')
    <link href="{{Request::root()}}/backend/assets/pages/css/blog-rtl.min.css" rel="stylesheet" type="text/css" />
    @else
        <link href="{{Request::root()}}/backend/assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />

    @endif

@endsection

@section('content')



            <div class="blog-page blog-content-2">
                <div class="row">
        <div class="col-lg-12">
            <div class="blog-single-content bordered blog-container">
                <div class="blog-single-head">
                    <h1 class="blog-single-head-title">
                     @foreach($adverts->description as $des)

                         @if($language_id==$des->language_id)
                              {{$des->name}}
                         @endif

                     @endforeach



                    </h1>
                    <div class="blog-single-head-date">
                        <i class="icon-calendar font-blue"></i>
                        <a href="javascript:;">{{$adverts->created_at}}</a>
                    </div>
                </div>
                <div class="blog-single-img">
                    <img style="width: 600px ; height:200px " src="{{asset("uploads/articles/$adverts->image")}}"> </div>
                <div class="blog-single-desc">
                    @foreach($adverts->description as $des)

                        @if($language_id==$des->language_id)
                            {{$des->description}}
                        @endif

                    @endforeach
                </div>


            </div>
        </div>
                </div>
            </div>







@endsection

@section('extra_js')
    <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
@endsection
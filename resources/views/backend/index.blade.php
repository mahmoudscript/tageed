@extends('backend.app')

@section('page_title' , trans('backend.dashboard'))

@section('breadcrumb')


         <li>
           <a href="">{{trans('backend.dashboard')}}</a>
            <i class="fa fa-circle"></i>
         </li>



@endsection

@section('content')

<div class="row">



    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 bordered">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="{{$users}}">{{$users}}</span>
                    </h3>
                    <small>{{trans('backend.users')}}</small>
                </div>
                <div class="icon">
                    <i class="icon-user"></i>
                </div>
            </div>
            <div class="progress-info">
                
            </div>
        </div>
    </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="dashboard-stat2 bordered">
                                <div class="display">
                                    <div class="number">
                                        <h3 class="font-green-sharp">
                  <span data-counter="counterup" data-value="{{$products}}"> {{ count($products) }}</span>
                                            <small class="font-green-sharp"></small>
                                        </h3>
                                        <small>{{trans('backend.products')}}</small>
                                    </div>
                                    <div class="icon">
                                        <i class="icon-pie-chart"></i>
                                    </div>
                                </div>
                                <div class="progress-info">
                                   
                                </div>
                            </div>
                        </div>
                       

                    </div>



@endsection





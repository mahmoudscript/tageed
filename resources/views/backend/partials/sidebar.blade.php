<!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                        {{--DASHBOARD--}}
                        <li class="nav-item start ">
                            <a href="{{Request::root()}}/adminPanel" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">{{trans('backend.dashboard')}}</span>

                            </a>

                        </li>
                        {{--LANGUAGES--}}
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-language"></i>
                                <span class="title">{{trans('backend.languages')}}</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="#" class="nav-link ">
                                        <span class="title">{{trans('backend.add_language')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{route('languages.index')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.all_languages')}}</span>
                                    </a>
                                </li>


                            </ul>
                        </li>
                        {{--USERS--}}
                        <li class="nav-item  ">
                                                    <a href="javascript:;" class="nav-link nav-toggle">
                                                        <i class="fa fa-users"></i>
                                                        <span class="title">{{trans('backend.users')}}</span>
                                                        <span class="arrow"></span>
                                                    </a>
                                                    <ul class="sub-menu">
                                                        <li class="nav-item  ">
                                                            <a href="{{route('users.create')}}"class="nav-link ">
                                                                <span class="title">{{trans('backend.create_user')}}</span>
                                                            </a>
                                                        </li>
                                                        <li class="nav-item  ">
                                                            <a href="{{route('users.index')}}" class="nav-link ">
                                                                <span class="title">{{trans('backend.show_users')}}</span>
                                                            </a>
                                                        </li>




                                                    </ul>
                        </li>
                        {{--DEPARTMENTS--}}
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-th"></i>
                                <span class="title">{{trans('backend.departments')}}</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="{{route('departments.create')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.add_department')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{route('departments.index')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.all_departments')}}</span>
                                    </a>
                                </li>


                            </ul>
                        </li>
                        {{--adverts--}}
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-basket"></i>
                                <span class="title">{{trans('backend.products')}}</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                
                                <li class="nav-item  ">
                                    <a href="{{route('products.index')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.show_all_product')}}</span>
                                    </a>
                                </li>


                            </ul>
                        </li>
                        {{--orders--}}
                        <li class="nav-item  ">
                            <a href="{{route('orders.index')}}" class="nav-link nav-toggle">
                                <i class="fa fa-star"></i>
                                <span class="title">{{trans('backend.orders')}}</span>

                            </a>

                        </li>
                        {{--PRIVACIES--}}
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-shield"></i>
                                <span class="title">{{trans('backend.privacies')}}</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="{{route('privacy.create')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.add_privacy')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{route('privacy.index')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.show_all_privacies')}}</span>
                                    </a>
                                </li>


                            </ul>
                        </li>
                        {{--POLIES--}}
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-lightbulb-o"></i>
                                <span class="title">{{trans('backend.policies')}}</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="{{route('policy.create')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.add_policy')}}</span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="{{route('policy.index')}}" class="nav-link ">
                                        <span class="title">{{trans('backend.show_all_policies')}}</span>
                                    </a>
                                </li>


                            </ul>
                        </li>
                        {{--VISTORES Messages--}}
                         <li class="nav-item  ">
                                    <a href="{{route('visitorsMessages.index')}}" class="nav-link nav-toggle">
                                        <i class="fa fa-envelope"></i>
                                        <span class="title">{{trans('backend.visitors_messages')}}</span>

                                    </a>

                                </li>
                        {{--ABOUT--}}
                         <li class="nav-item  ">
                                                           <a href="javascript:;" class="nav-link nav-toggle">
                                                               <i class="icon-settings"></i>
                                                               <span class="title">{{trans('backend.setting')}}</span>
                                                               <span class="arrow"></span>
                                                           </a>

                                                           <ul class="sub-menu">

                                                               <li class="nav-item  ">
                                                                   <a href="{{route('about')}}" class="nav-link ">
                                                                       <span class="title">{{trans('backend.about')}}</span>
                                                                   </a>
                                                               </li>

                                                               {{--<li class="nav-item  ">--}}
                                                                   {{--<a href="{{route("privacies")}}" class="nav-link ">--}}
                                                                       {{--<span class="title">{{trans('backend.terme')}}</span>--}}
                                                                   {{--</a>--}}
                                                               {{--</li>--}}


                                                           </ul>
                                                       </li>
                        {{--SOCIAL MEDIA--}}
                         <li class="nav-item  ">
                                                          <a href="javascript:;" class="nav-link nav-toggle">
                                                              <i class="icon-social-dribbble font-black"></i>
                                                              <span class="title">{{trans('backend.social_media')}}</span>
                                                              <span class="arrow"></span>
                                                          </a>
                                                          <ul class="sub-menu">
                                                              <li class="nav-item  ">
                                                                  <a href="{{route('social-media.create')}}" class="nav-link ">
                                                                      <span class="title">{{trans('backend.add_social')}}</span>
                                                                  </a>
                                                              </li>
                                                              <li class="nav-item  ">
                                                                  <a href="{{route('social-media.index')}}" class="nav-link ">
                                                                      <span class="title">{{trans('backend.show_social')}}</span>
                                                                  </a>
                                                              </li>


                                                          </ul>
                                                      </li>






                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
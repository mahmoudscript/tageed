<!DOCTYPE html>
 @php  $lang = LaravelLocalization::getCurrentLocale();  @endphp
<html lang="{{ $lang }}" dir="{!! $lang == 'ar' ? 'rtl':'ltr' !!}">


    <head>
        <meta charset="utf-8" />
        <title>@yield('title')</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('backend/assets/fav.png') }}">

        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />


    @if($lang == "ar")

            <!-- needed for RTL -->

         {{Html::style('backend/assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css')}}
         {{Html::style('backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css')}}


        {{Html::style('backend/assets/global/css/plugins-rtl.min.css')}}
        {{Html::style('backend/assets/global/css/components-rounded-rtl.min.css')}}
        {{Html::style('backend/assets/layouts/layout4/css/custom-rtl.min.css')}}
         {{Html::style('backend/assets/layouts/layout4/css/layout-rtl.min.css')}}
         {{Html::style('backend/assets/layouts/layout4/css/themes/light-rtl.min.css')}}

         <link href="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />
   <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css" rel="stylesheet" type="text/css" />
    @else
         <!-- needed for LTR  -->

       {{Html::style('backend/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}
        {{Html::style('backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}


      {{Html::style('backend/assets/global/css/plugins.min.css')}}
      {{Html::style('backend/assets/global/css/components-rounded.min.css')}}
      {{Html::style('backend/assets/layouts/layout4/css/custom.min.css')}}
       {{Html::style('backend/assets/layouts/layout4/css/layout.min.css')}}
       {{Html::style('backend/assets/layouts/layout4/css/themes/light.min.css')}}
   <link href="{{Request::root()}}/backend/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
   <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    @endif
   <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
   <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />



                 <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- BEGIN PAGE LEVEL PLUGINS -->
                <link href="{{Request::root()}}/backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
                <!-- END PAGE LEVEL PLUGINS -->
        {{ Html::style('backend/assets/global/plugins/font-awesome/css/font-awesome.min.css') }}
        {{Html::style('backend/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}


        {{Html::style('backend/assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}

       @yield('extra_css')






        <link rel="shortcut icon" href="favicon.ico" />



         </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
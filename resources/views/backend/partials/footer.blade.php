  </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->


        </div>
        <!-- END CONTAINER -->




        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> 2017 &copy; By
                <a href="http:\\www.tatweers.com" title="Purchase Metronic just for 27$ and get lifetime updates for free" target="_blank">Tatweer !</a>
            </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->

      @php   $lang = LaravelLocalization::getCurrentLocale();  @endphp


  @if($lang == "ar")

            <!-- needed for RTL -->

  @else




  @endif


  <!-- start the code for notifications -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>




  <script type="text/javascript">
      var notificationsWrapper   = $('.dropdown-notification');
     // var notificationsToggle    = notificationsWrapper.find('a[data-toggle]');
     // var notificationsCountElem = notificationsToggle.find('i[data-count]');
    //  var notificationsCount     = parseInt(notificationsCountElem.data('count'));
      var notifications          = notificationsWrapper.find('ul.dropdown-menu-list');

//      if (notificationsCount <= 0) {
//          notificationsWrapper.hide();
//      }

      // Enable pusher logging - don't include this in production
      // Pusher.logToConsole = true;

      var pusher = new Pusher('7d3826992e8ae292a768', {
          encrypted: true  ,
          cluster : 'us2'
          
      });

      // Subscribe to the channel we specified in our Laravel Event
      var channel = pusher.subscribe('championship_reservation');

      // Bind a function to a Event (the full Laravel class)
      channel.bind('App\\Events\\championshipReservationConfirmation', function(data) {


          console.log(data);
          
          var existingNotifications = notifications.html();
//          var avatar = Math.floor(Math.random() * (71 - 20 + 1)) + 20;
          var newNotificationHtml = `
          <li class="notification active">
              <div class="media">
                <div class="media-left">
                  <div class="media-object">
                    <img src="https://api.adorable.io/avatars/71/`+avatar+`.png" class="img-circle" alt="50x50" style="width: 50px; height: 50px;">
                  </div>
                </div>
                <div class="media-body">
                  <strong class="notification-title">`+data.message+`</strong>
                  <!--p class="notification-desc">Extra description can go here</p-->
                  <div class="notification-meta">
                    <small class="timestamp">about a minute ago</small>
                  </div>
                </div>
              </div>
          </li>
        `;
          notifications.html(newNotificationHtml + existingNotifications);

          notificationsCount += 1;
          notificationsCountElem.attr('data-count', notificationsCount);
          notificationsWrapper.find('.notif-count').text(notificationsCount);
          notificationsWrapper.show();
      });
  </script>





  
  <!-- end the code for notifications     -->
  {{ Html::script('backend/assets/global/plugins/respond.min.js') }}
           {{ Html::script('backend/assets/global/plugins/excanvas.min.js') }}
        <!-- BEGIN CORE PLUGINS -->

          {{ Html::script('backend/assets/global/plugins/jquery.min.js') }}
          {{ Html::script('backend/assets/global/plugins/bootstrap/js/bootstrap.min.js') }}
          {{ Html::script('backend/assets/global/plugins/js.cookie.min.js') }}
          {{ Html::script('backend/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') }}
          {{ Html::script('backend/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}
          {{ Html::script('backend/assets/global/plugins/jquery.blockui.min.js') }}
          {{ Html::script('backend/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}
          {{ Html::script('backend/assets/global/scripts/app.min.js') }}
          {{ Html::script('backend/assets/layouts/layout4/scripts/layout.min.js') }}
          {{ Html::script('backend/assets/layouts/layout4/scripts/demo.min.js') }}
          {{ Html::script('backend/assets/layouts/global/scripts/quick-sidebar.min.js') }}
          <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
         <script src="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>

        <script src="{{Request::root()}}/backend/assets/pages/scripts/form-dropzone.min.js" type="text/javascript"></script>

         <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
           @yield('extra_js')
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="{{Request::root()}}/backend/assets/pages/scripts/form-dropzone.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->
  <script src="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>


  <script src="{{Request::root()}}/backend/assets/global/plugins/moment.min.js" type="text/javascript"></script>
  <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
  <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
  <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
  <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
  <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="{{Request::root()}}/backend/assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
  <!-- END PAGE LEVEL SCRIPTS -->


  <!-- BEGIN PAGE LEVEL SCRIPTS -->
           {{--{{ Html::script('backend/assets/pages/scripts/form-validation-md.min.js') }}--}}
           <!-- END PAGE LEVEL SCRIPTS -->

    </body>

</html>
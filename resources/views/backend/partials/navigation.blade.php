

 @php

     $lang = LaravelLocalization::getCurrentLocale();

 @endphp
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top ">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="{{Request::root()}}">
                        <img src="{{Request::root()}}/backend/assets/layouts/layout4/img/logo-light.png"  alt="logo" class="logo-default" /> </a>
                    <div class="menu-toggler sidebar-toggler">

                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->

                <div class="page-top">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="separator hide"> </li>



                            
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            {{--<li class="dropdown dropdown-extended dropdown-notification dropdown-light" id="header_notification_bar">--}}
                                {{--<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">--}}
                                    {{--<i class="icon-bell"></i>--}}

                                    {{--@if(Auth::user()->unreadNotifications->count() > 0 )--}}
                                    {{--<span class="badge badge-success">{{ Auth::user()->unreadNotifications->count()}}  </span>--}}
                                        {{--@endif--}}
                                {{--</a>--}}
                                {{--<ul class="dropdown-menu">--}}

                                    {{--<li>--}}
                                        {{--<ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">--}}

                                            {{--@if(Auth::user()->unreadNotifications->count() <= 0)--}}
                                               {{----}}
                                                {{--<li>--}}
                                                    {{--<a href="javascript:;">--}}
                                                        {{--<span class="time"></span>--}}
                                                        {{--<span class="details">--}}
                                                            {{----}}
                                                        {{--</span> {{ trans('backend.notifications_count_zero') }}</span>--}}
                                                    {{--</a>--}}
                                                {{--</li>--}}
                                            {{--@endif--}}

                                           {{----}}

                                        {{--</ul>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</li>--}}
                            {{--<!-- END NOTIFICATION DROPDOWN -->--}}




                            <li class="separator hide"> </li>
                            <!-- BEGIN INBOX DROPDOWN -->

                            <li class="separator hide"> </li>

                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user dropdown-light">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> {{trans('backend.admins')}} </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="" class="img-circle" src="{{Request::root()}}/backend/assets/layouts/layout4/img/admin.jpg" /> </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                   


                                    <li>
                                        <a href="{{ route('logout') }}"
                                         onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                            <i class="icon-key"></i> {{trans('backend.logout')}} </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                                        {{ csrf_field() }}
                                                                                    </form>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN LANGUAGE BAR -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-language">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" src="{{asset("backend/assets/layouts/layout4/img/$lang.png")}}">
                                    <span class="langname"> {{$lang}} </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu">

                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                    <li>
                                        <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">

                                           <img width="20px"  src='{{ asset("/backend/assets/layouts/layout4/img/$localeCode.png") }}' alt="">
                                                                                                   {{ $properties['native'] }}
                                             </a>
                                    </li>
                                @endforeach

                                </ul>
                            </li>
                            <!-- END LANGUAGE BAR -->

                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">


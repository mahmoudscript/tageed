@extends('backend.app')

@section('page_title' ,trans('backend.products'))

@section('breadcrumb')


         <li>
           <a href="{{route('products.index')}}">{{trans('backend.products')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.add_product')}}</span>
         </li>


@endsection

@section('extra_css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
 <!-- END PAGE LEVEL PLUGINS -->
{{--<link rel="stylesheet" href="{{Request::root()}}/backend/dropify/dist/css/demo.css">--}}

<link href="{{Request::root()}}/backend/dropify/dist/css/dropify.css" rel="stylesheet" type="text/css" />

@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();

    $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;




@endphp
@section('content')


<div class="portlet box blue">

   <div class="portlet-body">
      <div class="tabbable-custom nav-justified">
         <ul class="nav nav-tabs nav-justified">
            @foreach($languages as $lang)
            <li class="{{$loop->iteration == 1 ? 'active' : '' }}">
               <a href="#{{$lang->label}}" data-toggle="tab" aria-expanded="true"> {{strtoupper($lang->label)}}</a>
            </li>
            @endforeach
         </ul>
         <form action="{{route('products.store')}}" method="post" id="form_sample_2" enctype="multipart/form-data">
         {{csrf_field()}}
            <div class="tab-content">
               @foreach($languages as $lang)
               <div class="tab-pane {{$loop->iteration == 1 ? 'active' : '' }} " id="{{$lang->label}}">

                  <div class="form-group form-md-line-input {{ $errors->has("name_$lang->label") ? ' has-error' : '' }}">
                     <input type="text" class="form-control" name="name_{{$lang->label}}" id="form_control_1" placeholder="{{trans('backend.product_name')}}">
                     <label for="form_control_1">{{trans('backend.product_name')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("name_$lang->label"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("name_$lang->label") }}</strong>
                                    </span>
                      @endif
                  </div>


                  <div class="form-group form-md-line-input {{ $errors->has("addition_info_$lang->label") ? ' has-error' : '' }}">
                     <textarea type="text" class="form-control" name="desc_{{$lang->label}}" id="form_control_1" >
                     </textarea>
                     <label for="form_control_1">{{trans('backend.product_description')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("desc_$lang->label"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("desc_$lang->label") }}</strong>
                                    </span>
                      @endif
                  </div>

             <div class="form-group form-md-line-input {{ $errors->has("addition_info_$lang->label") ? ' has-error' : '' }}">
                     <textarea type="text" class="form-control" name="addition_info_{{$lang->label}}" id="form_control_1" >
                     </textarea>
                     <label for="form_control_1">{{trans('backend.product_addition_info')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("addition_info_$lang->label"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("addition_info_$lang->label") }}</strong>
                                    </span>
                      @endif
                  </div>




               </div>
               @endforeach

                   {{-- quanitity --}}

                   <div class="form-group form-md-line-input  {{ $errors->has("quantity") ? ' has-error' : '' }}">
                       <input type="text" class="form-control" name="quantity" id="form_control_1" >
                       </input>
                       <label for="form_control_1">{{trans('backend.product_quantity')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("quantity"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("quantity") }}</strong>
                                    </span>
                       @endif
                   </div>
                   {{-- price --}}

                   <div class="form-group form-md-line-input  {{ $errors->has("price") ? ' has-error' : '' }}">
                       <input type="text" class="form-control" name="price" id="form_control_1" >
                       </input>
                       <label for="form_control_1">{{trans('backend.product_price')}}
                           <span class="required">*</span>
                       </label>
                       @if ($errors->first("price"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("price") }}</strong>
                                    </span>
                       @endif
                   </div>

                   
                   {{-- Account Number --}}




                   {{-- CATEGEROY--}}
                   <div class="form-group form-md-line-input">
                       <label class="form_control_1"> {{trans('backend.department_name')}}</label>

                       <select class="form-control" name="cat_id">

                           @foreach($departments as $department)
                               @foreach($department->description as $description)
                                   @if($language_id==$description->language_id)
                                       <option value="{{$department->id}}">{{$description->name}}</option>
                                   @endif
                               @endforeach
                           @endforeach

                       </select>

                   </div>
                   {{-- BRAND--}}
                   <div class="form-group form-md-line-input">
                       <label class="form_control_1"> {{trans('backend.brand_name')}}</label>

                       <select class="form-control" name="brand_id">

                           @foreach($brands as $brand)
                               @foreach($brand->description as $description)
                                   @if($language_id==$description->language_id)
                                       <option value="{{$brand->id}}">{{$description->title}}</option>
                                   @endif
                               @endforeach
                           @endforeach

                       </select>

                   </div>

                   {{-- STATUS--}}
                   <div class="form-group form-md-line-input">
                       <label class="form_control_1"> {{trans('backend.product_Status')}}</label>

                       <select class="form-control" name="status">

                          <option value="published">{{trans('backend.publish')}}</option>
                          <option value="not_published">{{trans('backend.not_publish')}}</option>

                       </select>

                   </div>


                   {{-- SPECIAL--}}

                   <div class="form-group form-md-line-input">
                       <label class="form_control_1"> {{trans('backend.product_special')}}</label>

                       <select class="form-control" name="special">

                           <option value="0">{{trans('backend.special')}}</option>
                           <option value="1">{{trans('backend.not_special')}}</option>

                       </select>

                   </div>
                   <div class="form-group form-md-line-input  {{ $errors->has("images") ? ' has-error' : '' }}">
                       <label class="form_control_1"> {{trans('backend.product_images')}}</label>

                       <div class="fallback">
                           <input class="dropify" data-default-file="{{trans('backend.product_images')}}" name="images[]" type="file" multiple />

                       </div>

                       @if ($errors->first("images"))
                           <span class="help-block">
                                        <strong>{{ $errors->first("images") }}</strong>
                                    </span>
                       @endif
                   </div>

                   





                   


                   <div class="form-actions">

                  <div class="row">
                     <div class="col-md-12">
                        <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                        <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>



@endsection

@section('extra_js')

    <script src="{{Request::root()}}/backend/dropify/dist/js/dropify.js" type="text/javascript"></script>

    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>




@endsection
@extends('backend.app')

@section('page_title' ,trans('backend.products'))

@section('breadcrumb')


    <li>
        <a href="{{route('products.index')}}">{{trans('backend.products')}}</a>
        <i class="fa fa-circle"></i>
    </li>
    <li>
        <span class="active">{{trans('backend.products')}}</span>
    </li>


@endsection
@php

    $lang = LaravelLocalization::getCurrentLocale();
     $laguage = \App\Language::where(['label'=>$lang])->first();


    $language_id = $laguage->id ;





@endphp


@section('extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    @if($lang=='ar')
    <link href="{{Request::root()}}/backend/assets/pages/css/blog-rtl.min.css" rel="stylesheet" type="text/css" />
    @else
        <link href="{{Request::root()}}/backend/assets/pages/css/blog.min.css" rel="stylesheet" type="text/css" />

    @endif

@endsection

@section('content')


    <div class="portlet box blue">

        <div class="portlet-body">
            <div class="tabbable-custom nav-justified">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="white-box">
                            <div class="">
                                <h2 class="m-b-0 m-t-0">

                                            {{$product->title}}


                                </h2>
                                <hr>
                                <div class="row">

                                    <div class="col-md-3 col-sm-6 col-xs-12 margin-bottom-5">
                                        <!-- BEGIN WIDGET WRAP IMAGE -->
                                        <div id="carousel-example-generic-v1" class="carousel slide widget-carousel" data-ride="carousel">
                                            <!-- Indicators -->

                                            <ol class="carousel-indicators carousel-indicators-red">
                                                @if($product->images)
                                                    @foreach($product->images as $des)
                                                        <li data-target="#carousel-example-generic-v2" data-slide-to="{{ $loop->iteration }}" class="circle {{$loop->iteration ==1 ?'active':''}}"></li>
                                                    @endforeach
                                                @endif
                                            </ol>
                                            <!-- Wrapper for slides -->
                                            <div class="carousel-inner" role="listbox">
                                                @if($product->images)
                                                    @foreach($product->images as $img)
                                                        <div class="item {{$loop->iteration == 1 ?'active':''}}">
                                                            <div class="" >

                                                                <img style="border-radius: 25px;" class="widget-wrap-img-element img-responsive" src="{{Request::root()}}/uploads/product_images/{{$img->name}}" alt=""> </div>
                                                        </div>
                                                    @endforeach
                                                @endif


                                            </div>
                                        </div>
                                        <!-- END WIDGET WRAP IMAGE -->
                                    </div>



                                    <div class="col-lg-9 col-md-9 col-sm-6">
                                        <h4 class="box-title m-t-40">{{trans('backend.product_description')}}</h4>
                                        <p>
                                                    {{$product->description}}
                                        </p>


                                        
                                        
                                    </div>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <h3 class="box-title m-t-40"></h3>
                                        <div class="table-responsive">
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>






@endsection

@section('extra_js')
    <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
@endsection
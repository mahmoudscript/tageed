@extends('backend.app')

@section('page_title' ,trans('backend.shipping_information'))

@section('breadcrumb')


         <li>
           <a href="{{route('shipping.index')}}">{{trans('backend.shipping_information')}}</a>
            <i class="fa fa-circle"></i>
         </li>
         <li>
          <span class="active">{{trans('backend.add_shipping_information')}}</span>
         </li>


@endsection

@section('extra_css')
<!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />
        <link href="{{Request::root()}}/backend/assets/global/plugins/dropzone/basic.min.css" rel="stylesheet" type="text/css" />
 <!-- END PAGE LEVEL PLUGINS -->

@endsection


@section('content')


<div class="portlet box blue">

   <div class="portlet-body">
      <div class="tabbable-custom nav-justified">
         <ul class="nav nav-tabs nav-justified">
            @foreach($languages as $lang)
            <li class="{{$loop->iteration == 1 ? 'active' : '' }}">
               <a href="#{{$lang->label}}" data-toggle="tab" aria-expanded="true"> {{strtoupper($lang->label)}}</a>
            </li>
            @endforeach
         </ul>
         <form action="{{route('shipping.store')}}" method="post" id="form_sample_2" enctype="multipart/form-data">
         {{csrf_field()}}
            <div class="tab-content">
               @foreach($languages as $lang)


               {{-- it send both of names ( ar , en ) --}}

               <div class="tab-pane {{$loop->iteration == 1 ? 'active' : '' }} " id="{{$lang->label}}">

                  <div class="form-group form-md-line-input {{ $errors->has("name_$lang->label") ? ' has-error' : '' }}">
                     <input type="text" class="form-control" name="name_{{$lang->label}}" id="form_control_1" >
                     <label for="form_control_1">{{trans('backend.title')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("name_$lang->label"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("name_$lang->label") }}</strong>
                                    </span>
                      @endif
                  </div>

                  <div class="form-group form-md-line-input {{ $errors->has("desc_$lang->label") ? ' has-error' : '' }}">
                     <textarea type="text" class="form-control" name="desc_{{$lang->label}}" id="form_control_1" >
                     </textarea>
                     <label for="form_control_1">{{trans('backend.description')}}
                     <span class="required">*</span>
                     </label>
                      @if ($errors->first("desc_$lang->label"))
                          <span class="help-block">
                                        <strong>{{ $errors->first("desc_$lang->label") }}</strong>
                                    </span>
                      @endif
                  </div>



               </div>
               @endforeach
               

               <div class="form-actions">

                  <div class="row">
                     <div class="col-md-12">
                        <button type="submit" class="btn green">{{trans('backend.save')}}</button>
                        <button type="reset" class="btn default">{{trans('backend.cancel')}}</button>
                     </div>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>



@endsection
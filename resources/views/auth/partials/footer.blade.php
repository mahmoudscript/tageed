 </div>
        <!-- END LOGIN -->
        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"> 2017 &copy; Tatweers. </div>
        <!-- END COPYRIGHT -->
        <!--[if lt IE 9]>
<script src="{{Request::root()}}/backend/assets/global/plugins/respond.min.js"></script>
<script src="{{Request::root()}}/backend/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{Request::root()}}/backend/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{Request::root()}}/backend/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="{{Request::root()}}/backend/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{Request::root()}}/backend/assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{Request::root()}}/backend/assets/pages/scripts/login-4.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
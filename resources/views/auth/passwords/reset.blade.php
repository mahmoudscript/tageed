@extends('layouts.app')

@section('content')

<!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="forget-form" action="{{ route('password.request') }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">
                <h3>Forget Password ?</h3>
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="input-icon">
                        <i class="fa fa-envelope"></i>
                        <input class="form-control placeholder-no-fix" value="{{ $email or old('email') }}" type="text" autocomplete="off" placeholder="Email" name="email" /> </div>
                                              @if ($errors->has('email'))
                                                   <span class="help-block">
                                                       <strong>{{ $errors->first('email') }}</strong>
                                                   </span>
                                               @endif

                </div>
                <div class="form-actions">
                    <button type="button" id="back-btn" class="btn red btn-outline">Back </button>
                    <button type="submit" class="btn green pull-right"> Submit </button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
@endsection

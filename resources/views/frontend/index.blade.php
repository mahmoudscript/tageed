@extends('frontend.app')


@section('content')
@php
$lang = LaravelLocalization::getCurrentLocale();
$laguage = \App\Language::where(['label'=>$lang])->first();
$language_id = $laguage->id ;
@endphp
<div class="banner">
    <div class="background">
        <img src="{{Request::root()}}/frontend/img/banner.jpg" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="banner-content">
                    <h2>Buy & Sell Marketplace</h2>
                    <p>It is a long established fact that a reader will be distracted by the readable content </p>
                    <div class="form-search " >

                         {{-- From --}}
                        {!! Form::open(['url'=>["$lang/searchresult"],'method'=>'POST','class'=>''  
                        ,'role'=>'form','files'=> true ,'id'=>'add_files']) !!}


                            <div class="row">
                                <div  class="col-md-4 col-sm-6 col-xs-12">
                                    <input  type="text" name="keywords" id="" placeholder="Enter Keywords" class="form-control">
                                </div>
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <select class="form-control" name="department_id">
                                        @foreach($searchdepartments as $department)
                                        <option value="{{$department->id}}" >
                                            @foreach($department->description as $desc)
                                @if($desc->language_id == $language_id)
                                    {{ $desc->name }}
                                @endif
                            @endforeach
                                        </option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                                
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <input type="submit"  value="Search" class="form-control search-submit">
                                </div>
                                
                            </div>


                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="category-icon">
    <div class="container">
        <div class="col-12 text-center latest-adds-title">
            <h2> Last {{ trans('frontend.catergory') }}</h2>
            <br>
        </div>
        <div class="row category-icon-row">
            @foreach($departments as $department)
            <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="category-icon-block">
                    <img src="{{Request::root()}}/uploads/departments/{{$department->photo}}" alt="">
                    <a href="{{ url("/catadvertsdetails/$department->id") }}">
                        @foreach($department->description as $desc)
                        @if($desc->language_id == $language_id)
                        {{ $desc->name }}
                        @endif
                        @endforeach
                    </a>
                </div>
            </div>
            @endforeach
            
        </div>
        <div class="text-center">
            <br>
            <a style="font-size:16px" href="{{ url('/catadverts') }}" class="btn btn-info"> All Categories </a>
        </div>
        
        
        
    </div>
    
</div>
<div class="latest-adds">
    <div class="container">
        <div class="row no-margin">
            <div class="text-center latest-adds-title">
                <h2>Latest Ads</h2>
                <p>Sell & Purchase Anything</p>
            </div>
            <div class="latest-adds-tabs">
                <div class="row no-margin">
                    <div class="col-sm-9 text-left">
                        <p class="latest-adds-tabs-botton">
                            Sort By:
                            
                        </p>
                    </div>
                    <div class="col-sm-3 group-type text-right">
                        <span href="{{url('/ad_details')}}" class="group-type-item group-type-item-list"><i class="fa fa-list" aria-hidden="true"></i></span>
                        <span href="{{url('/ad_details')}}" class="group-type-item group-type-item-grid active-list"><i class="fa fa-th" aria-hidden="true"></i></span>
                    </div>
                </div>
            </div>
            <div class="row latest-adds-items">
                {{-- item data  --}}
                @foreach($adverts as $advert)
                
                <div class=" shuffle-item col-lg-4 col-md-4 col-sm-6" >
                    <div class="latest-ads-item">
                        @foreach($advert->images as $img)
                        <img src="{{Request::root()}}/uploads/product_images/{{$img->name}}" alt="">
                        @endforeach
                        <span class="latest-ads-price"> {{ $advert->price }}</span>
                        
                        <span class="latest-ads-category red">
                            
                            @foreach($advert->department->description as $desc)
                            @if($desc->language_id == $language_id)
                            {{ $desc->name }}
                            @endif
                            @endforeach
                        </span>
                        <a href="{{url("/ad_details/$advert->id")}}" class="latest-ads-name">
                            
                            {{ $advert->title   }}
                        <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                        <span  class="latest-ads-time">
                            {{ date('Y-m-d' , strtotime($advert->created_at)) }}
                        </span>
                        
                    </div>
                </div>
                @endforeach
                
            </div>
        </div>
        
    </div>
</div>
<br>
@endsection
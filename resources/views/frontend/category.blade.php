@extends('frontend.app')


@section('content')

    @php
        $lang = LaravelLocalization::getCurrentLocale();

               $laguage = \App\Language::where(['label'=>$lang])->first();


               $language_id = $laguage->id ;
    @endphp


    <div class="banner">
        <div class="background">
            <img src="{{Request::root()}}/frontend/img/banner.jpg" alt="">
        </div>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <div class="banner-content">
                        <h2>Buy & Sell Marketplace</h2>
                        <p>It is a long established fact that a reader will be distracted by the readable content </p>
                        <div class="form-search">
                            <form action="#">
                                <div class="row">
                                    <div class="col-lg-3 col-md-6 col-sm-12"><input type="text" name="" id="" placeholder="Enter Keywords" class="form-control"></div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <select class="form-control">
                                            <option value="">Category</option>
                                            <option value="">Fashion</option>
                                            <option value="">Mobile Phones</option>
                                            <option value="">Electronics</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <select class="form-control">
                                            <option value="">Location</option>
                                            <option value="">place </option>
                                            <option value="">place 2</option>
                                            <option value="">Place 3</option>
                                        </select>
                                    </div>
                                    <div class="col-lg-3 col-md-6 col-sm-12">
                                        <input type="submit"  value="Search" class="form-control search-submit">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="category-icon">
        <div class="container">
            <div class="row category-icon-row">
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-1.png" alt="">
                        <a href="{{url('/user_ads')}}">Real Estate</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-2.png" alt="">
                        <a href="{{url('/user_ads')}}">Vehicles</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-3.png" alt="">
                        <a href="#">Mobile Phone</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-4.png" alt="">
                        <a href="#">Furniture</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-5.png" alt="">
                        <a href="#">Services</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-6.png" alt="">
                        <a href="#">Electronics</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-7.png" alt="">
                        <a href="#">Jobs</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-8.png" alt="">
                        <a href="#">Fashion</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-9.png" alt="">
                        <a href="#">Animal</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-10.png" alt="">
                        <a href="#">Education</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-11.png" alt="">
                        <a href="#">Baby Toys</a>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-4">
                    <div class="category-icon-block">
                        <img src="{{Request::root()}}/frontend/img/home-icone-12.png" alt="">
                        <a href="#"> Matrimonials</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="category-content">
        <div class="container">
            <div class="row no-margin">
                <div class="col-lg-9 col-md-8">
                    <div class="latest-adds">
                        <div class="container">
                            <div class="row no-margin">
                                <div class="col-12 latest-adds-tabs">
                                    <div class="row no-margin">
                                        <div class="col-9 text-left">
                                            <p class="latest-adds-tabs-botton">
                                                Sort By:
                                                <span data-class="all" class="activeTab">all</span>
                                                <span data-class=".Date" >Date</span>
                                                <span data-class=".Title" >Title</span>
                                                <span data-class=".Price" >Price</span>
                                            </p>
                                        </div>
                                        <div class="col group-type text-right">
                                            <span href="#" class="group-type-item group-type-item-list"><i class="fa fa-list" aria-hidden="true"></i></span>
                                            <span href="#" class="group-type-item group-type-item-grid active-list"><i class="fa fa-th" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row latest-adds-items">
                                    <div class="Date shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6" >
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555,000 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                                            <span class="latest-ads-category red">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                    <div class="Title shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                                            <span class="latest-ads-category blue">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                    <div class="Title shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                                            <span class="latest-ads-category blue">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                    <div class="Date shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                                            <span class="latest-ads-category red-2">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                    <div class="Date shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                                            <span class="latest-ads-category red-2">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                    <div class="Price shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555,000 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                                            <span class="latest-ads-category red">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                    <div class="Price shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                                            <span class="latest-ads-category blue">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                    <div class="Price shuffle-item col-xl-4 col-lg-6 col-md-6 col-sm-6">
                                        <div class="latest-ads-item">
                                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                                            <span class="latest-ads-price">555 EGP</span>
                                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                                            <span class="latest-ads-category blue">Fashion</span>
                                            <a href="{{url('/user_ads')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                                            <span class="latest-ads-time">Today 12:58</span>
                                            <span class="latest-ads-place">Giza</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col text-center">
                                    <a href="#" class="load-more">LOAD MORE</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4">
                    <div class="advanced-search">
                        <h2><i class="fa fa-search"></i> Advanced Search</h2>
                        <hr>
                        <h3><i class="fa fa-tag" aria-hidden="true"></i> Keywords</h3>
                        <hr>
                        <form action="" class="form-keywords">
                            <input type="text" class="form-control" placeholder="Enter Keyword">
                            <input type="submit" class="keyword-submit">
                        </form>
                        <hr>
                        <h3><i class="fa fa-map-marker"></i> Location</h3>
                        <hr>
                        <form action="">
                            <select class="custom-select">
                                <option value="">Select Country</option>
                                <option value="">Country 1</option>
                                <option value="">Country 2</option>
                                <option value="">Country 3</option>
                            </select>
                            <select class="custom-select">
                                <option value="">Select State</option>
                                <option value="">State 1</option>
                                <option value="">State 2</option>
                                <option value="">State 3</option>
                            </select>
                            <select class="custom-select">
                                <option value="">Select city</option>
                                <option value="">city 1</option>
                                <option value="">city 2</option>
                                <option value="">city 3</option>
                            </select>
                            <hr>
                            <h3><i class="fa fa-folder-open"></i> Category</h3>
                            <hr>
                            <select class="custom-select mb-3">
                                <option value="">Select Category</option>
                                <option value="">Category 1</option>
                                <option value="">Category 2</option>
                                <option value="">Category 3</option>
                            </select>

                            <p>Type of ad</p>
                            <label for="all"> <input type="radio" name="category" id="all" checked> All</label>
                            <label for="for-sale"> <input type="radio" name="category" id="for-sale"> For sale</label>
                            <label for="Wanted"> <input type="radio" name="category" id="Wanted"> Wanted</label>
                            <label for="for-rent"> <input type="radio" name="category" id="for-rent"> For Rent</label>
                            <hr>
                            <input type="submit" value="Search" class="form-control submit-search">
                        </form>
                    </div>
                    <div class="latest-ad-posts margin-top">
                        <h2>Latest Ads </h2>
                        <div class="media">
                            <img class="align-self-center mr-3" src="{{Request::root()}}/frontend/img/letast-post.jpg" alt="Generic placeholder image">
                            <div class="media-body">
                                <p class="mb-0">$60</p>
                                <h5 class="mt-0 mb-0"><a href="#">Title of the item</a></h5>
                                <p>Category: Fashion</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media">
                            <img class="align-self-center mr-3" src="{{Request::root()}}/frontend/img/letast-post.jpg" alt="Generic placeholder image">
                            <div class="media-body">
                                <p class="mb-0">$60</p>
                                <h5 class="mt-0 mb-0"><a href="#">Title of the item</a></h5>
                                <p>Category: Fashion</p>
                            </div>
                        </div>
                        <hr>
                        <div class="media">
                            <img class="align-self-center mr-3" src="{{Request::root()}}/frontend/img/letast-post.jpg" alt="Generic placeholder image">
                            <div class="media-body">
                                <p class="mb-0">$60</p>
                                <h5 class="mt-0 mb-0"><a href="#">Title of the item</a></h5>
                                <p>Category: Fashion</p>
                            </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection


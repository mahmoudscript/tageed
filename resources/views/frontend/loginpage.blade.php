@extends('frontend.app')


@section('content')

    @php
        $lang = LaravelLocalization::getCurrentLocale();

               $laguage = \App\Language::where(['label'=>$lang])->first();


               $language_id = $laguage->id ;
    @endphp



    <div class="banner-pages">
        <div class="background"><img src="{{Request::root()}}/frontend/img/banner-pages.jpg" alt=""></div>
    </div>
    <div class="content-pages">
        <div class="container">
            <div class="ba-color">
                <div class="row">
                    <div class="col">
                        <div class="register-content text-center">
                            <h2 class="register-title"> login  <i class="fa fa-caret-down"></i></h2>
                       

                            {!! Form::open(['url'=>["$lang/loginform"],'method'=>'POST','class'=>''  ,'role'=>'form','files'=> true ,'id'=>'add_files']) !!}
                                 
                                <input  required="required" type="email" name="email" placeholder="Email" class="form-control">
                                <input required="required" type="password" name="password" placeholder="Password" class="form-control">
                                <input type="submit" value="login" class="form-control submit">
                                <p><i class="fa fa-user"></i> Already not a member? <a href="{{ url('/register') }}"> Create new account</a></p>
                                <hr>
                                

                            {!! Form::close() !!}
                      {{-- end Form --}}
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>






@endsection


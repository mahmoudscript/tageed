@extends('frontend.app')


@section('content')

@php
$lang = LaravelLocalization::getCurrentLocale();

       $laguage = \App\Language::where(['label'=>$lang])->first();


       $language_id = $laguage->id ;
@endphp


<div class="banner-pages">
    <div class="background">
    	<img src="{{Request::root()}}/frontend/img/banner-pages.jpg" alt="">
    </div>
    <div class="container">
    	<div class="row">
    		<div class="div col-xs-12">
    			<h2 class="page-title-cat">Categories</h2>
    		</div>
    	</div>
    </div>				
</div>
<div class="content-pages">
     <div class="container">
         <div class="ba-color">
         	<div class="content-category">
         		<div class="row">
	                 <div class="col-xs-12"> 
	                 	<h2 class="content-title"><i class="fa fa-th" aria-hidden="true"></i>Categories</h2>
	                 </div>
             	</div>
             	<div class="row">
             		<div class="col-sm-12">
             			
                            @foreach ($cats as $cat)
                                 
                            
                        
             			<h3 class="category-links col-sm-4"><a href="{{url("catadvertsdetails/$cat->id")}}" title="">
                            <i class="fa fa-list"></i> 
                            @foreach($cat->description as $desc)
                                @if($desc->language_id == $language_id)
                                    {{ $desc->name }}
                                @endif
                            @endforeach

                        </a></h3>


                             @endforeach
             			
             		</div>
             		 
             		 
             		
             	</div>			
         	</div>
         </div>

     </div>
 </div>





@endsection




@extends('frontend.app')


@section('content')

@php
$lang = LaravelLocalization::getCurrentLocale();

       $laguage = \App\Language::where(['label'=>$lang])->first();


       $language_id = $laguage->id ;



@endphp


<div class="banner-pages">
    <div class="background">
    	<img src="{{Request::root()}}/frontend/img/banner-pages.jpg" alt="">
    </div>
    <div class="container">
    	<div class="row">
    		<div class="div col-xs-12">
    			<h2 class="page-title-cat">Categories</h2>
    		</div>
    	</div>
    </div>				
</div>
<div class="content-pages">
     <div class="container">
         <div class="ba-color">
         	<div class="content-category">
         		<div class="row">
	                 <div class="col-xs-12"> 
	                 	<h2 class="content-title"><i class="fa fa-th" aria-hidden="true"></i>Categorie Adverts</h2>
	                 </div>
             	</div>
                <div class="latest-adds-tabs">
                    <div class="row no-margin">
                        <div class="col-sm-9 text-left">
                            <p class="latest-adds-tabs-botton">
                                
                                 
                            </p>
                        </div>
                        <div class="col-sm-3 group-type text-right">
                            <span href="{{url('/ad_details')}}" class="group-type-item group-type-item-list"><i class="fa fa-list" aria-hidden="true"></i></span>
                            <span href="{{url('/ad_details')}}" class="group-type-item group-type-item-grid active-list"><i class="fa fa-th" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>


                        @if(count($adverts) > 0)

                <div class="row latest-adds-items">

                     {{-- item data  --}}
                    @foreach($adverts as $advert)
 
                    <div class=" shuffle-item col-lg-4 col-md-4 col-sm-6" >
                        <div class="latest-ads-item">
                            @foreach($advert->images as $img)
                            <img src="{{Request::root()}}/uploads/product_images/{{$img->name}}" alt="">
                            @endforeach

                            <span class="latest-ads-price"> {{ $advert->price }}</span>
                            
                            <span class="latest-ads-category red">
                            
                            @foreach($advert->department->description as $desc)
                                @if($desc->language_id == $language_id)
                                    {{ $desc->name }}
                                @endif
                            @endforeach

                        </span>
                            <a href="{{url("/ad_details/$advert->id")}}" class="latest-ads-name">

                                 
                                {{ $advert->title   }}

                                <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span  class="latest-ads-time">
                                {{ date('Y-m-d' , strtotime($advert->created_at)) }}
                            </span>
                             
                        </div>
                    </div>
                    @endforeach

                    {{-- end item --}}

                </div>

                @else

                <h3 class="text-center"> No items in this Categorie . </h3>

                @endif
             			
         	</div>
         </div>

     </div>
 </div>





@endsection




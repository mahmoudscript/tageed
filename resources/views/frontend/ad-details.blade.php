@extends('frontend.app')


@section('content')

    @php
        $lang = LaravelLocalization::getCurrentLocale();

               $laguage = \App\Language::where(['label'=>$lang])->first();


               $language_id = $laguage->id ;

                $catid = $advert->department->id ; 
    @endphp

    <div class="banner-pages">
        <div class="background"><img src="{{Request::root()}}/frontend/img/banner-pages.jpg" alt=""></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="banner-title">ad details</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row ad-details-header">
            <div class="col-md-8 col-lg-8 col-sm-12">
                <h1>{{ $advert->title }}</h1>
                <span> {{ date('Y-m-d' , strtotime($advert->created_at)) }}</span>
                <span>Category: <a href="{{ url("/catadvertsdetails/$catid") }}">  
                  @foreach($advert->department->description as $desc)
                                @if($desc->language_id == $language_id)
                                    {{ $desc->name }}
                                @endif
                            @endforeach</a></span>
                <span>Views: 
                    @foreach($advert->viewCount as $view)
                    {{ $view->viewCount }}
                    @endforeach
                </span>
            </div>
            <div class="col-md-4 col-lg-4 col-sm-12">
                <p class="text-md-right text-sm-left mb-0"><span class="ad-d-price"> {{ $advert->price }}</span></p>
            </div>
        </div>
    </div>
    <div class="slider mt-3 mb-3">
        <div class="inner-slider">

                  @foreach( $advert->images as $image )
            <div class="item">
                <img src="{{Request::root()}}/uploads/product_images/{{$image->name}}" alt="">
            </div>
            @endforeach
            
           
        </div>
        <span class="ad-d-arrow ad-d-arrow-right"><i class="fa fa-angle-right"></i></span>
        <span class="ad-d-arrow ad-d-arrow-left"><i class="fa fa-angle-left"></i></span>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <div class="ad-details-content col-12 mt-4 mb-4">
                    <div class="row">
                        <div class="col-12">
                            <h2 class="mb-4">  {{ $advert->title }} </h2>
                            <table class="table-responsive">
                                <tr>
                                    <td><span class="bold">Price: </span> {{ $advert->price }}</td>
                                    <td><span class="bold">Date: </span> {{ date('Y-m-d' , strtotime($advert->created_at)) }}</td>
                                    {{-- <td><span class="bold">Condition: </span> used</td> --}}
                                </tr>
                                 
                            </table>
                            <br>
                            <br>
                            <br>
                            <p> {{ $advert->description }}</p>
                            
                            
                        </div>
                    </div>
                </div>
                 
                
            </div>
            <br>

            <div class="col-md-4 mt-4 mb-3 sidebar-right  col-sm-12">
                <div class="media send-mail">
                    <span class="mail"><i class="fa fa-envelope-o"></i></span>
                    <P>Contact Seller vie email : {{ $advert->user->email }} </p>
                </div>
                <div class="media send-mail">
                    <span class="phone"><i class="fa fa-mobile"></i></span>
                    <a href="#">Click To view </a>
                </div>
                <div class="media user-info-sidbar">
                    
                  <img class="mr-3" src="{{Request::root()}}/uploads/users_photos/{{$advert->user->photo}}" alt="">
                    <div class="media-body">
                        <h5 class="mt-0"> {{ $advert->user->name }} </h5>  
                    </div>
                </div>
                
            </div>
        </div>
    </div>




@endsection


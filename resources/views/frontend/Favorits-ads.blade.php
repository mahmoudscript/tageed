@extends('frontend.app')


@section('content')

    @php
        $lang = LaravelLocalization::getCurrentLocale();

               $laguage = \App\Language::where(['label'=>$lang])->first();


               $language_id = $laguage->id ;
    @endphp

    <div class="banner-pages">
        <div class="background"><img src="{{Request::root()}}/frontend/img/banner-pages.jpg" alt=""></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="banner-title">favorite ads</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="latest-adds margin-top">
        <div class="container">
            <div class="row no-margin">
                <div class="col-12 latest-adds-tabs">
                    <div class="row no-margin">
                        <div class="col-9 text-left">
                            <p class="latest-adds-tabs-botton">
                                Sort By:
                                <span data-class="all" class="activeTab">all</span>
                                <span data-class=".Date" >Date</span>
                                <span data-class=".Title" >Title</span>
                                <span data-class=".Price" >Price</span>
                            </p>
                        </div>
                        <div class="col group-type text-right">
                            <span href="#" class="group-type-item group-type-item-list"><i class="fa fa-list" aria-hidden="true"></i></span>
                            <span href="#" class="group-type-item group-type-item-grid active-list"><i class="fa fa-th" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row latest-adds-items">
                    <div class="Date shuffle-item col-lg-4 col-md-4 col-sm-6" >
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555,000 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Title shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                            <span class="latest-ads-category blue">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Date shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red-2">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Price shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555,000 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Price shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                            <span class="latest-ads-category blue">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Date shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red-2">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Price shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555,000 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Title shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                            <span class="latest-ads-category blue">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Title shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red-2">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Price shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555,000 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Title shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart-o"></i></span>
                            <span class="latest-ads-category blue">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                    <div class="Title shuffle-item col-lg-4 col-md-4 col-sm-6">
                        <div class="latest-ads-item">
                            <img src="{{Request::root()}}/frontend/img/latest-item-1.png" alt="">
                            <span class="latest-ads-price">555 EGP</span>
                            <span class="adds-heart"><i class="fa fa-heart"></i></span>
                            <span class="latest-ads-category red-2">Fashion</span>
                            <a href="{{url('/ad_details')}}" class="latest-ads-name">Business Idea <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                            <span class="latest-ads-time">Today 12:58</span>
                            <span class="latest-ads-place">Giza</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <a href="#" class="load-more">LOAD MORE</a>
                </div>
            </div>
        </div>
    </div>



@endsection


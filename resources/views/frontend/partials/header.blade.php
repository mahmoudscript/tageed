@php
$lang = LaravelLocalization::getCurrentLocale();

       $laguage = \App\Language::where(['label'=>$lang])->first();


       $language_id = $laguage->id ;
@endphp
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Taget Website</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{Request::root()}}/frontend/css/bootstrap.min.css"  >
    <link rel="stylesheet" href="{{Request::root()}}/frontend/css/fontawesome.min.css"  >
    <link rel="stylesheet" href="{{Request::root()}}/frontend/css/main.css"  >

    
    <link rel="stylesheet" href="{{Request::root()}}/frontend/css/slick-theme.css" >
    <link rel="stylesheet" href="{{Request::root()}}/frontend/css/slick.css" >
    

</head>
<body>
<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 text-left">
                <h1>
                    <a href="{{url('/')}}"><img src="{{Request::root()}}/frontend/img/logo.png" alt=""></a>
                </h1>
            </div>
            <div class="col-xs-8 header-account text-right">
                

                 @if(Auth::check())
                @php $user_id = Auth::user()->id @endphp
                         <a href="{{url("/user_ads/$user_id")}}" class="my-account"><i class="fa fa-user"></i> {{ Auth::user()->name }}</a>

                        <a href="{{ route('logout') }}"
                                         onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                            <i class="icon-key"></i> {{trans('backend.logout')}} </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                                                        {{ csrf_field() }}
                                                                                    </form>

                <a href="{{url("$lang/advpage")}}" class="place-add"><i class="fa fa-plus"></i> Place a Free Ad</a>
                 @else
                         <a href="{{url('/register')}}" class="my-account"> Create Account</a>
                         <a href="{{url('/loginpage')}}" class="my-account"> login</a>

                 @endif


               
            </div>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 text-center footer-div-margin">
                <h2>ABOUT US</h2>
                <p>
                         
                        @foreach($about->description as $desc)
                        @if($desc->language_id == $language_id)
                            {{ $desc->description }}
                        @endif
                    @endforeach
                </p>
                <div class="social-footer">
                    <a href="{{url("$about->facebook")}}"><i class="fa fa-facebook"></i></a>
                    <a href="{{url("$about->twitter")}}"><i class="fa fa-twitter"></i></a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6  contact-us-footer footer-div-margin">
                <h2 class="text-center">Recent Adverts</h2>
                <ul class="list-unstyled text-md-left text-lg-left">


                    @foreach($products as $product)
                    
                    <li class="media ">
                        <img class="mr-3 media-img" src="{{Request::root()}}/uploads/product_images/{{$product->oneImage($product->id)}}" alt="">
                        <div class="media-body-footer">
                            <h5 class="mt-0 mb-1"><a href="{{url("/ad_details/$product->id")}}"> {{ $product->title }}</a></h5>
                            <p>
                                    @foreach($product->department->description as $desc)
                                    @if($desc->language_id == $language_id)
                                        {{ $desc->name }}
                                    @endif
                                @endforeach
                            </p>
                        </div>
                    </li>
                    @endforeach
                   
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 text-center contact-us-footer footer-div-margin">
                <h2>CONTACT US</h2>
                <p>@foreach($about->description as $desc)
                        @if($desc->language_id == $language_id)
                            {{ $desc->address }}
                        @endif
                    @endforeach</p>
                <p>Email: <a href="mailto:Username@email.com"> {{ $about->email }}</a></p>
                <p>Phone: {{ $about->phone }}</p>
                
                <div class="form-contact">

                        {{-- From --}}
                        {!! Form::open(['url'=>["$lang/contactus"],'method'=>'POST','class'=>''  
                        ,'role'=>'form','files'=> true ,'id'=>'add_files']) !!}
                        <input required="required" type="text" name="name" placeholder="Your Name ..." >
                        <input required="required" type="email" name="email"  placeholder="Your email ...">
                        <input required="required" type="text" name="phone"  placeholder="Your phone ...">
                        <textarea required="required" name="message"  rows="6" placeholder="Your Message ..."></textarea>
                        <input type="submit" value="Send" name="" class="text-uppercase">
                        {!! Form::close() !!}
                    {{-- End Form --}}

                </div>
            </div>
        </div>
    </div>
</div>
<div class="bottom-footer"></div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="{{Request::root()}}/frontend/js/jquery.min.js" ></script>
<script src="{{Request::root()}}/frontend/js/popper.min.js" ></script>
<script src="{{Request::root()}}/frontend/js/bootstrap.min.js" ></script>
<script src="{{Request::root()}}/frontend/js/jquery.filterizr.min.js"></script>
<script src="{{Request::root()}}/frontend/js/main.js"></script>


<script src="{{Request::root()}}/frontend/js/slick.js"></script>

</body>
</html>
@extends('frontend.app')


@section('content')

    @php
        $lang = LaravelLocalization::getCurrentLocale();

               $laguage = \App\Language::where(['label'=>$lang])->first();


               $language_id = $laguage->id ;
    @endphp


    <div class="banner-pages">
        <div class="background"><img src="{{Request::root()}}/frontend/img/banner-pages.jpg" alt=""></div>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h2 class="banner-title">User Ads</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="user-information margin-top">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 user-info-left">
                    <div class="media">
                        <img class="mr-3"
                         src="{{Request::root()}}/uploads/users_photos/{{$user->photo}}" 
                         alt="user Image">
                        <div class="media-body-user">
                            <h5 class="mt-0">{{$user->name}}</h5>
                            last login from 
                            @php
                            use Carbon\Carbon;
                            $now = Carbon::now();
                            $old_time = Carbon::parse($user->updated_at);

                            {{-- dd($now , $old_time); --}}

                             

                            $result = $now->diffInDays($old_time) . " days "; 

                            @endphp
                            <p class="mb-1 last-login"> {{ $result }} </p>
                             
                            <span class="verified">verified</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 social-user-profile">
                    <h5>Social profile Links </h5>
                    <a href="{{url("$user->facebook")}}"><i class="fa fa-facebook"></i></a>
                    <a href="{{url("$user->googleplus")}}""><i class="fa fa-google-plus"></i></a>
                    <a href="{{url("$user->twitter")}}"><i class="fa fa-twitter"></i></a>
                    <a href="{{url("$user->linkedin")}}"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>
            <hr class="mb-4">
            <div class="row">
                
                <div class="col-lg-12 col-md-12 col-sm-12 info-contact mb-3">
                    <h4 class="mb-4">Contact Details</h4>
                    <p class="mb-1">
                        <i class="fa fa-phone"></i>
                        {{$user->phone}}
                    </p>
                    <p class="mb-1">
                        <i class="fa fa-envelope"></i>
                        {{$user->email}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="latest-adds margin-top">
        <div class="container">
            <div class="row no-margin">
                <div class="col-12 latest-adds-tabs">
                    <div class="row no-margin">
                             
                        <div class="col group-type text-right">
                            <span href="#" class="group-type-item group-type-item-list"><i class="fa fa-list" aria-hidden="true"></i></span>
                            <span href="#" class="group-type-item group-type-item-grid active-list"><i class="fa fa-th" aria-hidden="true"></i></span>
                        </div>
                    </div>
                </div>
                <div class="row latest-adds-items">


                     {{-- item data  --}}
                     @foreach($adverts as $advert)
 
                     <div class="Date shuffle-item col-lg-4 col-md-4 col-sm-6" >
                         <div class="latest-ads-item">
                             @foreach($advert->images as $img)
                             <img src="{{Request::root()}}/uploads/product_images/{{$img->name}}" alt="">
                             @endforeach
 
                             <span class="latest-ads-price"> {{ $advert->price }}</span>
                             
                             <span class="latest-ads-category red">
                             
                             @foreach($advert->department->description as $desc)
                                 @if($desc->language_id == $language_id)
                                     {{ $desc->name }}
                                 @endif
                             @endforeach
 
                         </span>
                             <a href="{{url("/ad_details/$advert->id")}}" class="latest-ads-name">
 
                                  
                                 {{ $advert->title   }}
 
                                 <i class="fa fa-check-circle" aria-hidden="true"></i></i></a>
                             <span  class="latest-ads-time">
                                 {{ date('Y-m-d' , strtotime($advert->created_at)) }}
                             </span>
                              
                         </div>
                     </div>
                     @endforeach
                    
                </div>
            </div>
            
        </div>
    </div>




@endsection


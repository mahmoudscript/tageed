@extends('frontend.app')


@section('content')

@php
$lang = LaravelLocalization::getCurrentLocale();

       $laguage = \App\Language::where(['label'=>$lang])->first();


       $language_id = $laguage->id ;
@endphp


<div class="banner-pages">
    <div class="background"><img src="{{Request::root()}}/frontend/img/banner-pages.jpg" alt=""></div>
</div>
<div class="content-pages">
     <div class="container">
         <div class="ba-color">
             <div class="row">
                 <div class="col-xs-12">
                     <div class="register-content text-center add-content">
						 <h2 class="register-title">Free Add <i class="fa fa-caret-down"></i></h2>
						 

								 
		 {!! Form::open(['url'=>["$lang/addadvert"],'method'=>'POST','class'=>'form-horizontal'  
		 ,'role'=>'form','files'=> true ,'id'=>'add_files']) !!}
                               
                    
							<div class="form-group">
								<label for="title" class="col-sm-2 control-label">Title:</label>
								<div class="col-sm-10">
									<input name="title"  required="required" type="text" placeholder="Add Title" class="form-control" id="title">
								</div>
							</div>
							<div class="form-group">
								<label for="Descrtiption" class="col-sm-2 control-label">Descrtiption:</label>
								<div class="col-sm-10">
									<input  name="description"  required="required" type="text" placeholder="Descrtiption" class="form-control" id="Descrtiption">
								</div>
							</div>
							<div class="form-group">
								<label for="Price" class="col-sm-2 control-label">Price:</label>
								<div class="col-sm-10">
									<input name="price" required="required" type="text" placeholder="Price" class="form-control" id="Price">
								</div>
							</div>
							<div class="form-group">
								<label for="type" class="col-sm-2 control-label"> Category : </label>
								<div class="col-sm-10">
									<select name="cat" class="form-control" id="type">
										@foreach($cats as $cat )
									<option   value="{{ $cat->id }}"> 
										@foreach($cat->description as $desc)
										{{ $desc->name }}
										@endforeach
									</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="type2"  class="col-sm-2 control-label">Type 2:</label>
								<div class="col-sm-10">
									<select name="status"  class="form-control" id="type2">
									 <option value="not_special">Not_special</option>
									<option value="special">Special</option>
									
									 
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="type2" class="col-sm-2 control-label">add image: </label>
								<div class="col-sm-10">
									<input required="required" type="file" name="logo" value="" placeholder="" class="form-control">
								</div>
							</div>
							{{--  <div class="add-map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3415.561932432379!2d30.941266887632377!3d31.121905482177407!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzHCsDA3JzE4LjAiTiAzMMKwNTYnMzQuMiJF!5e0!3m2!1sen!2seg!4v1527161835374" width="90%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>  --}}
							 <input   type="submit" value="Submit" class="form-control submit">
							 
							 {!! Form::close() !!}
							 {{-- end Form --}}
						 


                     </div>
                 </div>

             </div>
         </div>

     </div>
 </div>





@endsection




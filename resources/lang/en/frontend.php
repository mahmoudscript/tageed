<?php

return [


    'hala_health' => 'Hala Health',
    'home'=>'Home',
    'about'=>'About Us',
    'catergory'=>'Departments',
    'advices'=>'Advices',


    'ar'=>'Arbic',
    'en'=>'English',
    'contact_us'=>'Contact Us',
    'full_name'=>'Full Name',
    'email'=>'Email',
    'message'=>'Message',
    'write_your_msg'=>'Your Message here',
    'send_msg'=>'Send Message',
    'addresses'=>'Addresses',
    'site_map'=>'Site Map',
    'upload_app'=>'Upload App',
    'social_media'=>'Social Media',
    'read_more'=>'Read More',

    'cart'=>'Cart',
    'my_account'=>' My account',
    'my_favourite'=>'My favourite',
    'cart_list'=>'Cart List',
    'register'=>'Register',
    'login'=>'Login',
    'email_phone'=>'Email ',
    'remember_me'=>'Remember me',
    'password'=>'Password',
    'phone'=>'Phone',
    'confirm_pass'=>'Confirm Password',


    'social_login'=>'Login With Social Media',
    'select_cat'=>' Select Category',
    'search'=>'Search',
    'privacy'=>'Privacy policy',
    'terms'=>'Terms of use',
    'tatweer'=>'Tatweer',

    'footer'=>'Copyright © 2017 All rights reserved to Hala Health care Designed by',


    'our_brands'=>'Our brands',
    'our_agencies'=>'Our exclusive agencies',

    'special_offer'=>'Special Offers',
    'new_hala'=>'New Hala',
    'most_sales'=>'Most Sales',

   'add_to_cart'=>'Add to cart',
   'details'=>'Details',
   'reviews'=>'Reviews',

    'find_us'=>'Find US',
    'get_in_touch'=>'Get in Touch',
    'download_app'=>'Download App',

    //contact us

    'msg_sent'=>'Message sent successfully ',
    'name'=>' Name',
    'email'=>'Email ',
    'msg'=>'Message',
    'send'=>' Send it ',
    'error'=>' Error accure ',
    'registration_success'=>'User has been register successfully',
    'enter_email_or_phone'=>'Enter Email' ,
    'enter_password'=>'Enter password'  ,
    'not_found'=>'Invalid user'  ,

    'price'=>'Price'  ,
    'ryal'=>'Ryal'  ,
    'added_to_favourite'=>'added to your favourite'  ,


    'cart_details'=>'Cart Details'  ,
    'total_price'=>'Total Price'  ,
    'buy_now'=>' Buy Now'  ,
    'amount'=>'Amount'  ,
    'unit_price'=>'Price per unit' ,

    //

    'choose_payment'=>'Choose Payment Method'  ,
    'payment_reciving'=>'Payement when reciving'  ,
    'payment_card'=>'Payement with card'  ,
    'payment_card_number'=>'Credit card number'  ,
    'payment_card_name'=>'The name the appears on your credit card'  ,
    'payment_card_auth'=>'ID check card'  ,
    'confirm_payment'=>'Confirm Payment'  ,
    'save_data'=>'Save Data'  ,

    //from request
    'request_form'=>'Request Form' ,
    'email_p'=>'Email OR Phone' ,
    'request'=>'Enter your request' ,
    'order_now'=>'Order now' ,

    //shipping address......
    'city'=>'City' ,
    'state'=>'State / Region' ,
    'gender'=>'Gender ' ,
    't_phone'=>'Telephone ' ,
    's_address'=>'St.name/Building number/Apartment number' ,
    's_name'=>'Name ' ,
    'insert_address'=>' Insert your address' ,
































];
<?php

return [
    //SIDE BAR
    'dashboard' => 'Dashboard',
    'languages' => 'Languages',
    'add_language' => ' Add Language',
    'all_languages' => 'Show All Languages',
    'users' => 'Users',
    'admins' => 'Admin',

'products' => 'adverts',
'show_all_product' => 'Show All adverts',
'add_product' => 'Add new advert',
'update_product' => 'Update advert',

'advices' => 'Advices',
'show_all_advices' => 'Show All Advices',
'add_advice' => 'Add new Advice',
'update_advice' => 'Update Advice',


'brands' => 'Brands',
'show_all_brands' => 'Show All Brands',
'add_brand' => 'Add new Brand',
'update_brand' => 'Update Brand',

'agencies' => 'Agencies',
'show_all_agencies' => 'Show All Agencies',
'add_agency' => 'Add new Agency',
    'update_agency' => 'Update Agency',

'privacies' => 'Privacies',
'show_all_privacies' => 'Show All Privacies',
'add_privacy' => 'Add new Privacy',
'update_privacy' => 'Update Privacy',

'policies' => 'Policies',
'show_all_policies' => 'Show All Policies',
'add_policy' => 'Add new Policy',
'update_policy' => 'Update Policy',

'orders' => 'Orders',

    //departments...
    'departments' => 'Departments ',
    'add_department' => 'Add new Department ',
    'all_departments' => 'Show All Department',
    'update_department' => 'Update Department',





    
    //actions
    'save' => 'Save',
    'cancel' => 'Cancel',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'show' => 'Show',
    'actions'=>'Actions'  ,
    'pending' => 'Pending',
    'approve' => 'Approve',
    'reject' => 'Reject',
    'added_date' => 'Added Date',
    // general :
    'action'=>'Actions' ,
    'active'=>'Active' ,
    'not_active'=>'Not Active',
    'add'=>'Add' ,
    'reset'=>'Reset' ,
    'update'=>'Update' ,
    'status'=>'Status',
    'change'=>'Change',
    'select_file'=>'ٍSelect Logo to be uploaded',
    'select_file_note'=>'You can choose another logo by clicking again',
    'date_added'=>'Date Added' ,
    'goodjob' => 'Good Work every thing done well',
    // header info for users like image - name  etc   :
    'my_profile'=>'My Profile' ,
    'logout'=>'Logout',
    // language table lisitng titles :
    'language'=>'Language',
    'create_language'=>'ِAdd New Language',
    'show_languages'=>'Show All Languages',
    'language_name'=>'Lang Name' ,
    'language_status'=>'Lang Status' ,
    'language_label'=>'Lang Label' ,
    'language_added_date'=>'Date Added' ,
    'language_count_zero'=>'Sorry , No language added yet',
    'adding_language'=>'Adding New Language' ,
    'editing_language'=>'Edit Language Info' ,
    'language_message_added'=>'New Language Has Been Added Successfully',
    'language_message_updated'=>'Language Has Been Updated Successfully',
    // users
    'all_users'=>'All Users',
    'create_user'=>'Add New User',
    'show_users'=>'Show All Users',
    'update_users'=>'Update User',

    'user_count_zero'=>'Sorry , No User has been  added yet',
    'user_password'=>'Password' ,
    'user_phone'=>'Phone' ,
    'user_gender'=>'Gender' ,

    'user_email'=>'Email',
    'user_logo'=>'Logo',
    'user_profile_info'=>'Account Info' ,
    'user_profile_setting'=>'Account Settings' ,
    'user_message_added'=>'New User Has Been Added Successfully',
    'user_message_updated'=>'User Has Been Updated Successfully',
    'select_user_type'=>'Select user type',
    'visitors_message'=>'Visitors Messages',
    //visitors Messages
    'visitors_messages'=>'Visitors Messages',
    'name'=>'Name',
    'subject'=>'Subject',
    'email'=>'Email',
    'message'=>'Message',
    'reply'=>'Reply Message',
    'delete_message'=>'Delete Message',
    'message_reply_successfully'=>'Message Has Been Sent Successfully',
    'no_messages'=>'Sorry,There Is No Message',
    'show_message'=>'Show Message',
    'message_not_sent'=>'Message Not Sent',
    'message_deleted'=>'Message Has Been Deleted',
    //social media
    'social_media'=>'Social Media',
    'add_social'=>'Add Social Media',
    'show_social'=>'Show All Social Media',
    'url'=>'Url',
    'icon'=>'Icon',
    //about
    'title'=>'Title',
    'content'=>'Content',
    'phone_one'=>'Phone Number One',
    'phone_two'=>'Phone Two One',
    'images'=>'Images',
    'setting'=>'Setting',
    'about_images'=>'About Images',
    'social_added'=>'Social Media Added Successfully' ,
    'social_deleted'=>'Social Media Deleted Successfully' ,
    'social_updated'=>'Social Media updated Successfully' ,
    // user
    'user_title'=>'User Title',
    'user_name'=>' Name ',
    'user_name_en'=>'Name en',
    'user_email'=>' Email',
    'user_phone'=>' Phone',
    'user_adress'=>' Adress',
    'user_gender'=>' Gender',
    'user_role'=>' Role',
    'user_message_added' => 'New User Has Been Added',
    'user_message_deleted' => 'User Has Been Deleted',
    'user_message_updated' => 'User Has Been Updated',

    'send_msg_all'=>'send message to all users' ,
    'send_msg_city'=>'send message acording city' ,
    'admins' => 'Admin',
    'user'  => 'User',
    'male' => 'Male',
    'female' => 'Female',
    'user_message_sent' => 'Message Sent successfully',
    // termes
    'terme'  => 'Termes',
    'terme_name'=>'Name',
    'terme_content'=>' Termes Content',
    'terme_message_added' => 'New Terme Has Been Added',
    'cterme_message_deleted' => 'Terme Has Been Deleted',
    'terme_message_updated' => 'Terme Has Been Updated',
    //notifications.................
    'notifications_count_zero'=>'There is no notifications '  ,
    //about.....................
    'about_image'=>"About Image",
    'phone'=>"Phone",
    'multi_email'=>"Email",
    'ios_app'=>"App Store app link",
    'google_store'=>"Google Store app link",
    'address'=>"Address",
    'about_info_updated'=>"website date has been updated",
    //model message
    'send_msg'=>'Send Message',
    'msg'=>'Message',
    'enter_your_msg'=>'Enter your message here',
    'close'=>'Close',
    'send'=>'Send',


   //department........
    'department_name'=>'Department name',
    'Categery_name'=>'Department Categery name',
    'update_department'=>'Update Department',
    'main_department'=>'Main Department',
    'sub_department'=>'sub Department',
    'add_sub_department'=>'add sub Department',
    'show_sub_departments'=>'show sub departments',
    'add_main_department'=>'Add main departments',



    'department_message_added' => 'New Department Has Been Added',
    'department_message_deleted' => 'Department Has Been Deleted',
    'department_message_updated' => 'Department Has Been Updated',

    //product .................

    'product_name'=>'advert name',
    'product_description'=>'adverts description ',
    'product_addition_info'=>'Addition Info ',
    'product_quantity'=>'adverts quantity',
    'product_price'=>'adverts price',
    'product_Status'=>'adverts Status',
    'product_special'=>'adverts special',
    'product_images'=>'adverts images',

    'publish'=>'Publish',
    'not_publish'=>'Not Publish',
    'special'=>'Special',
    'not_special'=>'Not Special',
    'image_message_deleted'=>'Image Has Been Deleted',



    
    'product_message_added' => 'New Product Has Been Added',
    'product_message_deleted' => 'Product Has Been Deleted',
    'product_message_updated' => 'Product Has Been Updated',

     //brand ........
    'brand_name'=>'Brand name',

    //advices....
    'title'=>'Title',
    'description'=>'Description',
    'message_deleted'=>'Deleteing has been done',
    'message_added'=>'Adding has been done',
    'message_update'=>'Updateing data has been done',

   //about

    'history' => 'History',
    'who_we_are' => 'Who we are?',
    'what_we_do' => 'What we do?',
    'our_mission' => 'Our Mission',

    //

    'shipping_information'=>'Shipping Information',
    'add_shipping_information'=>'Add Shipping Information',
    'update_shipping_information'=>'Update Shipping Information',
    'show_all_shipping_information'=>'Show Shipping Information',


    'replacement'=>'Replacement and retrieval',
    'add_replacement'=>'Add Replacement and retrieval',
    'update_replacement'=>'Update Replacement and retrieval',
    'show_all_replacement'=>'Show Replacement and retrieval',


    'payment_metgods'=>'payment methods',
    'add_payment_metgods'=>'Add payment methods',
    'update_payment_metgods'=>'Update payment methods',
    'show_all_payment_metgods'=>'Show payment methods',

    //orders

    'order_name' => 'Order name',
    'customer' => 'Customer',
    'image' => 'Photo',
    'paid'=>'Paid' ,
    'Pending'=>'Pending' ,
    'order_info'=>'Order Information' ,


    //order details........



     'customer_info'=>'Customer Information' ,
     'customer_name'=>'Customer Name' ,
     'order_details'=>'Order Details' ,
     'order_date'=>'Order Date & Time' ,
     'order_status'=>'Order Status' ,
     'order_total'=>'Grand Total' ,
     'order_payment'=>'Payment Information' ,

    'order_address'=>'Shipping Address' ,
    'order_quantity'=>'Order Quantity' ,













];

<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'يجب قبول',
    'active_url'           => ' لا يُمثّل رابطًا صحيحًا',
    'after'                => 'يجب على أن يكون تاريخًا لاحقًا للتاريخ ',
    'after_or_equal'       => ':attribute يجب أن يكون تاريخاً لاحقاً أو مطابقاً للتاريخ :date.',
    'alpha'                => 'يجب أن لا يحتوي  سوى على حروف',
    'alpha_dash'           => 'يجب أن لا يحتوي  على حروف، أرقام ومطّات.',
    'alpha_num'            => 'يجب أن يحتوي  على حروفٍ وأرقامٍ فقط',
    'array'                => 'يجب أن يكون  ًمصفوفة',
    'before'               => 'يجب على  أن يكون تاريخًا سابقًا للتاريخ :date.',
    'before_or_equal'      => ':attribute يجب أن يكون تاريخا سابقا أو مطابقا للتاريخ :date',
    'between'              => [
        'numeric' => 'يجب أن تكون قيمة  بين :min و :max.',
        'file'    => 'يجب أن يكون حجم الملف  بين :min و :max كيلوبايت.',
        'string'  => 'يجب أن يكون عدد حروف النّص  بين :min و :max',
        'array'   => 'يجب أن يحتوي على عدد من العناصر بين :min و :max',
    ],
    'boolean'              => 'يجب أن تكون قيمة إما true أو false ',
    'confirmed'            => 'حقل التأكيد غير مُطابق للحقل',
    'date'                 => ':attribute ليس تاريخًا صحيحًا',
    'date_format'          => 'لا يتوافق مع الشكل :format.',
    'different'            => 'يجب أن يكون الحقلان و :other مُختلفان',
    'digits'               => 'يجب أن يحتوي على :digits رقمًا/أرقام',
    'digits_between'       => 'يجب أن يحتوي بين :min و :max رقمًا/أرقام ',
    'dimensions'           => 'الـ يحتوي على أبعاد صورة غير صالحة.',
    'distinct'             => 'للحقل قيمة مُكرّرة.',
    'email'                => 'يجب أن يكون عنوان بريد إلكتروني صحيح البُنية',
    'exists'               => 'القيمة المحددة غير موجودة',
    'file'                 => 'الـ يجب أن يكون ملفا.',
    'filled'               => ':attribute إجباري',
    'image'                => 'يجب أن يكون صورةً',
    'in'                   => ':attribute لاغٍ',
    'in_array'             => ':attribute غير موجود في :other.',
    'integer'              => 'يجب أن يكون عددًا صحيحًا',
    'ip'                   => 'يجب أن يكون عنوان IP صحيحًا',
    'ipv4'                 => 'يجب أن يكون عنوان IPv4 صحيحًا.',
    'ipv6'                 => 'يجب أن يكون عنوان IPv6 صحيحًا.',
    'json'                 => 'يجب أن يكون نصآ من نوع JSON.',
    'max'                  => [
        'numeric' => 'يجب أن تكون قيمة مساوية أو أصغر لـ :max.',
        'file'    => 'يجب أن لا يتجاوز حجم الملف :max كيلوبايت',
        'string'  => 'يجب أن لا يتجاوز طول النّص :max حروفٍ/حرفًا',
        'array'   => 'يجب أن لا يحتوي على أكثر من :max عناصر/عنصر.',
    ],
    'mimes'                => 'يجب أن يكون ملفًا من نوع : :values.',
    'mimetypes'            => 'يجب أن يكون ملفًا من نوع : :values.',
    'min'                  => [
        'numeric' => 'يجب أن تكون قيمة مساوية أو أكبر لـ :min.',
        'file'    => 'يجب أن يكون حجم الملف على الأقل :min كيلوبايت',
        'string'  => 'يجب أن يكون طول النص على الأقل :min حروفٍ/حرفًا',
        'array'   => 'يجب أن يحتوي على الأقل على :min عُنصرًا/عناصر',
    ],
    'not_in'               => ':attribute لاغٍ',
    'numeric'              => 'يجب على أن يكون رقمًا',
    'present'              => 'يجب تقديم',
    'regex'                => 'صيغة .غير صحيحة',
    'required'             => ' مطلوب.',
    'required_if'          => ' مطلوب في حال ما إذا كان :other يساوي :value.',
    'required_unless'      => ' مطلوب في حال ما لم يكن :other يساوي :values.',
    'required_with'        => ' مطلوب إذا توفّر :values.',
    'required_with_all'    => ' مطلوب إذا توفّر :values.',
    'required_without'     => ' مطلوب إذا لم يتوفّر :values.',
    'required_without_all' => ':attribute مطلوب إذا لم يتوفّر :values.',
    'same'                 => 'يجب أن يتطابق مع :other',
    'size'                 => [
        'numeric' => 'يجب أن تكون قيمة مساوية لـ :size',
        'file'    => 'يجب أن يكون حجم الملف :size كيلوبايت',
        'string'  => 'يجب أن يحتوي النص على :size حروفٍ/حرفًا بالظبط',
        'array'   => 'يجب أن يحتوي على :size عنصرٍ/عناصر بالظبط',
    ],
    'string'               => 'يجب أن يكون نصآ.',
    'timezone'             => 'يجب أن يكون نطاقًا زمنيًا صحيحًا',
    'unique'               => 'قيمة مُستخدمة من قبل',
    'uploaded'             => 'فشل في تحميل الـ',
    'url'                  => 'صيغة الرابط غير صحيحة',
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */
    'custom'               => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],
    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */
    'attributes'           => [
        'name'                  => 'الاسم',
        'username'              => 'اسم المُستخدم',
        'email'                 => 'البريد الالكتروني',
        'first_name'            => 'الاسم الأول',
        'last_name'             => 'اسم العائلة',
        'password'              => 'كلمة السر',
        'password_confirmation' => 'تأكيد كلمة السر',
        'city'                  => 'المدينة',
        'country'               => 'الدولة',
        'address'               => 'عنوان السكن',
        'phone'                 => 'الهاتف',
        'mobile'                => 'الجوال',
        'age'                   => 'العمر',
        'sex'                   => 'الجنس',
        'gender'                => 'النوع',
        'day'                   => 'اليوم',
        'month'                 => 'الشهر',
        'year'                  => 'السنة',
        'hour'                  => 'ساعة',
        'minute'                => 'دقيقة',
        'second'                => 'ثانية',
        'title'                 => 'العنوان',
        'content'               => 'المُحتوى',
        'description'           => 'الوصف',
        'excerpt'               => 'المُلخص',
        'date'                  => 'التاريخ',
        'time'                  => 'الوقت',
        'available'             => 'مُتاح',
        'size'                  => 'الحجم',
    ],


];

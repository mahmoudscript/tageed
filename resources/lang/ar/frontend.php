<?php

return [

    'hala_health' => 'هاله هيلثى',
    'home'=>'الرئيسيه',
    'about'=>' من نحن',
    'catergory'=>'الاقسام',
    'advices'=>'النصائح',


    'ar'=>'عربى ',
    'en'=>'انجليزى',
    'contact_us'=>'تواصل معنا',
    'full_name'=>'الاسم بالكامل',
    'email'=>'البريد الالكترونى',
    'message'=>'الرساله',
    'write_your_msg'=>'اكتب رسالتك هنا',
    'send_msg'=>'ارسل رسالتك',
    'addresses'=>'العناوين',
    'site_map'=>'خريطه الموقع',
    'upload_app'=>'حمل التطبيق',
    'social_media'=>'قم بالتواصل معنا',
    'read_more'=>'اقراء المزيد',

    'copy_right_for'=>'جميع الحقوق محفوظه',
    'cart'=>'عربة التسوق',
    'my_account'=>'حسابى',
    'my_favourite'=>'المفضله',
    'cart_list'=>'سله المشتريات',
    'register'=>'التسجيل',
    'login'=>'الدخول',
    'email_phone'=>'البريد الالكترونى',
    'remember_me'=>'تذكرنى',
    'password'=>'الرقم السرى',
    'phone'=>'رقم الهاتف',
    'confirm_pass'=>'تاكيد الرقم السرى',

    'social_login'=>' سجل باستخدام شبكات التواصل الاجتماعى',
    'select_cat'=>'اختر القسم',
    'search'=>'ابحث',
    'privacy'=>'سياسه الخصوصيه',
    'terms'=>' شروط الاستخدام',
    'tatweer'=>'تطوير',
    'footer'=>'جميع الحقوق محفوظه لهاله تصميم وتطوير بواسطه',


    'our_brands'=>'علامتنا التجارية',
    'our_agencies'=>'وكالتنا الحصرية',

    
    'special_offer'=>'عروض خاصة',
    'new_hala'=>'جديد هالة',
    'most_sales'=>'الأكثر مبيعا',

    'add_to_cart'=>'اضافه الى عربه التسوق',
    'details'=>'عرض التفاصيل',
    'reviews'=>'التقييمات',

    'find_us'=>'اين تجدنا',
    'get_in_touch'=>'كن على تواصل معنا',
    'download_app'=>'  حمل التطبيق الان ',

    //contact us

    'msg_sent'=>' تم ارسال الرساله بنجاح ',
    'name'=>'  الاسم ',
    'email'=>'  البريد الالكترونى ',
    'msg'=>'الرساله ',
    'send'=>' ارسل ',
    'error'=>' حدث خطا ',
    'registration_success'=>'تم تسجيل المستخدم بنجاح' ,
    'enter_email_or_phone'=>'ادخل البريد الالكترونى' ,
    'enter_password'=>'ادخل الرقم السرى'  ,
    'not_found'=>'المستخدم غير موجود'  ,


    //
    'price'=>'السعر'  ,
    'ryal'=>'ريال'  ,
    'added_to_favourite'=>'تمت اضافه المنتج الى مفضلتك'  ,

    'cart_details'=>'عربة التسوق'  ,
    'total_price'=>'اجمالى المبلغ'  ,
    'buy_now'=>' شراء الأن '  ,
    'amount'=>'الكميه'  ,
    'unit_price'=>'سعر الوحده'  ,

     //payment

    'choose_payment'=>'اختر طريقه الدفع'  ,
    'payment_reciving'=>'الدفع عند الاستلام'  ,
    'payment_card'=>'الدفع ببطاقه الائتمان'  ,
    'payment_card_number'=>' رقم بطاقة الائتمان'  ,
    'payment_card_name'=>' الاسم كما يظهر على بطاقة الائتمان '  ,
    'payment_card_auth'=>'رقم تحقق البطاقه'  ,
    'confirm_payment'=>'تاكيد الطلب '  ,
    'save_data'=>'حفظ البيانات'  ,

    //from request
    'request_form'=>'طلب' ,
    'email_p'=>'ادخل الهاتف او البريد الالكترونى' ,
    'request'=>'الطلب' ,
    'order_now'=>'اطلب الان' ,

    //shipping address......
    'city'=>'المدينه' ,
    'state'=>'دولة / منطقة' ,
    'gender'=>'النوع ' ,
    't_phone'=>'رقم الهاتف ' ,
    's_address'=>'اسم الشارع/رقم المبنى/رقم الشقه' ,
    's_name'=>'الاسم ' ,
    'insert_address'=>' ادخل العنوان الخاص' ,








];
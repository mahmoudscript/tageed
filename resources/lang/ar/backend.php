<?php

return [

     //side bar
    'dashboard' => 'لوحه التحكم',
    'stadium' => 'الملاعب',
    'languages' => 'اللغات',
    'add_language' => 'اضافه لغه',
    'all_languages' => 'عرض كل اللغات',
    'users' => 'المستخدمين',
    'admins' => 'المدير',

    'products' => 'الاعلانات',
    'show_all_product' => 'عرض جميع الاعلاانات',
    'add_product' => 'اضافه اعلان جديد',
    'update_product' => 'تعديل بيانات الاعلان',

    'advices' => 'النصائح',
    'show_all_advices' => 'عرض جميع النصائح',
    'add_advice' => 'اضافه نصيحه جديده',
    'update_advice' => 'تعديل بيانات النصيحه',



    'brands' => 'الماركات',
    'show_all_brands' => 'عرض جميع الماركات',
    'add_brand' => 'اضافه ماركه جديده',
    'update_brand' => 'تعديل بيانات ماركه',

    'agencies' => 'الوكالات',
    'show_all_agencies' => 'عرض جميع الوكالات',
    'add_agency' => 'اضافه وكاله جديده',
    'update_agency' => 'تعديل بيانات وكاله',

    'privacies' => 'الخصوصيه والسياسات',
    'show_all_privacies' => 'عرض جميع الخصوصيه والسياسات',
    'add_privacy' => 'اضافه الخصوصيه والسياسات',
    'update_privacy' => 'تعديل بيانات الخصوصيه',

    'policies' => 'سياسه الاستخدام',
    'show_all_policies' => 'عرض سياسات الاستخدام',
    'add_policy' => 'اضافه سياسه استخدام',
    'update_policy' => 'تعديل بيانات سياسه الاستخدام',
    'orders' => 'الطلبات',

    //about
    'about' => 'عن الموقع',
    'history' => 'التاريخ',
    'who_we_are' => 'من نحن',
    'what_we_do' => 'رؤيتنا',
    'our_mission' => 'رسالتنا',



    //departments...
    'departments' => 'الاقسام ',
    'add_department' => 'اضافه قسم ',
    'all_departments' => 'عرض جميع الاقسام',
    'department_delete' => 'تم حذف القسم بنجاح',

    //actions
    'save' => 'حفظ',
    'cancel' => 'تراجع',
    'edit' => 'تعديل البيانات',
    'delete' => 'حذف',
    'show' => 'عرض البيانات',
    'actions'=>'الاجرات' ,
    'pending' => 'قيد الطلب ',
    'approve' => 'تمت الموافقه',
    'reject' => 'تم الرفض',
    'added_date' => 'تاريخ الاضافه',




   //images
    'select_image' => 'اختر صوره',
    'remove' => 'ازاله',
    'change_image' => 'غير الصوره',
    'active'=>'مفعل' ,
    'not_active'=>'غير مفعل',
    'add'=>'اضافة' ,
    'reset'=>'تراجع' ,
    'update'=>'تحديث' ,
    'status'=>'الحالة',
    'change'=>'تغيير' ,
    'my_profile'=>'الملف الشخصى' ,
	'logout'=>'تسجيل الخروج',
    'goodjob' => 'عمل رائع تم كل شئ بنجاح',


    // language table lisitng titles :
    'language'=>'اللغة',
    'create_language'=>'اضافة لغة جديدة',
    'show_languages'=>'عرض كل اللغات',
    'language_name'=>'اسم اللغة' ,
    'language_status'=>'حالة اللغة' ,
    'language_label'=>'معرف اللغة' ,
    'language_added_date'=>'تاريخ الاضافة' ,
    'language_count_zero'=>'عفوا , لم يتم اضافة اى لغة حتى الان',
    'adding_language'=>'اضافة لغة جديدة' ,
    'editing_language'=>'تعديل بيانات لغة' ,
    'language_message_added'=>'تم اضافة لغة جديدة بنجاح',
    'language_message_updated'=>'تم تحديث بيانات اللغة بنجاح',

    // users

    'create_user'=>'اضافة مستخدم جديد',
    'show_users'=>'عرض كل المستخدمين',
    'user_password'=>'كلمة المرور' ,
    'user_phone'=>'رقم الهاتف' ,
    'user_gender'=>'الجنس' ,
    'update_users'=>'تحديث بيانات المستخدم',

    'user_email'=>'البريد الاليكترونى',
    'user_profile_info'=>'بيانات الحساب' ,
    'user_profile_setting'=>'اعدادات الحساب' ,
    'user_message_added'=>'تم اضافة مستخدم جديد بنجاح',
    'user_message_updated'=>'تم تحديث بيانات المستخدم بنجاح',
    'select_user_type'=>'اختر نوع المستخدم',
    'other'=>'اخرى' ,

    //visitors messages
    'visitors_messages'=>'رسائل الزوار',
    'name'=>'الاسم',
    'subject'=>'الموضوع',
    'email'=>'البريد الالكترونى',
    'message'=>'الرساله',
    'reply'=>'رد على الرساله',
    'message_reply_successfully'=>'تم ارسال الرساله بنجاح',
    'no_messages'=>'عفوا,لايوجد رسائل حاليا',
    'show_message'=>'اظهر الرساله',
    'message_not_sent'=>'لم يتم ارسالالرساله',
    'message_deleted'=>'تم مسح الرساله',

    //social media
    'social_media'=>'التواصل الاجتماعى',
    'add_social'=>'اضافه التواصل الاجتماعى',
    'show_social'=>'عرض التواصل الاجتماعى',
    'url'=>'الرابط',
    'icon'=>'الايقونه',
    'social_added'=>'تم اضافه التواصل الاجتماعى بنجاح' ,
    'social_deleted'=>'تم حذف  التواصل الاجتماعى بنجاح' ,
    'social_updated'=>'تم تعديل  التواصل الاجتماعى بنجاح' ,
    //about
    'title'=>'الاسم',
    'content'=>'المحتوى',
    'phone_one'=>'رقم الهاتف الاول',
    'phone_two'=>'رقم الهاتف التانى',
    'images'=>'الصور',
    'setting'=>'الاعدادات',
    'about_images'=>'صور من نحن',
    // user
    'user_title'=>'المستخدمين',
    'user_name'=>' الاسم ',
    'user_name_en'=>'الاسم بلانجليزيه',
    'user_email'=>' البريد الالكتروني',
    'user_phone'=>' الهاتف',
    'user_adress'=>' العنوان',
    'user_gender'=>' النوع',
    'user_role'=>' الوظيفه',
    'user_message_added' => 'تم اضافه مستخدم بنجاح',
    'user_message_deleted' => 'تم حذف مستخدم بنجاح',
    'user_message_updated' => 'تم تحديث بيانات المستخدم بنجاح',
    'send_msg_all'=>'ارسال رساله الى جميع المستخدمين' ,
    'send_msg_city'=>'ارسال رساله حسب المدينه' ,
    //
    'admin' => 'المديرين',
    'trainer' => 'المدربين',
    'user'  => 'الاعضاء',
    'male' => 'ذكر',
    'female' => 'انثي',
    // termes
    'terme'  => 'الشروط والاحكام',
    'terme_name'=>'أسم الشرط',
    'terme_content'=>' محتوي الشرط',
    'terme_message_added' => 'تم اضافه شرط جديد',
    'cterme_message_deleted' => 'تم حذف الشرط بنجاح',
    'terme_message_updated' => 'تم تحديث بيانات الشرط',
    //about.....................
    'about_image'=>"صوره الكابتن",
    'phone'=>"رقم الهاتف",
    'multi_email'=>"البريد الالكترونى",
    'ios_app'=>"لينك التطبيق على متجر ابل ",
    'google_store'=>"لينك التطبيق على متجر جوجل ",
    'address'=>"العنوان",
    'about_info_updated'=>"تم تعديل بيانات الموقع",
    //messages
    'send_msg'=>'ارسل رسالتك',
    'msg'=>'الرساله',
    'enter_your_msg'=>'اكتب رسالتك هنا',
    'close'=>'اغلق',
    'send'=>'ارسل',
    'user_message_sent' => 'تم ارسال الرساله بنجاح',

    //department........
    'department_name'=>'اسم القسم',
    'Categery_name'=>'اسم القسم التابع له',
    'update_department'=>'تحديث بيانات القسم',
    'main_department'=>'قسم رئيسى',
    'sub_department'=>'قسم فرعى',
    'add_sub_department'=>'اضافه قسم فرعى',
    'show_sub_departments'=>'عرض الاقسام الفرعيه',
    'add_main_department'=>'اضافه قسم رئيسي',


    'department_message_added' => 'تم اضافه القسم بنجاح',
    'department_message_deleted' => 'تم حذف القسم بنجاح',
    'department_message_updated' => 'تم تحديث بيانات القسم بنجاح',

    //product .................

    'product_name'=>'اسم الاعلان',
    'product_description'=>'وصف الاعلان',
    'product_addition_info'=>' معلومات اضافيه',
    //'product_quantity'=>'كميه الاعلان',
    'product_price'=>'سعر الاعلان',
    'product_Status'=>'حاله الاعلان',
    'product_special'=>'تميز الاعلان',
    'product_images'=>'صور الاعلان',
    'product_message_added' => 'تم اضافه الاعلان بنجاح',
    'product_message_deleted' => 'تم حذف الاعلان بنجاح',
    'product_message_updated' => 'تم تحديث بيانات الاعلان بنجاح',
    'image_message_deleted'=>'تم حذف الصوره بنجاح',

    'publish'=>'منشور',
    'not_publish'=>'غير منشور',
    'special'=>'مميز',
    'not_special'=>'غير مميز',


    //brand ........
    'brand_name'=>'اسم الماركه',



     //advices....
    'title'=>'العنوان',
    'description'=>'الوصف',
    'message_deleted'=>'تم الحذف بنجاح',
    'message_added'=>'تمت الاضافه بنجاح',
    'message_update'=>'تم تحديث البيانات بنجاح',


    //

    'shipping_information'=>'معلومات الشحن',
    'add_shipping_information'=>'اضافه معلومات الشحن  ',
    'update_shipping_information'=>' تعديل بيانات معلومات الشحن',
    'show_all_shipping_information'=>' عرض معلومات الشحن',



    'replacement'=>'الاستبدال والاسترجاع',
    'add_replacement'=>'اضافه الاستبدال والاسترجاع',
    'update_replacement'=>' تعديل بيانات الاستبدال والاسترجاع',
    'show_all_replacement'=>' عرض الاستبدال والاسترجاع',


    'payment_metgods'=>'طرق الدفع',
    'add_payment_metgods'=>'اضافه طرق الدفع ',
    'update_payment_metgods'=>' تعديل بيانات طرق الدفع ',
    'show_all_payment_metgods'=>' عرض طرق الدفع ',

    //order
    'order_name' => 'اسم الطلب',
    'customer' => 'اسم العميل',
    'image' => 'الصوره',
    'paid'=>'تم البيع' ,
    'Pending'=>'قيد الانتظار' ,
    'order_info'=>'معلومات الطلب' ,

    //order details........

    'customer_info'=>'بيانات العميل' ,
    'customer_name'=>'اسم العميل' ,
    'order_details'=>'بيانات الطلب' ,
    'order_date'=>'وقت وتاريخ الطلب' ,
    'order_status'=>'حاله الطلب' ,
    'order_total'=>'السعر الكلى' ,
    'order_payment'=>'طريقه الدفع' ,

    'order_address'=>'عنوان الشحن' ,
    'order_quantity'=>'كميه الطلب' ,

    //





];

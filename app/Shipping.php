<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    //
    public function description()
    {
        return $this->hasMany('App\ShippingDescription','shipping_id');
    }
}

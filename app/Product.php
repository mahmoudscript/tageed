<?php

namespace App;

use App\ProductImages;


use Illuminate\Database\Eloquent\Model;

class Product extends Model
{



    public function description(){


        return $this->hasMany('App\ProductDescription') ;
    }



    public function user(){
        return $this->belongsTo('App\User');
    }

    



    public function cart()
    {

        return $this->hasMany('App\Cart','product_id');

    }

   public function view()
    {

        return $this->hasMany('App\View','product_id');

    }

    public function viewCount()
    {

        return $this->view()
            ->selectRaw('product_id, count(product_id) as viewCount')
            ->groupBy('product_id');

    }





    public function images(){


        return $this->hasMany('App\ProductImages') ;
    }





    public function brand()
    {

        return $this->belongsTo('App\Brand','brand_id');

    }

    public function department()
    {

        return $this->belongsTo('App\Department','cat_id');

    }

    public function oneImage($id)
    {
        // get the first image of adv 

        $image = ProductImages::where('product_id', $id)->first();

        return $image->name ;

    }
    


}

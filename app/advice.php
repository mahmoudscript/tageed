<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class advice extends Model
{
    //
    public function description()
    {
        return $this->hasMany('App\adviceDescription','advice_id');
    }
}

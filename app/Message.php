<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //

    protected $fillable= [
        'is_read','sender_id','receiver_id','message','type'
    ];

    protected $casts=['created_at'=>'timestamp' ];

   

    public function sender(){
        return $this->belongsTo('App\User' , 'sender_id')
        ->select('id','name','photo');
    }

    public function receiver(){
        return $this->belongsTo('App\User' , 'receiver_id')
            ->select('id','name','photo' );
    }




    public function conversation(){
        return $this->belongsTo('App\Conversation' , 'conversation_id');

    }


}

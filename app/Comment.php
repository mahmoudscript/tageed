<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //
    protected $table='comments';

    protected $fillable= [

       'id', 'product_id', 'user_id'  ,'comment'
    ];

    protected $casts = ['created_at'=>'timestamp' ];

    public function user(){
        return $this->belongsTo('App\User' , 'user_id')
            ->select('name','photo','id');
    }


    public function advert(){
        return $this->belongsTo('App\Product' , 'product_id');
    }


}

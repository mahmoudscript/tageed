<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class adviceDescription extends Model
{
    //
    public function advice()
    {

        return $this->belongsTo('App\advice');
    }


    public function language()
    {
        return $this->belongsTo('App\Language','language_id');
    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

use View;
use DB;
use App\About;
use App\Product;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);

        view::composer('*' , function ($view){
        
                // Footer data

                $about = About::take(1)->first();
                $products = Product::take(5)->get();
               
        
                $view-> with('about' , $about) ;
                 $view-> with('products' , $products) ;

                });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

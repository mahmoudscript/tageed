<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class department_description extends Model
{
    //

    public function department()
    {

        return $this->belongsTo('App\Departments','department_id');

    }

   public function language()
    {

        return $this->belongsTo('App\Language','language_id');

    }


}

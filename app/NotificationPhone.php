<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationPhone extends Model
{
    //


    protected $fillable= [
        'is_read','notifier_id','notify_from','type' ,'product_id'
    ];



    protected $casts = ['created_at'=>'timestamp' ];

    protected $hidden = ['updated_at'] ;

    public function notifier(){
        return $this->belongsTo('App\User' , 'notifier_id');
    }

    public function notifyFrom(){
        return $this->belongsTo('App\User' , 'notify_from')
            ->select('id','name','photo');
    }

    public function advert()
    {
        return $this->belongsTo('App\Product','product_id');
    }

    public function markAsRead($notification){

        return $notification->update(['is_read'=>1]);

    }


}

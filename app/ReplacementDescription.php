<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplacementDescription extends Model
{
    //
    public function replacement()
    {

        return $this->belongsTo('App\Replacement');
    }


    public function language()
    {
        return $this->belongsTo('App\Language','language_id');
    }
}

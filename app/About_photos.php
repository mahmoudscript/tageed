<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About_photos extends Model
{
    //
    public function about()
    {

        return $this->belongsTo('App\About');
    }

}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    //
    protected $fillable = ['id'];

    public function description()
    {
        return $this->hasMany('App\About_description','about_id');
    }

    public function photos()
    {
        return $this->hasMany('App\About_photos','about_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    public function description()
    {
        return $this->hasMany('App\PaymentDescriptions','payment_id');
    }
}

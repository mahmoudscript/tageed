<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgencyDescription extends Model
{
    public function agency(){


        return $this->hasMany('App\Agency','agency_id') ;
    }

    public function language()
    {

        return $this->belongsTo('App\Language','language_id');

    }
}

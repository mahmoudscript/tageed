<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentDescriptions extends Model
{
    //
    public function payment()
    {

        return $this->belongsTo('App\Payment');
    }


    public function language()
    {
        return $this->belongsTo('App\Language','language_id');
    }
}

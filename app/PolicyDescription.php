<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PolicyDescription extends Model
{
    //
    public function policy()
    {

        return $this->belongsTo('App\Policy');
    }


    public function language()
    {
        return $this->belongsTo('App\Language','language_id');
    }
}

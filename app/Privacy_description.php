<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Privacy_description extends Model
{
    //
    public function Privacy()
    {

        return $this->belongsTo('App\Privacy','privacy_id');

    }

    public function language()
    {

        return $this->belongsTo('App\Language','language_id');

    }
}

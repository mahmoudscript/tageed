<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    //
    protected $fillable=['parent_id'];
    public function description(){


        return $this->hasMany('App\department_description') ;
    }

    public function parent(){

        return $this->hasMany('App\Department','parent_id') ;
    }

    public function product(){

        return $this->hasMany('App\Product','cat_id') ;
    }

    public function productCount(){
        return $this->product()
            ->selectRaw('cat_id, count(cat_id) as productCount')
            ->groupBy('cat_id');

    }









}

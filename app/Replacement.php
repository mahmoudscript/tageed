<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replacement extends Model
{
    //
    public function description()
    {
        return $this->hasMany('App\ReplacementDescription','replacement_id');
    }
}

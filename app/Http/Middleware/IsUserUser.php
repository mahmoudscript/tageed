<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsUserUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role == 'user') {
            return $next($request); //process request normally if admin
        }elseif(Auth::user()->role == 'admin') {
            return redirect('/login'); //home page if not user
        }else {
            return redirect('/login'); //home page if not user
        }
    }
}

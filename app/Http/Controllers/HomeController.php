<?php

namespace App\Http\Controllers;
use App\ProductImages;
use App\Brand;
use App\Agency;
use App\advice;
use App\Cart;
use App\Product;
use App\ProductDescription;

use App\About;
use App\ContactUs;
use App\Policy;
use App\Privacy;
use App\Replacement;
use App\User;
use Auth;

use App\Shipping;
use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;

use App\Department;
use App\department_description;
 

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              $searchdepartments = Department::all();

            $departments = Department::take(12)->get();

            // get adverts 
            $adverts = Product::take(9)->get();

        return view('frontend.index',compact('departments','adverts','searchdepartments'));
    }


    public function register()
    {

        return view('frontend.register');
    }

    public function registerform(Request $request)
    {
        // create new user :-
        
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
         $user->password =bcrypt( $request->get('password'));

         // upload use Image 
           if($request->hasFile('file')){


                // in update we will check first if user has an image or not to be replaced 

                $logo = $request->file('file'); 
                $extension = $logo->getClientOriginalExtension();
                $logo_rename = time() . '.' .$logo->getClientOriginalExtension(); 



                $logo->move(public_path('uploads/users_photos'), $logo_rename) ; 


                 
                $user->photo = $logo_rename ; 

        } 

         if($user->save()){

                    Auth::loginUsingId($user->id);
                    return redirect()->Route('homepage');
   
                }

    }

    public function loginpage(){

        return view('frontend.loginpage');

    }

    public function loginform(Request $request){

        $email = $request->email ; 
        $password = $request->password ;
        // dd($email);

        if (   (Auth::attempt(['email' => $email, 'password' => $password ]) )  ) {
                    $user = Auth::user() ;
                     Auth::login($user);

                     $user->updated_at = Carbon::now()->toDateTimeString();
                      return redirect()->Route('homepage');
                    // return response()->json(['status'=>'success']);

            }else{
                return redirect()->Route('homepage');
                // return response()->json(['status'=>'not_found']);

            }

    }



    public function user_ads($id)
    {       
        // get data of this user :-
        $user = User::find($id);

        //get user Adverts :- 

        $adverts = Product::where('user_id',$id)->get();

        return view('frontend.user_ads', compact('user','adverts'));
    }

     public function Favorits_ads()
    {

        return view('frontend.Favorits-ads');
    }


     public function category()
    {

        return view('frontend.category');
    }

     public function ad_details($id)
    {
        $advert = Product::find($id);
        return view('frontend.ad-details',compact('advert'));
    }



    

    public function advpage()
    {
        // get all Cats of products :- 

        $cats = Department::all();
 
        return view('frontend.advpage',compact('cats'));
    }

    public function addadvert(Request $request)
    {
       // here will save data of advert :-
       
       $adv = new Product();

       $adv->title = $request->title;
       $adv->description = $request->description;
       $adv->price = $request->price;
       $adv->status = $request->status;
       $adv->cat_id = $request->cat ;
       $adv->user_id = Auth::user()->id ;  

       $adv->save();

       // add image to advrts ":- 

       if ($request->hasFile('logo')) {

        // in update we will check first if user has an image or not to be replaced

        // dd($request->file('logo'));
        $logo = $request->file('logo');
        $extension = $logo->getClientOriginalExtension();
        $logo_rename = time() . '.' . $logo->getClientOriginalExtension();

        $logo->move(public_path('uploads/product_images'), $logo_rename);

        // create new inst 

        $advimage = new ProductImages();

        $advimage->name =  $logo_rename;

        // dd($adv->id);
        $advimage->product_id = $adv->id;
        $advimage->save();


       
    }
             
       
      


       
       return redirect("/ad_details/$adv->id");


    }



    public function contactus(Request $request)
    {
        $about = new ContactUs();
        $about->name = $request->name;
        $about->email = $request->email;
        $about->message = $request->message;
        $about->phone = $request->phone;

        $about->save();

        return back();
    }

    public function catadverts(){

        $cats = Department::all();

        return view('frontend.catadv',compact('cats'));

    }

    public function catadvertsdetails($id)
    {
        $adverts = Product::where('cat_id',$id)->get();
        return view('frontend.catadvertsdetails',compact('adverts'));
    }


    public function searchresult(Request $request)
    {
        // search about Adverts :- 
        
        $department_id = $request->department_id;
        $keyword       = $request->keyword;

            // Search Query :-
          $adverts = Product::where('title','LIKE','%'.$keyword.'%')->where('cat_id',$department_id)->get();

          // dd($adverts);
        return view('frontend.searchresult',compact('adverts'));
    }


    



}

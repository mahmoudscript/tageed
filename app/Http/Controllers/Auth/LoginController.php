<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


   public function loginUser(Request $request)
    {

        if($request->ajax()){

            $email_phone = $request->get('email_phone');
            $password = $request['password'];
            if(empty($email_phone)){
                return response()->json(['status'=>'empty_email_phone']);
            }

            if(empty($password)){
                return response()->json(['status'=>'empty_password']);
            }

            if (   (Auth::attempt(['email' => $email_phone, 'password' => $password ]) )  ||  (Auth::attempt(['phone' => $email_phone, 'password' => $password ]) ) ) {
                    $user = Auth::user() ;
                     Auth::login($user);
                    return response()->json(['status'=>'success']);


            }else{

                return response()->json(['status'=>'not_found']);

            }

        }

    }





}

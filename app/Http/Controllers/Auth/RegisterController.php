<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
//use Socialite;
use Laravel\Socialite\Facades\Socialite;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/index';


    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'role'=>'admin',
        ]);
    }


    public function register(Request $request){

      // return 'hi';

        // create default user object
        $user = new User ;


        if($request->ajax()){

            $user->name = $request->get('name');
            $user->email = $request->get('email');
            $user->phone = $request->get('phone');
            $user->password =bcrypt( $request->get('password'));
            $user->role = 'user';


                if($user->save()){

                    Auth::loginUsingId($user->id);

                    $request->session()->flash('registration_success', trans('frontend.registration_success'));

                    return response()->json(['status'=>'success']);

                }

            }
        }


    public function redirect($service) {
        return Socialite::driver($service)->redirect();
    }


    public function callback($service) {
                 //return 'error';
        dd(Socialite::driver($service)->userFromTokenAndSecret('0rOqpB7ySphoTr6XYmZqB6V0d', 'lr9x1DgkrpQLZv3hdsOHJQZOiOHW8lau0ih8TtvNDAyi6oYnet'));

        $user = Socialite::driver($service)->user();

        $authUser = $this->findOrCreateUser($user, $service);
        Auth::login($authUser, true);
        return redirect('/');
    }


    public function findOrCreateUser($user, $service)
    {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'name'     => $user->name,
            'email'    => $user->email,
            'provider' => $provider,
            'provider_id' => $user->id
        ]);
    }

}






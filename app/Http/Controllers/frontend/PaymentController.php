<?php

namespace App\Http\Controllers\frontend;


use App\Cart;
use App\ShippingAddres;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class PaymentController extends Controller
{
    //
    public function payment(Request $request)
    {

        return view('frontend.pages.payement');
    }

    public function confirm(Request $request)
    {
        $user_id=Auth::user()->id;
        $confirm=Cart::where('user_id',$user_id)
            ->update(['status'=>'confirmed','payment_method'=>'On Reciving']);

        return back();
    }

    public function saveAddress(Request $request){
        
       // dd($request) ;

        $shipping_name=$request['shipping_name'];
        $shipping_address=$request['shipping_address'];
        $shipping_phone=$request['shipping_phone'];
        $shipping_city=$request['shipping_city'];
        $shipping_method=$request['shipping_city'];

        $last_address=ShippingAddres::where('user_id',Auth::user()->id)
            ->first();
        if(count($last_address)>0){

            $last_address->update([
                'name'=>$shipping_name,
                'address'=>$shipping_address ,
                'phone'=>$shipping_phone ,
                'city'=>$shipping_city
                
            ]);

        }else{
            $address= new ShippingAddres();
            $address->name=$shipping_name;
            $address->address=$shipping_address;
            $address->phone=$shipping_phone;
            $address->city=$shipping_city;
            $address->user_id=Auth::user()->id;
            $address->save();

        }


        $carts=Cart::where('user_id',Auth::user()->id)
            ->get();

        foreach($carts as $cart){

            $cart->update([
                'payment_method'=>'On Reciving',
                'status'=>'confirmed'

            ]);

        }


       return back();
        
        


    }


}

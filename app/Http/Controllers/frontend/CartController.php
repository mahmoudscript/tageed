<?php

namespace App\Http\Controllers\frontend;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CartController extends Controller
{

    public function GetCart(Request $request ,$id=""){


             
        $auth_user= Auth::user()->id;

        if($id !="") {

            $product_price=Product::find($id)->first()->price;

            $cart = Cart::where(['user_id' => $auth_user, 'product_id' =>$id,'status'=>'not_confirmed'])
                ->first();

            if (count($cart)>0) {

                $carts=Cart::where(['user_id'=>$auth_user,'status'=>'not_confirmed'])
                    ->with('product.description')
                    ->with('product.images')
                    ->get();
                
            }else{



                $addToCart = new Cart();
                $addToCart->user_id = $auth_user;
                $addToCart->product_id = $id;
                $addToCart->price = $product_price;
                $addToCart->total_price = $product_price;
                $addToCart->save();

                $carts=Cart::where(['user_id'=>$auth_user,'status'=>'not_confirmed'])
                    ->with('product.description')
                    ->with('product.images')
                    ->get();
            }

        }else{

           // dd('hi');
            $carts=Cart::where(['user_id'=>$auth_user,'status'=>'not_confirmed'])
                ->with('product.description')
                ->with('product.images')
                ->get();
        }

        $total_prices=0;
        foreach($carts as $cart){
            $total_prices =$total_prices + ($cart->price *  $cart->quantity);
        }

        //return $total_prices;

        return view('frontend.pages.cart',compact('carts','total_prices'));
    }

    public function changePrice(Request $request ,$id){

        $auth_user= Auth::user()->id;
        $amount=$request->get('amount');

        $cart = Cart::where(['user_id' => $auth_user, 'product_id' =>$id])
            ->first();
        $total_price = ($cart->price * $amount);
        $cart->quantity=$amount;
        $cart->total_price = $total_price;
        //$cart->save();

        if($cart->save()){


            return response()->json(['status'=>'success']);

        }

    }


    public function deleteProduct($id){
        $auth_user= Auth::user()->id;

        $cart=Cart::where(['user_id'=>$auth_user,'product_id'=>$id])
            ->first();
        $cart->delete();

        session()->flash('success' , trans('backend.product_message_deleted'));
        return redirect()->route('cart_list');

    }
}

<?php

namespace App\Http\Controllers\frontend;

use App\Favourite;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
class FavouriteController extends Controller
{
    //

    public function GetFavourite(Request $request){

        $auth_user= Auth::user()->id;
        $favourite=Favourite::where('user_id',$auth_user)
            ->with('product.description')
            ->with('product.images')
            ->get();
         //return $favourite;

        return view('frontend.pages.favourite',compact('favourite'));
    }

    public function addToFavourite($id){

        $auth_user= Auth::user()->id;
        $favourite=Favourite::where(['user_id'=>$auth_user ,'product_id'=>$id])
            ->first();
        if(count($favourite)>0){

            return response()->json(['status'=>'true']);


        }else {
            $addToFav = new Favourite();
            $addToFav->user_id=$auth_user;
            $addToFav->product_id=$id;
            $addToFav->save();

            return response()->json(['status'=>'true']);


        }
    }
}

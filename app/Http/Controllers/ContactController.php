<?php

namespace App\Http\Controllers;

use App\ContactUs;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    //

    public function postContact(Request $request){

        //dd($request);
        if($request->ajax()){


            $name    = $request->name;
            $email   = $request->email;
            //$subject   = $request->msg_subject;
            $message = $request->message;



            // this part for saving contact in database

            $contact = new ContactUs() ;

            $contact->Name = $name ;
            $contact->email = $email ;
            $contact->message = $message ;


            $contact->save();



            return response()->json(['status'=>'contact_sent']);


        }



    }


    public function postContactUs(Request $request){

        //dd($request);
        if($request->ajax()){


            $name    = $request->name;
            $phone    = $request->phone;
            $subject    = $request->subject;
            $email   = $request->email;
            $message = $request->message;



            // this part for saving contact in database

            $contact = new ContactUs() ;

            $contact->Name = $name ;
            $contact->email = $email ;
            $contact->phone = $phone ;
            $contact->subject = $subject ;
            $contact->message = $message ;
            $contact->save();



            return response()->json(['status'=>'contact_sent']);


        }



    }




}

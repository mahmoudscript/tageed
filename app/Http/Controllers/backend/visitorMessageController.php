<?php

namespace App\Http\Controllers\backend;


use App\ContactUs;
use App\Http\Controllers\Controller;
use App\Language;
use App\Mail\OrderShipped;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class visitorMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $visitorsMessages=ContactUs::orderBy('created_at','desc')->get();

        return view('backend.visitorsMessages.index',compact('visitorsMessages'));
    }


    public function show($id)
    {
        //
        $visitorsMessage=ContactUs::where('id',$id)->first();
        $languages=Language::where('status','1')->get();
        // dd($visitorsMessage);
        return view('backend.visitorsMessages.show',compact('visitorsMessage','languages'));

    }



    public function replyMsg(Request $request)
    {

        $email=$request['email'];
        $name=$request['name'];
        $message=$request['message'];
       // dd($email);

        Mail::send('emails.contact', [ 'newmessage'=>$message ,'name'=>$name ,'email'=>$email
        ], function ($m) use ($email ,$name ) {

            $m->from('info@halahhealth.com', 'HalahHealth');
            $m->to($email, $name)->subject('Reply!');

        });
        
        session()->flash('success' , trans('backend.user_message_sent'));


        return redirect()->route('visitorsMessages.index');

    }


    
    public function destroy($id)
    {

        ContactUs::destroy($id);

        session()->flash('success' , trans('backend.message_deleted'));


        return redirect()->route('visitorsMessages.index');

    }
}

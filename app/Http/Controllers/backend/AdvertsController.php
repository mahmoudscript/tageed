<?php

namespace App\Http\Controllers\backend;

use App\Article;
use App\article_description;
use App\Article_image;
use App\Http\Controllers\Controller;
use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Storage ;
use File ;

class AdvertsController  extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        $articles=Article::where('type','advert')->with('description')->get();
     //  return $articles;
        return view('backend.advert.index',compact('articles','languages'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $languages=Language::where('status','1')->get();

        return view('backend.advert.create',compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $rules=[
            "name_ar" => "required|min:3|max:255" ,
            "desc_ar" =>"required|min:5" ,
            "name_en" => "required|min:3|max:255",
            "desc_en" =>"required|min:3",
            "image" =>"image|required"
        ];

        $this->validate($request , $rules) ;

            $pic='';
            /** upload image ....... */
            if ($request->hasFile('image')) {
                $pic = $this->uploadImage($request->file('image'));
            }
            /**  create new doctor @var  $doctor  */
            $article=new Article();
            $article->addedBy_id=Auth::user()->id;
            $article->type='advert';
            $article->image=$pic;
            $article->save();


            $languages = Language::where('status','1')->get();

            foreach($languages as $language) {

                $articleDescription = new article_description();
                $articleDescription->article_id=$article->id;
                $articleDescription->language_id=$language->id;
                $articleDescription->type='advert';
                $articleDescription->name=$request->input("name_$language->label");
                $articleDescription->description= $request->input("desc_$language->label");
                $articleDescription->save();

            }

            session()->flash('success' , trans('backend.article_message_added'));
            return redirect()->route('advert.index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //

        $languages=Language::where('status','1')->get();
        $adverts=Article::where(['type'=>'advert','id'=>$id])->with('description')->first();
      //  return $adverts;
        return view('backend.advert.show',compact('languages','adverts'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $languages=Language::where('status','1')->get();
        $adverts=Article::where(['type'=>'advert','id'=>$id])->with('description.languages')->first();
      //return $adverts;
        return view('backend.advert.edit',compact('languages','adverts'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
      //return $id;

        $rules=[
            "name_ar" => "required|min:3|max:255" ,
            "desc_ar" =>"required|min:5" ,
            "name_en" => "required|min:3|max:255",
            "desc_en" =>"required|min:3",
            "image" =>"image|"
        ];

        $this->validate($request , $rules) ;

            $article=Article::find($id);
            
            /** upload image ....... */
            if ($request->hasFile('image')) {
                $pic = $this->uploadImage($request->file('image'));
                $article->image=$pic;
                $article->save();
            }



            $descriptions = $article->description;


        // return  $descriptions;
            // $slug =  new Slug ;
            foreach ($descriptions as $description) {


                $description->article_id = $article->id ;
                $description->language_id = $description->language_id ;

                if ($request->has("name_".$description->languages->label)) {
                    $description->name = $request->input("name_".$description->languages->label);
                }

                if ($request->has("desc_".$description->languages->label)) {

                    $description->description = $request->input("desc_".$description->languages->label);
                }

                $description->save();
            }

            session()->flash('success' , trans('backend.article_message_added'));
            return redirect()->route('advert.index');
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $doctor=Article::find($id);
        $image=$doctor->imageName;
        //delete old image...................
        $path = public_path().'/uploads/articles/'.$image;
        File::delete($path);
        Article::destroy($id);

        session()->flash('success' , trans('backend.articles_message_deleted'));
        return redirect()->route('advert.index');
    }

    public function uploadImage($image)
    {
        $fileName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();


        if ($image) {
            //rename uploaded file
            $new_image_name = md5(date("u") . rand(1, 1000) . uniqid('@@yas##_') .$fileName) . '.' .$fileExtension ;

            //move file
            $image->move(public_path('uploads/articles'), $new_image_name);

            return $new_image_name;
        }
        return null;
    }
}

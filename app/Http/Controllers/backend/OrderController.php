<?php

namespace App\Http\Controllers\backend;


use App\Cart;
use App\Language;
use App\Product;
use App\ShippingAddres;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    //

    public function index()
    {
        
        $languages=Language::where('status','1')->get();
        $orders=Product::where('status','special')
            ->with('images')
            ->with('user')
            ->orderBy('created_at','desc')
            ->get();

      // return $orders;


        return view('backend.orders.index',compact('orders','languages'));
    }

    public function show($id)
    {
        $languages=Language::where('status','1')->get();
        $order=Product::where('id',$id)
            ->with('images')
            ->with('user')
            ->first();



        
        return view('backend.orders.show',compact('order','languages','address'));

    }


    public function destroy($id)
    {
        $cart = Product::find($id);

        $cart->status= 'not_special' ;
        $cart->save();


        session()->flash('success' , trans('backend.message_deleted'));

        // return to a specific view
        return redirect()->route('orders.index');

    }

    public function makeSpecial($id , Request $request){

        $advert=Product::find($id);
        
        $advert->days=$request->days;
        $advert->save();
        session()->flash('success' , "تم تميز الاعلان $request->days ايام ");

        // return to a specific view
        return redirect()->route('orders.index');

    }



}

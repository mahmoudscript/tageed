<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Language;
use App\Policy;
use App\PolicyDescription;
use Illuminate\Http\Request;

class PolicyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        $advices=Policy::with('description')
            ->orderBy('created_at','desc')
            ->get();


        return view('backend.polices.index',compact('advices','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $languages=Language::where('status','1')->get();

        return view('backend.polices.create',compact('languages'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;


        $advice= new Policy();

        $advice->save();

        $languages=Language::where('status','1')->get();

        foreach($languages as $lang){
            // dd("name_".$lang->label);
            $adviceDescription=new PolicyDescription();
            $adviceDescription->language_id=$lang->id;
            $adviceDescription->policy_id=$advice->id;
            $adviceDescription->title=$request->input("name_$lang->label");
            $adviceDescription->content=$request->input("desc_$lang->label");
            $adviceDescription->save();

        }

        session()->flash('success' , trans('backend.message_added'));
        return redirect()->route('policy.index');



    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $advice=Policy::where('id',$id)->with('description')->first();
        $languages=Language::where('status','1')->get();

        return view('backend.polices.edit',compact('languages','advice'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;


        $advice=Policy::find($id);



        $languages=Language::where('status','1')->get();

        foreach($advice->description as $description){
            // dd("name_".$lang->label);

            $description->language_id=$description->language_id;
            $description->policy_id=$advice->id;
            $description->title=$request->input("name_".$description->language->label);
            $description->content=$request->input("desc_".$description->language->label);
            $description->save();

        }

        session()->flash('success' , trans('backend.message_update'));
        return redirect()->route('policy.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advice = Policy::find($id);

        $advice->delete() ;



        session()->flash('success' , trans('backend.message_deleted'));

        // return to a specific view
        return redirect()->route('policy.index');

    }

}

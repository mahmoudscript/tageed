<?php

namespace App\Http\Controllers\backend;

use App\Agency;
use App\AgencyDescription;
use App\Http\Controllers\Controller;
use App\Language;
use File;
use Illuminate\Http\Request;

class AgencyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        $advices=Agency::with('description')
            ->orderBy('created_at','desc')
            ->get();


        return view('backend.agencies.index',compact('advices','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $languages=Language::where('status','1')->get();

        return view('backend.agencies.create',compact('languages'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',
            'image'=>'required|',

        ];

        $this->validate($request , $rules) ;
        $pic='';
        /** upload image ....... */
        if ($request->hasFile('image')) {
            $pic = $this->uploadImage($request->file('image'));
        }

        $advice= new Agency();
        $advice->image=$pic;
        $advice->save();

        $languages=Language::where('status','1')->get();

        foreach($languages as $lang){
            // dd("name_".$lang->label);
            $adviceDescription=new AgencyDescription();
            $adviceDescription->language_id=$lang->id;
            $adviceDescription->agency_id=$advice->id;
            $adviceDescription->title=$request->input("name_$lang->label");
            $adviceDescription->content=$request->input("desc_$lang->label");
            $adviceDescription->save();

        }

        session()->flash('success' , trans('backend.message_added'));
        return redirect()->route('agencies.index');



    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $advice=Agency::where('id',$id)->with('description')->first();
        $languages=Language::where('status','1')->get();

        return view('backend.agencies.edit',compact('languages','advice'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;



        $advice=Agency::find($id);

        if ($request->hasFile('image')) {
            $pic = $this->uploadImage($request->file('image'));
            $advice->image=$pic;
            $advice->save();
        }


        $languages=Language::where('status','1')->get();

        foreach($advice->description as $description){
            // dd("name_".$lang->label);

            $description->language_id=$description->language_id;
            $description->agency_id=$advice->id;
            $description->title=$request->input("name_".$description->language->label);
            $description->content=$request->input("desc_".$description->language->label);
            $description->save();

        }

        session()->flash('success' , trans('backend.message_update'));
        return redirect()->route('agencies.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advice = Agency::find($id);
        $image=$advice->image;
        //delete old image...................
        $path = public_path().'/uploads/agencies/'.$image;
        File::delete($path);

        $advice->delete() ;



        session()->flash('success' , trans('backend.message_deleted'));

        // return to a specific view
        return redirect()->route('agencies.index');

    }

    public function uploadImage($image)
    {
        $fileName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();


        if ($image) {
            //rename uploaded file
            $new_image_name = md5(date("u") . rand(1, 1000) . uniqid('@@yas##_') .$fileName) . '.' .$fileExtension ;

            //move file
            $image->move(public_path('uploads/agencies'), $new_image_name);

            return $new_image_name;
        }
        return null;
    }


}

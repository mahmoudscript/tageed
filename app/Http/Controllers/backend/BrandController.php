<?php

namespace App\Http\Controllers\backend;

use App\Brand;
use App\BrandDescription;
use App\Http\Controllers\Controller;
use App\Language;
use Illuminate\Http\Request;
use File;
class BrandController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        $advices=Brand::with('description')
            ->orderBy('created_at','desc')
            ->get();


        return view('backend.brands.index',compact('advices','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $languages=Language::where('status','1')->get();

        return view('backend.brands.create',compact('languages'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',
            'image'=>'required|',

        ];

        $this->validate($request , $rules) ;
        $pic='';
        /** upload image ....... */
        if ($request->hasFile('image')) {
            $pic = $this->uploadImage($request->file('image'));
        }

        $advice= new Brand();
        $advice->image=$pic;
        $advice->save();

        $languages=Language::where('status','1')->get();

        foreach($languages as $lang){
            // dd("name_".$lang->label);
            $adviceDescription=new BrandDescription();
            $adviceDescription->language_id=$lang->id;
            $adviceDescription->brand_id=$advice->id;
            $adviceDescription->title=$request->input("name_$lang->label");
            $adviceDescription->content=$request->input("desc_$lang->label");
            $adviceDescription->save();

        }

        session()->flash('success' , trans('backend.message_added'));
        return redirect()->route('brands.index');



    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $advice=Brand::where('id',$id)->with('description')->first();
        $languages=Language::where('status','1')->get();

        return view('backend.brands.edit',compact('languages','advice'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;



        $advice=Brand::find($id);

        if ($request->hasFile('image')) {
            $pic = $this->uploadImage($request->file('image'));
            $advice->image=$pic;
            $advice->save();
        }


        $languages=Language::where('status','1')->get();

        foreach($advice->description as $description){
            // dd("name_".$lang->label);

            $description->language_id=$description->language_id;
            $description->brand_id=$advice->id;
            $description->title=$request->input("name_".$description->language->label);
            $description->content=$request->input("desc_".$description->language->label);
            $description->save();

        }

        session()->flash('success' , trans('backend.message_update'));
        return redirect()->route('brands.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advice = Brand::find($id);
        $image=$advice->image;
        //delete old image...................
        $path = public_path().'/uploads/brands/'.$image;
        File::delete($path);
        
        $advice->delete() ;



        session()->flash('success' , trans('backend.message_deleted'));

        // return to a specific view
        return redirect()->route('brands.index');

    }

    public function uploadImage($image)
    {
        $fileName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();


        if ($image) {
            //rename uploaded file
            $new_image_name = md5(date("u") . rand(1, 1000) . uniqid('@@yas##_') .$fileName) . '.' .$fileExtension ;

            //move file
            $image->move(public_path('uploads/brands'), $new_image_name);

            return $new_image_name;
        }
        return null;
    }

}

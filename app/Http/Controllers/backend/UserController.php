<?php

namespace App\Http\Controllers\backend;

use App\NotificationMobile;
use App\NotificationPhone;
use App\User;
use App\Http\Controllers\Controller;
use App\Language;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Validator;
use Storage ;
use File ;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $users  = User::whereIn('role' ,['admin','user'])
            ->orderBy('created_at','desc')
            ->get();

        return view('backend.users.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $languages=Language::where('status','1')->get();

        return view('backend.users.create',compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        //
        $rules=[
            "name" => "required|min:3|max:255" ,
            "email" => "required|min:3|max:255",
            "phone" =>"required|min:6",
            "image" =>"image|required" ,
            "password" =>"min:6|required" ,

        ];

        $this->validate($request , $rules) ;

            $pic='';
            /** upload image ....... */
            if ($request->hasFile('image')) {
                $pic = $this->uploadImage($request->file('image'));
            }
            /**  create new doctor @var  $doctor  */
            $user =new User();
            $user->name=$request->name;
            $user->password = bcrypt($request->password);
            $user->phone=$request->phone;
            $user->email=$request->email;
            $user->role='admin';
            $user->photo=$pic;
            $user->save();
            session()->flash('success' , trans('backend.user_message_added'));
            return redirect()->route('users.index');

    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::where(['status'=>'1'])->get();
        $user = User::find($id);

        return view('backend.users.edit',compact('languages','user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $rules=[
            "name" => "required|min:3|max:255" ,
            "email" => "required|min:3|max:255",
            "phone" =>"required|min:6",
            "image" =>"image|required" ,
           // "password" =>"min:6|required" ,
        ];

        $this->validate($request , $rules) ;

            $pic='';
            /** upload image ....... */
            if ($request->hasFile('image')) {
                $pic = $this->uploadImage($request->file('image'));
            }

            $user =User::find($id);
            $user->name=$request->name;
            $user->email=$request->email;
            $user->phone=$request->phone;
            $user->photo=$pic;
            $user->save();



            session()->flash('success' , trans('backend.user_message_updated'));
            return redirect()->route('users.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        User::destroy($id);

        session()->flash('success' , trans('backend.user_message_deleted'));
        return redirect()->route('users.index');
    }

    public function uploadImage($image)
    {
        $fileName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();


        if ($image) {
            //rename uploaded file
            $new_image_name = md5(date("u") . rand(1, 1000) . uniqid('@@yas##_') .$fileName) . '.' .$fileExtension ;

            //move file
            $image->move(public_path('uploads/users_photos'), $new_image_name);

            return $new_image_name;
        }
        return null;
    }
    //send a push message through the FCM......
    public function msgAll(Request $request){

        $users=User::where('role','user')->get();
        $message=$request['message'];
        foreach ($users as $device){

            $device_token= $device->device_token;

            $notification=new NotificationPhone();
            $notification->notifier_id=$device->id;
            $notification->notify_from=Auth::user()->id;
            $notification->product_id=null;
            $notification->is_read=0  ;
            $notification->type='public';
            $notification->message= $message;
            $notification->save();


            $notifications=NotificationPhone::where(['id'=>$notification->id])
                ->with('notifyFrom')
                ->first();

            //here the data should send with notification fcm.....

            $notificationData['id'] = $notification->id;
            $notificationData['notifier_id'] = $notification->notifier_id;
            $notificationData['message'] = $notification->message;

            $notificationData['notifyFrom']['id'] = $notification->notifyFrom->id;
            $notificationData['notifyFrom']['name'] = $notification->notifyFrom->name;


            if($notification->notifyFrom->photo) {
                $notificationData['notifyFrom']['photo'] = URL::to('uploads/users_photos/' . $notification->notifyFrom->photo);
            }else{
                $notificationData['notifyFrom']['photo']= "";
            }



            // here we write the code to send notification through firebase.....

            if($device_token) {

                //here we sort the data which will send through fcm.......
                $data = [
                    'title' => 'Tagged',
                    'message' => 'new message',
                    'type' => 'public',
                    'data' => $notificationData
                ];

                // Sending a Downstream Message to a Device

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60 * 20);

                $notificationBuilder = new PayloadNotificationBuilder('Tagged');
                $notificationBuilder->setBody($data)
                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData( $data);

                $option = $optionBuilder->build();
                $notification =null;
                $data = $dataBuilder->build();

                $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);


                //this if you want to check the notification sent to device or not ....
            // return  $downstreamResponse->numberSuccess();
                //   $downstreamResponse->numberFailure();
                //   $downstreamResponse->numberModification();

            }




        }


        session()->flash('success' , trans('backend.user_message_sent'));
        return redirect()->route('users.index');


    }

    //it is for showing the plade to send a single message to user
    public function show($id)
    {

        return view('backend.users.show',compact('id'));
    }

    //send a message to user as an FCM .................
    public function postMsg($id ,Request $request)
    {
        $message = $request['message'];

        $user = User::where('id', $id)->first();


        $device_token = $user->device_token;

        $notification = new NotificationPhone();
        $notification->notifier_id = $user->id;
        $notification->notify_from = Auth::user()->id;
        $notification->product_id = null;
        $notification->is_read = 0;
        $notification->type = 'public';
        $notification->message = $message;
        $notification->save();


        $notifications = NotificationPhone::where(['id' => $notification->id])
            ->with('notifyFrom')
            ->first();

        //here the data should send with notification fcm.....

        $notificationData['id'] = $notification->id;
        $notificationData['notifier_id'] = $notification->notifier_id;
        $notificationData['message'] = $notification->message;

        $notificationData['notifyFrom']['id'] = $notification->notifyFrom->id;
        $notificationData['notifyFrom']['name'] = $notification->notifyFrom->name;


        if ($notification->notifyFrom->photo) {
            $notificationData['notifyFrom']['photo'] = URL::to('uploads/users_photos/' . $notification->notifyFrom->photo);
        } else {
            $notificationData['notifyFrom']['photo'] = "";
        }


        // here we write the code to send notification through firebase.....
//return $device_token;
        if ($device_token) {

            //here we sort the data which will send through fcm.......
            $data = [
                'title' => 'Tagged',
                'message' => 'new message',
                'type' => 'public',
                'data' => $notificationData
            ];



            // Sending a Downstream Message to a Device

            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60 * 20);

            $notificationBuilder = new PayloadNotificationBuilder('Tagged');
            $notificationBuilder->setBody($data)
                ->setSound('default');
                
                
            $dataBuilder = new PayloadDataBuilder();
            $dataBuilder->addData($data);

            $option = $optionBuilder->build();
            $notification = null;
            $data = $dataBuilder->build();

            $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);


            //this if you want to check the notification sent to device or not ....
         // return  $downstreamResponse->numberSuccess();
             // $downstreamResponse->numberFailure();
             //$downstreamResponse->numberModification();

        }





        session()->flash('success' , trans('backend.user_message_sent'));
        return redirect()->route('users.index');
    }


}

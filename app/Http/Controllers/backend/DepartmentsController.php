<?php

namespace App\Http\Controllers\backend;

use App\Department;
use App\department_description;
use App\Http\Controllers\Controller;
use App\Language;
use App\Product;
use Illuminate\Http\Request;
use Validator ;

class DepartmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        $departments=Department::where('parent_id',null)
            ->with('description')
            ->with('parent.description')
            ->orderBy('created_at','desc')
            ->get();

        //return $departments;

      return view('backend.departments.index',compact('departments','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

          $languages=Language::where('status','1')->get();
         // $departments=Department::where('parent_id',null)->with('description')->get();

      return view('backend.departments.create',compact('languages'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        //validation rule.....
          $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'image'=>'required'

          ];

         /*
          * validate the rules we put and it will automatically
          *  return the error which we handle in the blade
          */

        $this->validate($request , $rules) ;

           // create new department and assign requested variable to it...
           $departments= new Department();
           $departments->parent_id=null;

        $pic='';
        /** upload image ....... */
        if ($request->hasFile('image')) {
            $pic = $this->uploadImage($request->file('image'));
        }

           $departments->photo=$pic;
           $departments->save();
       

            $languages=Language::where('status','1')->get();

            foreach($languages as $lang){
               // dd("name_".$lang->label);
                $departmentDescription=new department_description();
                $departmentDescription->language_id=$lang->id;
                $departmentDescription->department_id=$departments->id;
                $departmentDescription->name=$request->input("name_$lang->label");
                $departmentDescription->save();

            }

            session()->flash('success' , trans('backend.department_message_added'));
            return redirect()->route('departments.index');



    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $department=Department::where('id',$id)->with('description')->first();
        $languages=Language::where('status','1')->get();
        $departments=Department::where('parent_id',null)->with('description')->get();

       //return $department->description;


        return view('backend.departments.edit',compact('languages','department','departments'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;


        $departments=Department::find($id);
        $departments->parent_id=null;

        $pic=$departments->photo;
        /** upload image ....... */
        if ($request->hasFile('image')) {
            $pic = $this->uploadImage($request->file('image'));
        }

        $departments->photo=$pic;
        $departments->save();

     //  return $departments->description;

        foreach($departments->description as $departmentDescription){


            
            $departmentDescription->language_id=$departmentDescription->language_id;
            $departmentDescription->department_id=$departmentDescription->department_id;
            $departmentDescription->name=$request->input("name_".$departmentDescription->language->label);
            $departmentDescription->save();

        }

        session()->flash('success' , trans('backend.department_message_updated'));
        return redirect()->route('departments.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Department = Department::find($id);

        $Department->delete() ;



        session()->flash('success' , trans('backend.department_message_deleted'));

        // return to a specific view
        return redirect()->route('departments.index');

    }

    /*
     * this function to handle images
     * it take the file image
     * and put it's contant in aspacific file
     * return the image name
     */
    public function uploadImage($image)
    {
        if ($image) {


            $fileName = $image->getClientOriginalName();
            $fileExtension = $image->getClientOriginalExtension();


            //rename uploaded file
            $new_image_name = md5(date("u") . rand(1, 1000) . uniqid('@@yas##_') .$fileName) . '.' .$fileExtension ;

            //move file
            $image->move(public_path('uploads/payment_photos'), $new_image_name);

            return $new_image_name;
        }
        return null;
    }


    public function addSub($id)
    {

        $languages=Language::where('status','1')->get();

        return view('backend.departments.add_sub',compact('languages','id'));

    }


    public function storeSub(Request $request ,$id){
        //validation rule.....
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'image'=>'required'

        ];

        /*
         * validate the rules we put and it will automatically
         *  return the error which we handle in the blade
         */

        $this->validate($request , $rules) ;

        // create new department and assign requested variable to it...
        $departments= new Department();
        $departments->parent_id=$id;

        $pic='';
        /** upload image ....... */
        if ($request->hasFile('image')) {
            $pic = $this->uploadImage($request->file('image'));
        }

        $departments->photo=$pic;
        $departments->save();


        $languages=Language::where('status','1')->get();

        foreach($languages as $lang){
            // dd("name_".$lang->label);
            $departmentDescription=new department_description();
            $departmentDescription->language_id=$lang->id;
            $departmentDescription->department_id=$departments->id;
            $departmentDescription->name=$request->input("name_$lang->label");
            $departmentDescription->save();

        }

        session()->flash('success' , trans('backend.department_message_added'));
        return redirect()->route('departments.index');



    }



    public function showSubs($id){

        $languages=Language::where('status','1')->get();
        $departments=Department::where('parent_id',$id)
            ->with('description')
            ->with('parent.description')
            ->orderBy('created_at','desc')
            ->get();

        return view('backend.departments.show_subs',compact('languages','departments'));
    }


    public function showAdverts($id)
    {

        $languages=Language::where('status','1')->get();
        $products=Product::where('cat_id',$id)
            ->with('description')
            ->with('user')
            ->orderBy('created_at','desc')
            ->get();

        return view('backend.product.index',compact('products','languages'));

    }

}

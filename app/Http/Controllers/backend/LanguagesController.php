<?php

namespace App\Http\Controllers\backend;

use App\Language;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class LanguagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        //dd($languages);
        return view('backend.languages.index',compact('languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $languages=Language::where('status','1')->get();
      //  dd($languages);

        return view('backend.languages.create',compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $language = new Language ;

        $language->name = $request->lang_name ;
        $language->label = $request->lang_label ;
        //$language->status = $request->lang_status == 'on' ? '1' : '0' ;


        $language->save();


        // flashing a success message
        session()->flash('success' , trans('backend.language_message_added'));

        // return to a specific view
        return redirect()->route('language.index');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::find($id) ;
        return view('backend.languages.edit' , compact('languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // validation in future

        // get language
        $language = Language::find($id) ;

        $language->name = $request->lang_name ;
        $language->label = $request->lang_label ;
       // $language->status = $request->lang_status == 'on' ? '1' : '0' ;


        $language->save();



        // flashing a success message
        session()->flash('success' , trans('backend.language_message_updated'));

        // return to a specific view
        return redirect()->route('languages.index');





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        /**
         * Since lang is very important to system
         * i will not allow him to delete it
         * instead showing him an error message
         */


        return back();
    }
}

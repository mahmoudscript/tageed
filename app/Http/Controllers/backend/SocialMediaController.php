<?php

namespace App\Http\Controllers\backend;

use App\Social_media;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Language ; 
use LaravelLocalization ; 

use Auth ; 



class SocialMediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        

        $social_media = Social_media::all();

        return view('backend.social_media.index' , compact('social_media')); 



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.social_media.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $rules=[
            'icon'=>'required|min:3|max:250' ,
            'name'=>'required|min:3|max:250',
            'url'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;



        $social = new Social_media ;


        $social->icon = $request->icon ;
        $social->name = $request->name ;
        $social->url = $request->url ;



        $social->save(); 




         // flashing a success message  
         session()->flash('success' , trans('backend.social_added'));

        // return to a specific view 
        return redirect()->route('social-media.index'); 





    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        

        $link = Social_media::find($id);

        return view('backend.social_media.edit' , compact('link'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules=[
            'icon'=>'required|min:3|max:250' ,
            'name'=>'required|min:3|max:250',
            'url'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;

        $link = Social_media::find($id);



        $link->icon = $request->icon ;
        $link->name = $request->name ;
        $link->url = $request->url ;
       



        $link->save() ; 




        session()->flash('success' , trans('backend.social_updated'));

        // return to a specific view 
        return redirect()->route('social-media.index'); 






    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        

       $link = Social_media::find($id);

       $link->delete() ; 



        session()->flash('success' , trans('backend.social_deleted'));

        // return to a specific view 
        return redirect()->route('social-media.index'); 


    }
}

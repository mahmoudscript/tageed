<?php

namespace App\Http\Controllers\backend;

use App\Brand;
use App\Department;
use App\Http\Controllers\Controller;
use App\Language;
use App\Product;
use App\ProductDescription;
use App\ProductImages;
use Illuminate\Http\Request;
use File;

class ProductController extends Controller
{


    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        $products=Product::with('description')
            ->with('user')
            ->orderBy('created_at','desc')
            ->get();

       // return $products;
        return view('backend.product.index',compact('products','languages'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $languages=Language::where('status','1')->get();
        $departments=Department::where('parent_id','!=',null)->with('description')->get();
        $brands=Brand::with('description')->get();

       // return  $brands;


     return view('backend.product.create',compact('languages','departments','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_en'=>'required|min:3',
            'desc_ar'=>'required|min:3',
            'addition_info_en'=>'required|min:3',
            'addition_info_ar'=>'required|min:3',
            'quantity'=>'required|integer',
            'price'=>'required|integer',
            'images'=>'required|',

        ];

        $this->validate($request , $rules) ;


        $product= new Product();
        $product->quantity=$request->quantity;
        $product->price=$request->price;
        $product->Status=$request->status;
        $product->special=$request->special;
        $product->cat_id=$request->cat_id;
        $product->brand_id=$request->brand_id;
        $product->save();

        $languages=Language::where('status','1')->get();

        foreach($languages as $lang){
            // dd("name_".$lang->label);
            $productDescription=new ProductDescription();
            $productDescription->language_id=$lang->id;
            $productDescription->product_id=$product->id;
            $productDescription->title=$request->input("name_$lang->label");
            $productDescription->description=$request->input("desc_$lang->label");
            $productDescription->addition_info=$request->input("addition_info_$lang->label");
            $productDescription->save();

        }

        if($request->hasFile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {

                $pic = $this->uploadImage($image);

                $productImages = new ProductImages();
                $productImages->name = $pic;
                $productImages->product_id = $product->id;
                $productImages->save();
            }
        }
        session()->flash('success' , trans('backend.product_message_added'));
        return redirect()->route('products.index');



    }



    public function show($id)
    {
        //

        $languages=Language::where('status','1')->get();

        $product=Product::where('id',$id)
            ->with('description')
            ->with('user')
            ->with('images')
            ->first();

        return view('backend.product.show',compact('product','languages','departments'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $languages=Language::where('status','1')->get();
        $departments=Department::where('parent_id','!=',null)->with('description')->get();
        $brands=Brand::with('description')->get();
        $product=Product::where('id',$id)
            ->with('description')
            ->with('images')
            ->first();
        //return $department->description;


        return view('backend.product.edit',compact('product','languages','departments','brands'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       //dd($request);
        
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_en'=>'required|min:3',
            'desc_ar'=>'required|min:3',
            'addition_info_en'=>'required|min:3',
            'addition_info_ar'=>'required|min:3',
            'quantity'=>'required|integer',
            'price'=>'required|integer',
           

        ];

        $this->validate($request , $rules) ;


        $product=Product::find($id);
        $product->quantity=$request->quantity;
        $product->price=$request->price;
        $product->Status=$request->status;
        $product->special=$request->special;
        $product->cat_id=$request->cat_id;
        $product->brand_id=$request->brand_id;
        $product->save();

        $languages=Language::where('status','1')->get();


      
        foreach($product->description as $description){
            // dd("name_".$lang->label);

            $description->language_id=$description->language_id;
            $description->product_id=$description->product_id;
            $description->title=$request->input("name_".$description->language->label);
            $description->description=$request->input("desc_".$description->language->label);
            $description->addition_info=$request->input("addition_info_".$description->language->label);
            $description->save();

        }

        if($request->hasFile('images')) {
            $images = $request->file('images');
            foreach ($images as $image) {

                $pic = $this->uploadImage($image);
                $productImages = new ProductImages();
                $productImages->name = $pic;
                $productImages->product_id = $product->id;
                $productImages->save();
            }

        }
        session()->flash('success' , trans('backend.product_message_updated'));
        return redirect()->route('products.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        $images=ProductImages::where('product_id',$id)->select('name')->get();

        foreach($images as $img){
           
            $path = public_path().'/uploads/product_images/'.$img->name;
            File::delete($path);
        }

        $product->delete() ;



        session()->flash('success' , trans('backend.product_message_deleted'));

        // return to a specific view
        return redirect()->route('products.index');

    }


    
     public function deleteSingleImage($id)
    {
        $product = ProductImages::find($id);



            $path = public_path().'/uploads/product_images/'.$product->name;
            File::delete($path);


        $product->delete() ;



        session()->flash('success' , trans('backend.image_message_deleted'));

        // return to a specific view
        return back();

    }




    public function uploadImage($image)
    {
        $fileName = $image->getClientOriginalName();
        $fileExtension = $image->getClientOriginalExtension();


        if ($image) {
            //rename uploaded file
            $new_image_name = md5(date("u") . rand(1, 1000) . uniqid('@@yas##_') .$fileName) . '.' .$fileExtension ;

            //move file
            $image->move(public_path('uploads/product_images'), $new_image_name);

            return $new_image_name;
        }
        return null;
    }
    
}

<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\About ; 
use App\About_description ;
use App\About_photos ;
use App\Language ; 
use LaravelLocalization ; 
use DB ; 
use Auth ; 
use File ; 


class AboutController extends Controller
{
    
	/**
	 * So here we will check if we have about information 
	 * then we will update it unless create a new one  
	 * cause this is a single blade 
	 * @return [type] [description]
	 */
	public function index(){

	    $about = About::updateOrCreate(['id'=>1]);

	    $languages = Language::where(['status'=>'1'])->get();
	    foreach($languages as $language){

		    $about_description = About_description::updateOrCreate([

		    	'about_id'=>$about->id , 
		    	'language_id'=>$language->id
		    ]);
	    }
	//return $about->description;
	    return view('backend.about.index')->withAbout($about)->withLanguages($languages);
    }

    public function update(Request $request , $id){

         //   dd($request) ;

        $rules=[
            'phone'=>'required|' ,
            'email'=>'required|min:3|max:250',
            'ios_app'=>'required|min:3|max:250',
            'google_store'=>'required|min:3|max:250',
            
            'name_en'=>'required|min:3|max:250',
            'name_ar'=>'required|min:3|max:250',
            'desc_en'=>'required|min:3',
            'desc_en'=>'required|min:5',
            'address_en'=>'required|min:5',
            'address_ar'=>'required|min:3',



        ];

        $this->validate($request , $rules) ;
    	 
    	$about = About::find($id);


    	//$about->status = $request->status == 'on' ? 'active':'not_active' ;
    	$about->phone = $request->phone ;
    	$about->email = $request->email ;
    	$about->ios_app =$request->ios_app;
    	$about->google_store =$request->google_store;
        $about->save();

		if($request->image){

              $image= $request->file('image');
           
     $extension = $image->getClientOriginalExtension();

     $image_rename = str_random(6). '.' .$image->getClientOriginalExtension();


      $image->move(public_path('uploads/about/'), $image_rename) ;

                    $about->image = $image_rename ;
                    $about->save();


                }


 


            $descriptions = $about->description ; 

            // $slug =  new Slug ; 
            foreach ($descriptions as $description) {
                
                
                $description->about_id = $about->id ; 
                $description->language_id = $description->language_id ;


                $description->title = $request->input("name_".$description->language->label);
                $description->description = $request->input("desc_".$description->language->label);
                $description->address = $request->input("address_".$description->language->label);
                $description->why_do_i_need_it = $request->input("why_do_i_need_it_".$description->language->label);


                $description->save(); 
            }


             // flashing a success message  
           session()->flash('success' , trans('backend.about_info_updated'));

                
    			return back(); 

    }






}

<?php

namespace App\Http\Controllers\backend;

use App\Privacy;
use App\Privacy_description;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\About ; 
use App\About_description ;
use App\About_photos ;
use App\Language ; 
use LaravelLocalization ; 
use DB ; 
use Auth ; 
use File ; 


class PrivaciesController extends Controller
{
    
	/**
	 * So here we will check if we have about information 
	 * then we will update it unless create a new one  
	 * cause this is a single blade 
	 * @return [type] [description]
	 */
	public function index(){

	    $privacies = Privacy::updateOrCreate(['id'=>1]);

	    $languages = Language::where(['status'=>'1'])->get();
	    foreach($languages as $language){

		    $about_description = Privacy_description::updateOrCreate([

		    	'privacy_id'=>$privacies->id ,
		    	'language_id'=>$language->id
		    ]);
	    }

	    return view('backend.privacies.index',compact('privacies','languages'));
    }

    public function update(Request $request , $id){

        $rules=[
            'content_ar'=>'required|min:3|' ,
            'content_en'=>'required|min:3|' ,


        ];

        $this->validate($request , $rules) ;

    	 
    	$about = Privacy::find($id);

    	$about->save(); 

            $descriptions = $about->description ; 


            foreach ($descriptions as $description) {
                
                
                $description->privacy_id = $about->id ;

                $description->language_id = $description->language_id ;



                $description->content = $request->input("content_".$description->languages->label);


 
                // saving department content !
                $description->save(); 
            }


             // flashing a success message  
                session()->flash('success' , trans('backend.terme_message_updated'));

                
    			return back(); 

    }





}

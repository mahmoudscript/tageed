<?php

namespace App\Http\Controllers\backend;

use App\ChampionshipReservationConfirmation;
use App\Http\Controllers\Controller;
use App\Language;
use App\NotificationMobile;
use App\stadium_description;
use App\StadiumReservation;
use App\StadiumReservationConfirmation;
use App\StudiumTimeTable;
use App\User;
use Edujugon\PushNotification\PushNotification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StadiumReservationController extends Controller
{
    //

    public function getReservation(){
        $languages=Language::where('status','1')->get();
        $stadiumReservation=StadiumReservationConfirmation::with('StadiumReservation.Stadium.description')
            // ->with('user')
            ->get();

      // return $stadiumReservation;
        return view('backend.reservation.index-stadium',compact('languages','stadiumReservation'));
    }


    public  function accpetReservation($id){



        $reservation =StadiumReservationConfirmation::where('id',$id)
            ->first();

        $reservationupdate =StadiumReservationConfirmation::where('id',$id)
            ->update(['status'=>'approve']);

        if($reservationupdate==true){

            $reservation_id= $reservation->reservation_id;
            $stadium_id=StadiumReservation::where('id',$reservation_id)->first()->stadium_id;
            $notification=new NotificationMobile();
            $notification->notifier_id=$reservation->user_id;
            //Auth::user()->id
            $notification->notify_from=1;
            $notification->stadium_id=$stadium_id;
            $notification->champion_id=null;
            $notification->invitation_id=null;
            $notification->is_read=0  ;
            $notification->type='approvePlayground';
            //$notification->save();


            //code for push notification ....

            $user=User::find($reservation->user_id);

            $userData= DB::table('users')->where('id',$user->id)
                ->select('id','name','photo','lang')
                ->first();

            $language=Language::where('label',$userData->lang)->first();
            $lang_id=$language->id;

            $stadiumData=stadium_description::where(['language_id'=>$lang_id,'stadium_id'=>$stadium_id])
                ->select('stadium_id','name','description')
                ->first();

            $data=[];
            $new['userData']=$userData;
            $new['stadiumData']=$stadiumData;

            $data=$new;
            $device_token=$user->device_token;
            $userLastLang=$user->lang;
            $Sdate=StadiumReservation::where('id',$reservation_id)->first()->date;
             $time_id=StadiumReservation::where('id',$reservation_id)->first()->time_id;
            $time=StudiumTimeTable::where(['date'=>$Sdate,'id'=>$time_id])->firs()->time;
            if($userLastLang=='ar'){
                $message=" $time  $Sdate في $stadiumData->name تم تأكيد حجز ملعب   " ;
            }else{
                $message="Your booking of the playground $stadiumData->name   has been confirmed in  $Sdate $time   ";
            }

            $notification->message= $message;
            $notification->save();

            $notifications=NotificationMobile::where(['id'=>$notification->id])
                ->with('teamInvitation.team')
                ->with('notifyFrom')
                ->with('stadium')
                ->with('champion')

                ->first();

            //return $data;
            if($device_token!= null) {
                $data = [

                    'data' => [
                        'title' => 'multiSport',
                         'message'=>$message,
                        'type'=>'approvePlayground',
                        'data'=>$notifications
                    ]

                ];

                $push = new PushNotification('fcm');
                $push->setMessage($data)
                    ->setDevicesToken($device_token)
                    ->send()
                    ->getFeedback();
                // dd($push->getFeedback());
            }

            session()->flash('success' , trans('backend.reservation_approve'));
            return redirect()->back();

        }else{

            session()->flash('success' , trans('backend.error'));
            return redirect()->back();
        }
    }


    public  function rejectReservation($id){

        

        $reservation =StadiumReservationConfirmation::where('id',$id)
            ->first();

        $reservationupdate =StadiumReservationConfirmation::where('id',$id)
            ->update(['status'=>'reject']);

        if($reservationupdate==true){

            $reservation_id= $reservation->reservation_id;
            $stadium_id=StadiumReservation::where('id',$reservation_id)->first()->stadium_id;
            $notification=new NotificationMobile();
            $notification->notifier_id=$reservation->user_id;
            //Auth::user()->id
            $notification->notify_from=1;
            $notification->stadium_id=$stadium_id;
            $notification->champion_id=null;
            $notification->invitation_id=null;
            $notification->is_read=0  ;
            $notification->type='rejectPlayground';
           // $notification->save();


            //code for push notification ....

            $user=User::find($reservation->user_id);

            $userData= DB::table('users')->where('id',$user->id)
                ->select('id','name','photo','lang')
                ->first();

            $language=Language::where('label',$userData->lang)->first();
            $lang_id=$language->id;

            $stadiumData=stadium_description::where(['language_id'=>$lang_id,'stadium_id'=>$stadium_id])
                ->select('stadium_id','name','description')
                ->first();

            $data=[];
            $new['userData']=$userData;
            $new['stadiumData']=$stadiumData;

            $data=$new;
            $device_token=$user->device_token;

            if($userLastLang=='ar'){
                $message="   بعدم الموافقه على حجز الملعب$userData->name قام " ;
            }else{
                $message="$userData->name  reject you to recerve the playground ";
            }
            
            $notification->message= $message;
            $notification->save();

            $notifications=NotificationMobile::where(['id'=>$notification->id])
                ->with('teamInvitation.team')
                ->with('notifyFrom')
                ->with('stadium')
                ->with('champion')

                ->first();

            //return $data;
            if($device_token!= null) {
                $data = [

                    'data' => [
                        'title' => 'multiSport',
                         'message'=>$message,
                        'type'=>'rejectPlayground',
                        'data'=>$notifications
                    ]

                ];

                $push = new PushNotification('fcm');
                $push->setMessage($data)
                    ->setDevicesToken($device_token)
                    ->send()
                    ->getFeedback();
                // dd($push->getFeedback());
            }

            session()->flash('success' , trans('backend.reservation_reject'));
            return redirect()->back();

        }else{

            session()->flash('success' , trans('backend.error'));
            return redirect()->back();
        }
    }



}

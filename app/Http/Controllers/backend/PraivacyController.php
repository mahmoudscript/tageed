<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Language;
use App\Privacy;
use App\Privacy_description;
use Illuminate\Http\Request;

class PraivacyController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $languages=Language::where('status','1')->get();
        $advices=Privacy::with('description')
            ->orderBy('created_at','desc')
            ->get();


        return view('backend.privacy.index',compact('advices','languages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $languages=Language::where('status','1')->get();

        return view('backend.privacy.create',compact('languages'));


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request);
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;


        $advice= new Privacy();

        $advice->save();

        $languages=Language::where('status','1')->get();

        foreach($languages as $lang){
            // dd("name_".$lang->label);
            $adviceDescription=new Privacy_description();
            $adviceDescription->language_id=$lang->id;
            $adviceDescription->privacy_id=$advice->id;
            $adviceDescription->title=$request->input("name_$lang->label");
            $adviceDescription->content=$request->input("desc_$lang->label");
            $adviceDescription->save();

        }

        session()->flash('success' , trans('backend.message_added'));
        return redirect()->route('privacy.index');



    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $advice=Privacy::where('id',$id)->with('description')->first();
        $languages=Language::where('status','1')->get();

       // return $advice;
        return view('backend.privacy.edit',compact('languages','advice'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'name_ar'=>'required|min:3|max:250' ,
            'name_en'=>'required|min:3|max:250',
            'desc_ar'=>'required|min:3|max:250' ,
            'desc_en'=>'required|min:3|max:250',

        ];

        $this->validate($request , $rules) ;


        $advice=Privacy::find($id);



        $languages=Language::where('status','1')->get();

        foreach($advice->description as $description){
            // dd("name_".$lang->label);

            $description->language_id=$description->language_id;
            $description->privacy_id=$advice->id;
            $description->title=$request->input("name_".$description->language->label);
            $description->content=$request->input("desc_".$description->language->label);
            $description->save();

        }

        session()->flash('success' , trans('backend.message_update'));
        return redirect()->route('privacy.index');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advice = Privacy::find($id);

        $advice->delete() ;



        session()->flash('success' , trans('backend.message_deleted'));

        // return to a specific view
        return redirect()->route('privacy.index');

    }



}

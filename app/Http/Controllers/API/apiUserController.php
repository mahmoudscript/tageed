<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Validator;

class apiUserController extends Controller
{
    
    public function login(Request $request){
        $email=$request['Email'];
        $password=$request['Password'];

        $validator=Validator::make($request->all(),[
            'Email'=> "email|required|string",
            'Password' => 'required|min:6|max:100',

        ]);
        if ($validator->passes()) {

            // $password=bcrypt($password);

            // return  $password;

            Auth::attempt(['email' => $email, 'password' => $password ]);
            $user = Auth::user() ;

            $userdevice=User::find($user->id);
            if($request->has('device_token')&& $request->device_token !=" "){
                $userdevice->device_token=$request->device_token;
                $userdevice->save();
            }



            if($user){

                $arr['id']=(string)$user->id;
                $arr['name']=(string)$user->name;
                $arr['email']=(string)$user->email;
                $arr['phone']=(string)$user->phone;
                $arr['latitude']=(string)$user->latitude;
                $arr['longitude']=(string)$user->longitude;
                if($user->photo==null){
                    $arr['photo'] = '';
                }else{
                    $arr['photo'] = URL::to('uploads/users_photos/' . $user->photo);
                }
                $arr['token']=$user->token;


                $msg='user login successfully' ;
                $response = [
                    'status' => true,
                    'value'=>'1',
                    'msg'=>$msg,
                    'user' => $arr
                ];
                return response()->json($response);

            }else{

                $msg='invalid user' ;
                $response = [
                    'status' => false,
                    'value'=>'0',
                    'msg'=>$msg,
                    'user' =>null
                ];
                return response()->json($response);


            }

        }else{


            foreach ((array)$validator->errors() as $value){
                if (isset($value['Email'])) {
                    $msg = 'invalid email';
                    return response()->json([
                        'status' => false,
                        'value' => '3',
                        'msg' => $msg,
                        'user' => null
                    ]);

                } elseif (isset($value['Password'])) {
                    $msg = 'invalid password';
                    return response()->json([
                        'status' => false,
                        'value' => '4',
                        'msg' => $msg,
                        'user' => null
                    ]);

                } else {
                    $msg = 'not login';
                    return response()->json([
                        'status' => false,
                        'value' => '6',
                        'msg' => $msg,
                        'user' => null
                    ]);
                }


            }

        }


    }

    public function register(Request $request){
        $validator=Validator::make($request->all(),[
            'Email'=> "email|unique:users,email|required|string",
            'Password' => 'required|min:6|max:100',
            'Name' => 'required|min:3|max:50|string',
            'Phone' => 'required|min:3|max:50|string',
            'device_token' => 'required',
        ]);
        if ($validator->passes()) {
            $password=$request['Password'];
            $name=$request['Name'];
            $email=$request['Email'];
            $phone=$request['Phone'];
            $device_token=$request['device_token'];
            $token=str_random(60);
            $user=new User();
            $user->name = $name;
            $user->email = $email;
            $user->phone = $phone ;
            $user->password = bcrypt($password);
            $user->device_token =$device_token ;
            $user->token=$token ;
             if($request->has('socialite_id')&&$request->socialite_id !='') {
                 $user->socialite_id=$request->socialite_id ;
             }

            $user->role='user';
            $user->save();

            $msg='user registered successfully' ;
            $response = [
                'status' => true,
                'value'=>'1',
                'msg'=>$msg,
                'user' => $user
            ];
            return response()->json($response);
        }else {


            foreach ((array)$validator->errors() as $value){
                if (isset($value['Name'])) {
                    $msg = 'invalid name';
                    return response()->json([
                        'status' => false,
                        'value' => '2',
                        'msg' => $msg,
                        'user' => null
                    ]);
                } elseif (isset($value['Email'])) {
                    $msg = 'invalid email';
                    return response()->json([
                        'status' => false,
                        'value' => '3',
                        'msg' => $msg,
                        'user' => null
                    ]);

                } elseif (isset($value['Password'])) {
                    $msg = 'invalid password';
                    return response()->json([
                        'status' => false,
                        'value' => '4',
                        'msg' => $msg,
                        'user' => null
                    ]);

                } elseif (isset($value['Phone'])) {
                    $msg = 'invalid phone';
                    return response()->json([
                        'status' => false,
                        'value' => '5',
                        'msg' => $msg,
                        'user' => null
                    ]);

                } else {
                    $msg = 'not Registered';
                    return response()->json([
                        'status' => false,
                        'value' => '6',
                        'msg' => $msg,
                        'user' => null
                    ]);
                }


            }
        }
    }

    public function socialLogin(Request $request){

        $social_id=$request['social_id'];

        $userInfo=User::where('socialite_id',$social_id)->first();

        if($userInfo)
        {
            $imagePath='';
            if($userInfo->photo) {
                $imagePath = asset("uploads/users_photos/$userInfo->photo");
            }
            $arr['token'] =$userInfo->token;
            $arr['id'] = $userInfo->id;
            $arr['name'] = $userInfo->name;
            $arr['email'] = $userInfo->email;
            $arr['phone'] = $userInfo->phone;
            $arr['longitude']=$userInfo->longitude;
            $arr['latitude']=$userInfo->latitude;
            $arr['device_token']=$userInfo->device_token;

            $arr['photo'] = $imagePath;
            
            $response = [
                'status' => true,
                'is_new' => false,
                'user' => $arr,
            ];


            return response()->json($response);
        }
        else{


            $response=[
                'status'=>false,
                'is_new' => true,
                'user' =>null,
            ];

            return response()->json($response);
        }


    }

    public function reset_password(Request $request)
    {
        $validate=Validator::make($request->all(),[
            'Email' => 'required|email',
        ]);
        if($validate->fails()){
            foreach ((array)$validate->errors() as $value){
                if (isset($value['Email'])) {
                    $msg='Email is required' ;
                    return response()->json([
                        'status'=>false,
                        'value'=>'2',
                        'msg'=>$msg
                    ]);
                }else{
                    $msg='failed';
                    return response()->json([
                        'status'=>'fail',
                        'value'=>'0',
                        'msg'=>$msg
                    ]);
                }
            }
        }

        $user=User::where('email',$request->Email)->first();
        if($user)
        {
            $code=rand(4567,3456);
            $name=$user->name;
            $email=$user->email;


            Mail::send('emails.reset_password', [ 'code'=>$code ,'name'=>$user->name ,'email'=>$user->email
            ], function ($m) use ($code ,$name ,$email ) {

                $m->from('info@tjjed.com', 'tjjed');
                $m->to($email, $name)->subject('Reset password!');

            });


            return response()->json([
                'status'=>true,
                'value'=>'1',
                'code'=>$code,
                'user'=> $user
            ]);
        }
        else{
            return response()->json([
                'status'=>false,
                'value'=>'0',
                'code'=>'',
                'user'=> null
            ]);
        }
    }

    public function change_password(Request $request)
    {
        $validate=Validator::make($request->all(),[
            'api_token' => 'required',
            'password'=>'required'
        ]);
        if($validate->fails()){
            foreach ((array)$validate->errors() as $value){
                if (isset($value['password'])) {
                    $msg='password required' ;
                    return response()->json([
                        'status'=>false,
                        'value'=>'1',
                        'msg'=>$msg
                    ]);
                }elseif (isset($value['api_token'])) {
                    $msg='api_token required' ;
                    return response()->json([
                        'status'=>false,
                        'value'=>'2',
                        'msg'=>$msg
                    ]);
                }else{
                    $msg='failed';
                    return response()->json([
                        'status'=>false,
                        'value'=>'0',
                        'msg'=>$msg
                    ]);
                }
            }
        }
        $api_token=$request->api_token;
        $password=$request->password;
        $user=User::where('token',$api_token)->first();
        if ($user)
        {
            $user->password=bcrypt($password);
            $user->device_token=$request->device_token;
            $user->update();
            $msg='password has been changed';
            return response()->json([
                'status'=>true,
                'value'=>'3',
                'msg'=>$msg
            ]);

        }else{

            $msg='user not found';

            return response()->json([
                'status'=>false,
                'value'=>'0',
                'msg'=>$msg
            ]);
        }
    }

    public function updateDevicToken(Request $request)
    {
        $api_token=$request['api_token'];
        $device_token=$request['device_token'];

        $user=User::where('token',$api_token)->first();

        if($user){

            $user->device_token=$device_token;
            $user->update();

            $msg='device token updated';
            return response()->json([
                'status'=>true,
                'value'=>'1',
                'msg'=>$msg,
            ]);

        }else{
            $msg='not allow';
            return response()->json([
                'status'=>false,
                'value'=>'0',
                'msg'=>$msg,


            ]);
        }
    }

    public  function changeLang(Request $request)
    {
        $api_token=$request['api_token'];
        $lang=$request['lang'];

        $user=User::where('token',$api_token)->first();

        if($user){

            $user->lang=$lang;
            $user->update();

            $msg='lang updated';
            return response()->json([
                'status'=>true,
                'value'=>'1',
                'msg'=>$msg,
            ]);

        }else{
            $msg='not allow';
            return response()->json([
                'status'=>false,
                'value'=>'0',
                'msg'=>$msg,


            ]);
        }
    }

    public function updateProfile(Request $request)
    {

        //   return $request;
        // validate the request
        $user_token=$request['api_token'];

        $user=User::where('token',$user_token)->first();

        if($user){
            $rules = array(
                'Email' => "email|unique:users,email,$user->id",
                'Name' => 'required|min:3|max:50|string',
                'Phone' => 'required|min:3|max:50|string',


            );

            $validation = Validator::make($request->all(), $rules);

            if ($validation->passes()) {

                $user = User::find($user->id);

                $photo = $request['Photo'];
                $imageName = $user->photo;
                // convert base64 to an image
                if ($photo != null) {

                    $imageName = md5(date("u") . rand(1, 1000)) . '.jpg';
                    $path ='../public/uploads/users_photos/';

                    $image = base64_decode($photo); // decode the image
                    file_put_contents($path . $imageName, $image);

                }

                $user->photo=$imageName;

                if ($request->has('Name') && $request->Name != '') {
                    $user->name = $request->Name;
                }
                if ($request->has('Email') && $request->Email != '') {
                    $user->email = $request->Email;
                }

                if ($request->has('Phone') && $request->Phone != '') {


                    $user->phone = $request->Phone;
                }


                if ($request->has('device_token') && $request->device_token != '') {

                    $user->device_token = $request->device_token;
                }

                if ($request->has('latitude') && $request->latitude != '') {

                    $user->latitude = $request->latitude;
                }

                if ($request->has('longitude') && $request->longitude != '') {

                    $user->longitude = $request->longitude;
                }


                if ($user->update()) {
                    $imagePath='';
                    if($user->photo!=null) {

                        $imagePath = asset("uploads/users_photos/$user->photo");
                    }

                    $arr['id']=$user->id;
                    $arr['token']=$user->token;
                    $arr['name']=$user->name;
                    $arr['phone']=$user->phone;
                    $arr['email']=$user->email;
                    $arr['photo']=$imagePath;
                    $arr['device_token']=$user->device_token;
                    $arr['longitude']=$user->longitude;
                    $arr['latitude']=$user->latitude;


                    $msg="updated successfully" ;
                    return response()->json([

                        'status' =>true,
                        'value'=>1,
                        'msg'=>$msg,
                        'user'=>$arr
                    ] );


                }

            } else {


                $errors =$validation->errors();

                //  return   $errors;
                foreach($errors as $error){

                    if(isset($error['Name'])){
                        $msg='invalid name' ;
                        return response()->json([
                            'status'=>false,
                            'value'=>'2',
                            'msg'=>$msg,
                            'user'=>null
                        ]);
                    }elseif(isset($error['Email'])){
                        $msg='invalid email' ;
                        return response()->json([
                            'status'=>false,
                            'value'=>'3',
                            'msg'=>$msg,
                            'user'=>null
                        ]);
                    }else{
                        $msg='not updated';
                        return response()->json([
                            'status'=>false,
                            'value'=>'4',
                            'msg'=>$msg,
                            'user'=>null
                        ]);
                    }


                }
            }



        }else{

            $msg='not allow';
            return response()->json([
                'status'=>false,
                'value'=>'0',
                'msg'=>$msg,
                'user'=> null

            ]);

        }
    }

}




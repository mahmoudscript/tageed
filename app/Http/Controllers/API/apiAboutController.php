<?php

namespace App\Http\Controllers\API;

use App\About_description;
use App\ContactUs;
use App\Http\Controllers\Controller;
use App\Language;
use App\Privacy_description;
use Illuminate\Http\Request;
use Validator;

class apiAboutController extends Controller
{
    //

    public function about(Request $request)
    {

        $lang=$request['lang'];

        $language=Language::where('label',$lang)->first();
        $lang_id=$language->id;

        $about =About_description::where('language_id',$lang_id)
            ->select('title','why_do_i_need_it')
            ->first();
          if($about){
        return response([
                 'status'=>true,
                  'data'=>$about
            ]);

    }else{
              return response([
                  'status'=>false,
                  'data'=>[ ]
              ]);

          }
    }



   public function contact_us(Request $request)
   {
       $rules = array(

           'Name' => 'required|min:3|max:255|string',
           'Phone' => 'required|min:3|string',
           'Message' => 'required|min:2|string',


       );

       $validation = Validator::make($request->all(), $rules);

       if ($validation->passes()) {


           $contact_us= new ContactUs();
           $contact_us->name=$request->Name;
           $contact_us->phone=$request->Phone;
           $contact_us->message=$request->Message;
           $contact_us->save();


           $msg="Added successfully" ;
           return response()->json([

               'status' =>true,
               'value'=>1,
               'msg'=>$msg,

           ] );




       } else {


           $errors =$validation->errors();
             return $errors;
           foreach($errors as $error){

               if(isset($error['Name'])){
                   $msg='invalid name' ;
                   return response()->json([
                       'status'=>false,
                       'value'=>'2',
                       'msg'=>$msg,

                   ]);
               }elseif(isset($error['Phone'])){
                   $msg='invalid Phone' ;
                   return response()->json([
                       'status'=>false,
                       'value'=>'3',
                       'msg'=>$msg,

                   ]);
               }
               elseif(isset($error['Message'])){
                   $msg='invalid Message' ;
                   return response()->json([
                       'status'=>false,
                       'value'=>'3',
                       'msg'=>$msg,

                   ]);
               }

               else{
                   $msg='not added';
                   return response()->json([
                       'status'=>false,
                       'value'=>'4',
                       'msg'=>$msg,

                   ]);
               }


           }
       }



   }












}




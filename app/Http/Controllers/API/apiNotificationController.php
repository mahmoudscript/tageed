<?php

namespace App\Http\Controllers\API;

use App\Favourite;
use App\Http\Controllers\Controller;
use App\Language;
use App\like;
use App\NotificationPhone;
use App\User;
use App\View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;

class apiNotificationController extends Controller
{
    //

    public function getAllNotifications(Request $request)
    {
        $api_token = $request['api_token'];
        $lang = $request['lang'];
        $user = User::where('token', $api_token)->first();
        if ($user) {

            $user_id = $user->id;
            $notifications = NotificationPhone::where(['notifier_id' => $user_id])
                ->with('notifyFrom')
                ->with('advert.images')
                ->orderBy('created_at', 'desc')
                ->get();

        //  return $notifications ;

            if (count($notifications) > 0) {

                $notificationData = [];
                foreach ($notifications as $notification) {

                    $notificationData['id'] = $notification->id;
                    $notificationData['notifier_id'] = $notification->notifier_id;
                    $notificationData['created_at'] = $notification->created_at;
                    $notificationData['message'] = $notification->message;
                    $notificationData['type'] = $notification->type;
                    $notificationData['is_seen'] = $notification->is_read;



                    $notificationData['notifyFrom']['id'] = $notification->notifyFrom->id;
                    $notificationData['notifyFrom']['name'] = $notification->notifyFrom->name;


                    if($notification->notifyFrom->photo) {
                        $notificationData['notifyFrom']['photo'] = URL::to('uploads/users_photos/' . $notification->notifyFrom->photo);
                    }else{
                        $notificationData['notifyFrom']['photo']= "";
                    }
                    if ($notification->advert) {
                        $notificationData['advert']['id'] = $notification->advert->id;
                        $notificationData['advert']['title'] = $notification->advert->title;
                        $notificationData['advert']['description'] = $notification->advert->description;
                        $notificationData['advert']['price'] = $notification->advert->price;
                        $notificationData['advert']['longitude'] = $notification->advert->longitude;
                        $notificationData['advert']['latitude'] = $notification->advert->latitude;
                        $notificationData['advert']['created_at'] = Carbon::parse($notification->advert->created_at)->timestamp;

                        $notificationData['advert']['views']=View::where('product_id',$notification->advert->id)->count();

                            $user_id=$notification->advert->user_id;
                            $favourite = Favourite::where(['product_id' => $notification->advert->id, 'user_id' => $user_id])
                                ->first();
                            if($favourite){
                                $notificationData['advert']['is_favourite']=true;
                            }else{
                                $notificationData['advert']['is_favourite']=false;
                            }

                            $like=like::where(['product_id' => $notification->advert->id, 'user_id' => $user_id])
                                ->first();

                            if($like){
                                $notificationData['advert']['is_like']=true;
                            }else{

                                $notificationData['advert']['is_like']=false;
                            }




                        $likecount=like::where(['product_id' => $notification->advert->id])->get()->count();


                        $notificationData['advert']['likes_count']= $likecount;


                        $userofadvert=User::find($user_id);

                        // return $userofadvert;

                        $notificationData['advert']['user']['id']=$userofadvert->id;
                        $notificationData['advert']['user']['name']=$userofadvert->name;
                        $notificationData['advert']['user']['phone']=$userofadvert->phone;
                        $notificationData['advert']['user']['photo']=URL::to('uploads/users_photos/' . $userofadvert->photo);
                        $notificationData['advert']['user']['longitude']=$userofadvert->longitude;
                        $notificationData['advert']['user']['latitude']=$userofadvert->latitude;






                        $notificationData['advert']['images'] = [];

                        if ($notification->advert->images != null) {
                            foreach ($notification->advert->images as $image) {

                                $img['id']=$image->id;
                                $img['name'] = URL::to('uploads/product_images/' . $image->name);
                                $notificationData['advert']['images'][] = $img;
                            }
                        }
                            
                        }else{
//                           
                        $notificationData['advert'] =null;

                        }

                        $data[]= $notificationData;
                    }


                   // return $data;
                    // Get the current page or default to 1, this is what you miss!
                    $page = $request->get('page', 1);
                    // default number per page
                    $perPage = 10;
                    // offset results
                    $offset = ($page * $perPage) - $perPage;


                    $count = count($data);


                    $arr = new LengthAwarePaginator($data, $count, $perPage, $offset);


//                    // make notification  read
//
//                    foreach ($notifications as $notification) {
//
//                        $notification->markAsRead($notification);
//                    }

                    return $arr;


                }else {

                return response([
                    'status' => false,
                    'current_page' => 1,
                    'total_pages_count' => 1,
                    'data' => []
                ]);


            }


        } else{

    $msg = 'not allow';
    return response()->json([
        'status' => false,
        'value' => '0',
        'msg' => $msg,


    ]);


}

}


    public function deleteNotification(Request $request)
    {
        $id=$request['notification_id'];

        $notifications = NotificationPhone::where('id',$id)->first();



        if($notifications){

            $notifications->delete();
            return response()->json([
                "status" =>true,

            ]);
        }else{
            return response()->json([
                "status" =>false,

            ]);
        }
    }


    public function seeNotification(Request $request)
    {
        $id=$request['notification_id'];

        $notifications = NotificationPhone::where('id',$id)->first();



        if($notifications){

           if($notifications->is_read == 0 ) {
               $notifications->update(['is_read'=>'1']);
               return response()->json([
                   "status" =>true,

               ]);  
           }else{

               return response()->json([
                   "status" =>false,

               ]);
           }




        }else{
            return response()->json([
                "status" =>false,

            ]);
        }
    }



    public function getUnReadNotificationsCount(Request $request)
    {
        $user_token=$request['api_token'];

        $user=User::where('token', $user_token)
            ->first();

        if($user){

            $id = $user->id;

            $notifications = NotificationPhone::where('notifier_id',$id)
                ->where('is_read',0)
                ->get()
                ->count();
            

            $response = [
                'notificationsCount' =>$notifications ,

            ];

            return response()->json($response);

        }else{

            $response = [
                'msg' => ' not allowed'
            ];

            return response()->json($response);

        }


    }



}

<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Conversation;
use App\Http\Controllers\Controller;
use App\Message;
use App\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;


class apiChatController extends Controller
{
    /**
     * this function to get all conversation with  last message to
     * be shown in the view ..........
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|LengthAwarePaginator
     */

    public function getConversations(Request $request)
    {
        $user_token = $request['api_token'];

        $user = User::where('token', $user_token)->first();

        if ($user) {

            $conversations = Conversation::where('receiver_id', $user->id)
                ->orWhere('sender_id', $user->id)
                ->select('id')
                ->with('lastMessage')
                ->orderBy('created_at', 'desc')
                ->get();
            

           //  return $conversations;

            if (count($conversations) > 0) {


                $dataconversations=[];
                foreach($conversations as $conversation){

                    $dataconversations['id']=$conversation->id;

                    $dataconversations['last_message']['id']=$conversation->lastMessage->id;
                    $dataconversations['last_message']['type']=$conversation->lastMessage->type;

                    if($conversation->lastMessage->type=='msg'){
                        $dataconversations['last_message']['message']=$conversation->lastMessage->message;
                    }elseif($conversation->lastMessage->type=='img'){
                        $dataconversations['last_message']['message']=asset("uploads/messages_photos/".$conversation->lastMessage->message);
                    }

                    $dataconversations['last_message']['sender_id']=$conversation->lastMessage->sender_id;
                    $dataconversations['last_message']['receiver_id']=$conversation->lastMessage->receiver_id;
                    $dataconversations['last_message']['created_at']=$conversation->lastMessage->created_at;
                    $dataconversations['last_message']['is_read']=$conversation->lastMessage->is_read;

                    //data for sender .....

                    $dataconversations['last_message']['sender']['id']=$conversation->lastMessage->sender->id;
                    $dataconversations['last_message']['sender']['name']=$conversation->lastMessage->sender->name;
                    $dataconversations['last_message']['sender']['photo']=asset("uploads/users_photos/".$conversation->lastMessage->sender->photo);


                    //DATA OF receiver USER

                    $dataconversations['last_message']['receiver']['id']=$conversation->lastMessage->receiver->id;
                    $dataconversations['last_message']['receiver']['name']=$conversation->lastMessage->receiver->name;
                    $dataconversations['last_message']['receiver']['photo']=asset("uploads/users_photos/".$conversation->lastMessage->receiver->photo);



                    $data[]=$dataconversations;


                }





                $page = $request->get('page', 1);
                // default number per page
                $perPage = 10;
                // offset results
                $offset = ($page * $perPage) - $perPage;


                $count = count($conversations);


                return new LengthAwarePaginator($data, $count, $perPage, $offset);


            } else {

                return response()->json([
                    "status" => false,
                    "data" => []
                ]);


            }
        } else {

            $msg = 'not allow';
            return response()->json([
                'status' => false,
                'value' => '0',
                'msg' => $msg,
                'data' => []

            ]);

        }
    }


    /**
     * this function get all messages between to users.....
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|LengthAwarePaginator
     */
    public function getConversationsMessages(Request $request)
    {
        $sender=$request['sender_id'];
        $receiver=$request['receiver_id'];


        $ids = [$sender , $receiver ] ;

        $conversation=DB::table('conversations')
            ->whereIn('sender_id' , $ids)
            ->whereIn('receiver_id' , $ids)
            ->first();

        if($conversation) {

            $conversation_id=$conversation->id;

            $messages = Message::where('conversation_id', $conversation->id)
                //  ->orWhere(['receiver_id'=>$sender,'sender_id'=>$receiver])
                ->select('id', 'message', 'type', 'sender_id', 'receiver_id', 'created_at')
                ->with('sender')
                ->with('receiver')
                ->orderBy('created_at', 'desc')
                ->get();
            
            if($messages) {
                $dataMsg=[];
                foreach($messages as $singleMsg){
                  $dataMsg['id']=$singleMsg->id;

                  // check the type of msg and return the url of image
                    //if the type is img
                  if($singleMsg->type=='msg'){
                      $dataMsg['message']=$singleMsg->message;
                  }elseif($singleMsg->type=='img'){
                      $dataMsg['message']=asset("uploads/messages_photos/".$singleMsg->message);
                  }
                    $dataMsg['sender_id']=$singleMsg->sender_id;
                  $dataMsg['receiver_id']=$singleMsg->receiver_id;
                  $dataMsg['type']=$singleMsg->type;
                  $dataMsg['created_at']=$singleMsg->created_at;

                  //DATA OF SENDER USER
                  $dataMsg['sender']['id']=$singleMsg->sender->id;
                  $dataMsg['sender']['name']=$singleMsg->sender->name;
                  $dataMsg['sender']['photo']=asset("uploads/users_photos/".$singleMsg->sender->photo);

                   //DATA OF receiver USER
                  $dataMsg['receiver']['id']=$singleMsg->receiver->id;
                  $dataMsg['receiver']['name']=$singleMsg->receiver->name;
                  $dataMsg['receiver']['photo']=asset("uploads/users_photos/".$singleMsg->receiver->photo);

                 $data[]=$dataMsg;

                 //update single message to make it read....
                    $singleMsg->update(['is_read'=>1]);
                }



                $page = $request->get('page', 1);
                // default number per page
                $perPage = 10;
                // offset results
                $offset = ($page * $perPage) - $perPage;


                $count = count($data) ;


                return new LengthAwarePaginator($data,  $count , $perPage  , $offset );
                
            }else{
                return response()->json([
                    "status" =>false,
                    "data"=>[]
                ]);

            }

        }else{

            return response()->json([
                "status" =>false,
                "data"=>[]
            ]);
        }

    }

    /**
     * this function for send single message to a spacific user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function sendMessage(Request $request)
    {
        $sender=$request['sender_id'];
        $receiver=$request['receiver_id'];
        $msg=$request['message'];
        $type=$request['type'];

        $sender_id=User::find($sender);
        $receiver_id=User::find($receiver);

        if($sender_id && $receiver_id){

//        $conversation=Conversation::where(['sender_id'=>$sender,'receiver_id'=>$receiver])
//            ->orWhere(['sender_id'=>$receiver,'receiver_id'=>$sender])
//            ->first();

            $ids = [$sender , $receiver ] ;

            $conversation=DB::table('conversations')
                ->whereIn('sender_id' , $ids)
                ->whereIn('receiver_id' , $ids)
                ->first();

        if($conversation){

            $message=new Message();
            $message->sender_id=$sender;
            $message->receiver_id=$receiver;
            $message->is_read=0;
            $message->type=$type;


            /*
             * here we check the type of msg
             * if  type---->msg
             *    we won't do any thing more rather than save data only
             * else
             *     type----->img
             *   1- we will convert base64 to an image
             *   2-save image to a spacific folder
             *   3- finally save the name of image.......
             */
            if($type=='msg'){
                $message->message=$msg;
            }elseif($type=='img'){

                //first me declare varible with empty value to assign to it the name of image
                $imageName ='';
                // convert base64 to an image
                if ($msg != null) {

                    $imageName = md5(date("u") . rand(1, 1000)) . '.jpg';
                    $path ='../public/uploads/messages_photos/';

                    $image = base64_decode($msg); // decode the image
                    file_put_contents($path . $imageName, $image);

                }

                $message->message=$imageName;
            }


            $message->conversation_id=$conversation->id;
            $message->save();




            $real_message=Message::where('id',$message->id)
                ->select('id','message','type','sender_id','receiver_id','created_at')
                ->with('sender')
                ->with('receiver')
                ->first();

            $dataMsg['id']=$real_message->id;

            // check the type of msg and return the url of image
            //if the type is img
            if($real_message->type=='msg'){
                $dataMsg['message']=$real_message->message;
            }elseif($real_message->type=='img'){
                $dataMsg['message']=asset("uploads/messages_photos/".$real_message->message);
            }
            $dataMsg['sender_id']=$real_message->sender_id;
            $dataMsg['receiver_id']=$real_message->receiver_id;
            $dataMsg['type']=$real_message->type;
            $dataMsg['created_at']=$real_message->created_at;

            //DATA OF SENDER USER
            $dataMsg['sender']['id']=$real_message->sender->id;
            $dataMsg['sender']['name']=$real_message->sender->name;
            $dataMsg['sender']['photo']=asset("uploads/users_photos/".$real_message->sender->photo);

            //DATA OF receiver USER
            $dataMsg['receiver']['id']=$real_message->receiver->id;
            $dataMsg['receiver']['name']=$real_message->receiver->name;
            $dataMsg['receiver']['photo']=asset("uploads/users_photos/".$real_message->receiver->photo);


        /*****here we will send fcm notification to user who will recive *****/

             /**
              * 1- we get the user who we send notification to him
              * 2- selsct the devicetoken which we send notification throw it..
              * 3- then we write some code from the brozto backage
              *    https://github.com/brozot/Laravel-FCM
              *   but frist we should get server_key & sender_id from andriod man -_-
              * 4- we have to thing to send notification
              *     - data Payload
              *     - notification Payload
              *   in **-andriod-**---> we send the dataPayload
              *    but we didn't send  notification payload or send it null
              *  but i didn't know if is that right i read in documentation
              * that the notification payload which appear in device
              * but data msg is which client handel .......
              *
              *   in **--ios--**----> the opsite
              *
              **/


    $device_token=User::find($receiver)->device_token;
         if($device_token) {
             //here we sort the data which will send through fcm.......
             $datadata=json_encode( $dataMsg);
             $data = [
                 'title' => 'tjjed',
                 'message' => 'new message',
                 'type' => 'message',
                 'data' => $datadata
             ];

             // Sending a Downstream Message to a Device
             $optionBuilder = new OptionsBuilder();
             $optionBuilder->setTimeToLive(60 * 20);

             $notificationBuilder = new PayloadNotificationBuilder('tjjed');
             $notificationBuilder->setBody($data)
                 ->setSound('default');

             $dataBuilder = new PayloadDataBuilder();
             $dataBuilder->addData($data);

             $option = $optionBuilder->build();
             $notification =null;
             $data = $dataBuilder->build();

             $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);


             //this if you want to check the notification sent to device or not ....
             //  $downstreamResponse->numberSuccess();
             //   $downstreamResponse->numberFailure();
             //   $downstreamResponse->numberModification();

         }



            return response()->json([
                "status" =>true,
                "messages"=>$dataMsg
            ]);


        }else{

            //create new conversation & and then create message
            $new_conversation = new Conversation();
            $new_conversation->sender_id=$sender;
            $new_conversation->receiver_id=$receiver;

            if($new_conversation->save()){


                $message=new Message();
                $message->sender_id=$sender;
                $message->receiver_id=$receiver;
                $message->is_read=0;
                if($type=='msg'){
                    $message->message=$msg;
                }elseif($type=='img'){

                    //first me declare varible with empty value to assign to it the name of image
                    $imageName ='';
                    // convert base64 to an image
                    if ($msg != null) {

                        $imageName = md5(date("u") . rand(1, 1000)) . '.jpg';
                        $path ='../public/uploads/messages_photos/';

                        $image = base64_decode($msg); // decode the image
                        file_put_contents($path . $imageName, $image);

                    }

                    $message->message=$imageName;
                }
                $message->conversation_id=$new_conversation->id;



                if($message->save()){


                    $real_message=Message::where('id',$message->id)
                        ->select('id','message','sender_id','receiver_id','created_at')
                        ->with('sender')
                        ->with('receiver')
                        ->first();

                    $dataMsg['id']=$real_message->id;

                    // check the type of msg and return the url of image
                    //if the type is img
                    if($real_message->type=='msg'){
                        $dataMsg['message']=$real_message->message;
                    }elseif($real_message->type=='img'){
                        $dataMsg['message']=asset("uploads/messages_photos/".$real_message->message);
                    }
                    $dataMsg['sender_id']=$real_message->sender_id;
                    $dataMsg['receiver_id']=$real_message->receiver_id;
                    $dataMsg['type']=$real_message->type;
                    $dataMsg['created_at']=$real_message->created_at;

                    //DATA OF SENDER USER
                    $dataMsg['sender']['id']=$real_message->sender->id;
                    $dataMsg['sender']['name']=$real_message->sender->name;
                    $dataMsg['sender']['photo']=asset("uploads/users_photos/".$real_message->sender->photo);

                    //DATA OF receiver USER
                    $dataMsg['receiver']['id']=$real_message->receiver->id;
                    $dataMsg['receiver']['name']=$real_message->receiver->name;
                    $dataMsg['receiver']['photo']=asset("uploads/users_photos/".$real_message->receiver->photo);



                    /*****here we will send fcm notification to user who will recive *****/

                    /**
                     * 1- we get the user who we send notification to him
                     * 2- selsct the devicetoken which we send notification throw it..
                     * 3- then we write some code from the brozto backage
                     *    https://github.com/brozot/Laravel-FCM
                     *   but frist we should get server_key & sender_id from andriod man -_-
                     * 4- we have to thing to send notification
                     *     - data Payload
                     *     - notification Payload
                     *   in **-andriod-**---> we send the dataPayload
                     *    but we didn't send  notification payload or send it null
                     *
                     * but i didn't know if is that right i read in documentation
                     * that the notification payload which appear in device
                     * but data msg is which client handel .......
                     *
                     *   in **--ios--**----> the opsite
                     *
                     **/


                    $device_token=User::find($receiver)->device_token;
                    if($device_token) {

                        //here we sort the data which will send through fcm.......
                        $data = [
                            'title' => 'tjjed',
                            'message' => 'new message',
                            'type' => 'message',
                            'data' => $dataMsg
                        ];

                        // Sending a Downstream Message to a Device

                        $optionBuilder = new OptionsBuilder();
                        $optionBuilder->setTimeToLive(60 * 20);

                        $notificationBuilder = new PayloadNotificationBuilder('tjjed');
                        $notificationBuilder->setBody($data)
                            ->setSound('default');

                        $dataBuilder = new PayloadDataBuilder();
                        $dataBuilder->addData(['data' => $data]);

                        $option = $optionBuilder->build();
                        $notification = $notificationBuilder->build();
                        $data = $dataBuilder->build();

                        $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);


                        //this if you want to check the notification sent to device or not ....
                       // $downstreamResponse->numberSuccess();
                        //   $downstreamResponse->numberFailure();
                        //   $downstreamResponse->numberModification();

                    }


                    return response()->json([
                        "status" =>true,
                        "messages"=>$dataMsg
                    ]);

                }else{

                    return response()->json([
                        "status" =>false,
                        "messages"=>'message wasnot saved aslan'
                    ]);

                }

            }else{

                return response()->json([
                    "status" =>false,
                    "messages"=>'conversation wasnt saved from the begining'
                ]);

            }


        }

        }else{
            return response()->json([
                "status" =>false,
                "messages"=>'not allowed'
            ]); 

        }
    }

    /**
     * this function for delete single conversation.........
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */

    public function deleteConversation(Request $request){
        $con_id=$request['conversation_id'];

        $conversation=Conversation::where('id',$con_id)->first();

        if($conversation){

            if($conversation->delete()){

                return response([
                    'msg'=>true
                ]);

            }else{
                return response([
                    'msg'=>false
                ]);

            }

        }else{
            return response([
                'msg'=>false
            ]);
        }


    }


}

<?php

namespace App\Http\Controllers\API;

use App\Article;
use App\Favourite;
use App\like;
use App\NotificationPhone;
use App\Product;
use App\View;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Comment;
use App\User;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\URL;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class apiCommentController extends Controller
{
    //


    public function addView(Request $request)
    {


        $advert_id=$request['advert_id'];
        $token=$request['api_token'];

        $user=User::where('token',$token)->first();

        //return $user->id;
        if($user){
            $viewed = View::where(['product_id' => $advert_id, 'user_id' => $user->id])
                ->first();

            if (!$viewed) {

                $view = new View();
                $view->product_id = $advert_id;
                $view->user_id = $user->id;

                if ($view->save()) {

                    return response()->json([
                        'status' => true
                    ]);

                } else {
                    return response()->json([
                        'status' => false,
                    ]);
                }


            }else{

                return response()->json([
                    'status' => false,
                ]);

            }


        }
        else{

            $msg='not allow';
            return response()->json([
                'status'=>false,
                'msg'=>$msg,
            ]);
        }
    }

    public function addFavourite(Request $request)
    {

        $advert_id=$request['advert_id'];
        $token=$request['api_token'];

        $user=User::where('token',$token)->first();

        //return $user->id;
        if($user){
            $favourites = Favourite::where(['product_id' => $advert_id, 'user_id' => $user->id])
                ->first();

            if(!$favourites){

                $favourite = new Favourite();
                $favourite->product_id = $advert_id;
                $favourite->user_id = $user->id;

                if ($favourite->save()) {

                    return response()->json([
                        'status' => true
                    ]);

                } else {
                    return response()->json([
                        'status' => false,
                    ]);
                }


            }else{

                $favourites->delete();
                return response()->json([
                    'status' => false,
                ]);

            }


        }
        else{

            $msg='not allow';
            return response()->json([
                'status'=>false,
                'msg'=>$msg,
            ]);
        }


    }

    public function getFavourite(Request $request)
    {

        $token=$request['api_token'];

        $user=User::where('token',$token)->first();


        if($user){

            $adverts = Favourite::where([ 'user_id' => $user->id])
                ->with('product.user')
                ->orderBy('created_at','desc')
                ->with('product.images')
                ->get();

            // return $adverts;


            if($adverts){
                $data=[];
                foreach($adverts as $adv ){


                    $arr['id']=$adv->product->id;
                    $arr['title']=$adv->product->title;
                    $arr['description']=$adv->product->description;
                    $arr['price']=$adv->product->price;
                    $arr['longitude']=$adv->product->longitude;
                    $arr['latitude']=$adv->product->latitude;
                    $arr['created_at']=Carbon::parse($adv->product->created_at)->timestamp;
                    $arr['views']=View::where('product_id',$adv->product->id)->count();
                    $arr['is_favourite']= true;

                    $likecount=like::where(['product_id' => $adv->product->id])->get()->count();
                    $arr['likes_count']= $likecount;

                    $like=like::where(['product_id' => $adv->product->id, 'user_id' => $user->id])
                        ->first();

                    if($like){
                        $arr['is_like']=true;
                    }else{

                        $arr['is_like']=false;
                    }

                    $arr['images']=[];
                    foreach($adv->product->images as $images ){

                        $img['id'] = $images->id ;
                        $img['name']=URL::to('uploads/product_images/' . $images->name);

                        $arr['images'][]= $img;

                    }

                    $arr['user']['id']=$adv->product->user->id;
                    $arr['user']['name']=$adv->product->user->name;
                    $arr['user']['photo']=URL::to('uploads/users_photos/' . $adv->product->user->photo);
                    $arr['user']['longitude']=$adv->product->user->longitude;
                    $arr['user']['latitude']=$adv->product->user->latitude;
                    $arr['user']['phone']=$adv->product->user->phone;






                    $data[]= $arr;
                }

                //  return $data;


                $page = $request->get('page', 1);
                // default number per page
                $perPage = 10;
                // offset results
                $offset = ($page * $perPage) - $perPage;


                $count = count($data) ;


                return new LengthAwarePaginator($data,  $count , $perPage  , $offset );

            }else{
                return response([
                    'status' =>true ,
                    'current_page' =>1,
                    'total_pages_count'=>1,
                    'data' => [ ]
                ]);

            }


        } else{
            $msg='not allow';
            return response()->json([
                'status'=>false,
                'msg'=>$msg,
            ]);
        }


    }

    public function addLike(Request $request){

        $advert_id=$request['advert_id'];
        $token=$request['api_token'];


        //  return 'hi';

        $user=User::where('token',$token)->first();
        $advert_owner=Product::find($advert_id);
        //return $user->id;
        if($user && $advert_owner){
            $like= like::where(['product_id' => $advert_id, 'user_id' => $user->id])
                ->first();

            if(!$like){

                $like = new like();
                $like->product_id = $advert_id;
                $like->user_id = $user->id;

                if ($like->save()) {

                    //create notification for this comment

                    $advertUser=User::find($advert_owner->user_id);
                    $device_token = $advertUser->device_token;
                    if($user->id != $advert_owner->user_id ) {

                        $notification = new NotificationPhone();
                        $notification->notifier_id = $advert_owner->user_id;
                        $notification->notify_from = $user->id;
                        $notification->product_id = $advert_id;
                        $notification->is_read = 0;
                        $notification->type = 'like';
                        $notification->message = $user->name . 'like your advert';
                        $notification->save();


                        $notifications = NotificationPhone::where(['id' => $notification->id])
                            ->with('notifyFrom')
                            ->with('advert.images')
                            ->first();


                        // return $notifications ;

                        //here the data should send with notification fcm.....

                        $notificationData['id'] = $notification->id;
                        $notificationData['notifier_id'] = $notification->notifier_id;
                        $notificationData['message'] = null;

                        $notificationData['notifyFrom']['id'] = $notification->notifyFrom->id;
                        $notificationData['notifyFrom']['name'] = $notification->notifyFrom->name;


                        if ($notification->notifyFrom->photo) {
                            $notificationData['notifyFrom']['photo'] = URL::to('uploads/users_photos/' . $notification->notifyFrom->photo);
                        } else {
                            $notificationData['notifyFrom']['photo'] = "";
                        }

                        $notificationData['product']['id'] = $notification->advert->id;
                        $notificationData['product']['title'] = $notification->advert->title;
                        $notificationData['product']['description'] = $notification->advert->description;

                        $notificationData['product']['price'] = $notification->advert->price;
                        $notificationData['product']['longitude'] = $notification->advert->longitude;
                        $notificationData['product']['latitude'] = $notification->advert->latitude;
                        $notificationData['product']['created_at'] = Carbon::parse($notification->advert->created_at)->timestamp;
                        $notificationData['product']['views'] = View::where('product_id', $advert_id)->count();


                        $user_id = $advert_owner->user_id;
                        $favourite = Favourite::where(['product_id' => $advert_id, 'user_id' => $user_id])
                            ->first();
                        if ($favourite) {
                            $notificationData['product']['is_favourite'] = true;
                        } else {
                            $notificationData['product']['is_favourite'] = false;
                        }

                        $like = like::where(['product_id' => $advert_id, 'user_id' => $user_id])
                            ->first();

                        if ($like) {
                            $notificationData['product']['is_like'] = true;
                        } else {

                            $notificationData['product']['is_like'] = false;
                        }


                        $userofadvert = User::find($user_id);

                        // return $userofadvert;

                        $notificationData['product']['user']['id'] = $userofadvert->id;
                        $notificationData['product']['user']['name'] = $userofadvert->name;
                        $notificationData['product']['user']['phone'] = $userofadvert->phone;
                        $notificationData['product']['user']['photo'] = URL::to('uploads/users_photos/' . $userofadvert->photo);
                        $notificationData['product']['user']['longitude'] = $userofadvert->longitude;
                        $notificationData['product']['user']['latitude'] = $userofadvert->latitude;


                        $likecount = like::where(['product_id' => $advert_id])->get()->count();


                        $notificationData['product']['likes_count'] = $likecount;


                        $notificationData['product']['images'] = [];
                        foreach ($notification->advert->images as $images) {
                            $img['id'] =$images->id;
                            $img['name'] = URL::to('uploads/product_images/' . $images->name);

                            $notificationData['product']['images'][] = $img;

                        }


                        // $notificationD = json_encode($notificationData);


                        // here we write the code to send notification through firebase.....

                        if ($device_token && ($user->id != $advert_owner->user_id)) {

                            $notificationD = json_encode($notificationData);
                            //here we sort the data which will send through fcm.......
                            $data = [
                                'title' => 'tjjed',
                                'message' => $notification->notifyFrom->name . 'like your advert',
                                'type' => 'like',
                                'data' => $notificationD
                            ];


                            //  return $data;

                            // Sending a Downstream Message to a Device

                            $optionBuilder = new OptionsBuilder();
                            $optionBuilder->setTimeToLive(60 * 20);

                            $notificationBuilder = new PayloadNotificationBuilder('tjjed');
                            $notificationBuilder->setBody($data)
                                ->setSound('default');

                            $dataBuilder = new PayloadDataBuilder();
                            $dataBuilder->addData($data);

                            $option = $optionBuilder->build();
                            $notification = null ;
                            $data = $dataBuilder->build();

                            $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);


                            //this if you want to check the notification sent to device or not ....
                            // $downstreamResponse->numberSuccess();
                            // $downstreamResponse->numberFailure();
                            //   $downstreamResponse->numberModification();

                        }

                    }






                    return response()->json([
                        'status' => true
                    ]);

                } else {
                    return response()->json([
                        'status' => false,
                    ]);
                }


            }else{

                $like->delete();
                return response()->json([
                    'status' => false,
                ]);

            }


        }
        else{

            $msg='not allow';
            return response()->json([
                'status'=>false,
                'msg'=>$msg,
            ]);
        }

    }

    public function addComment(Request $request){

        $advert_id=$request['advert_id'];
        $token=$request['api_token'];
        $comment=$request['comment'];

        $user=User::where('token',$token)->first();
        $advert_owner=Product::find($advert_id);
        //return $user->id;
        if($user && $advert_owner ) {

            $comment = Comment::create([
                'product_id' => $advert_id,
                'user_id' => $user->id,
                'comment' => $comment,
            ]);

            //create notification for this comment


            $advertUser = User::find($advert_owner->user_id);
            $device_token = $advertUser->device_token;

            if($user->id != $advert_owner->user_id) {
                $notification = new NotificationPhone();
                $notification->notifier_id = $advert_owner->user_id;
                $notification->notify_from = $user->id;
                $notification->product_id = $advert_id;
                $notification->is_read = 0;
                $notification->type = 'comment';
                $notification->message = $user->name . 'comment on your advert';
                $notification->save();


                $notifications = NotificationPhone::where(['id' => $notification->id])
                    ->with('notifyFrom')
                    ->with('advert.images')
                    ->first();

                //here the data should send with notification fcm.....

                $notificationData['id'] = $notification->id;
                $notificationData['notifier_id'] = $notification->notifier_id;
                $notificationData['message'] = null;

                $notificationData['notifyFrom']['id'] = $notification->notifyFrom->id;
                $notificationData['notifyFrom']['name'] = $notification->notifyFrom->name;


                if ($notification->notifyFrom->photo) {
                    $notificationData['notifyFrom']['photo'] = URL::to('uploads/users_photos/' . $notification->notifyFrom->photo);
                } else {
                    $notificationData['notifyFrom']['photo'] = "";
                }

                $notificationData['product']['id'] = $notification->advert->id;
                $notificationData['product']['title'] = $notification->advert->title;
                $notificationData['product']['description'] = $notification->advert->description;

                $notificationData['product']['price'] = $notification->advert->price;
                $notificationData['product']['longitude'] = $notification->advert->longitude;
                $notificationData['product']['latitude'] = $notification->advert->latitude;
                $notificationData['product']['created_at'] = Carbon::parse($notification->advert->created_at)->timestamp;
                $notificationData['product']['views'] = View::where('product_id', $advert_id)->count();


                $user_id = $advert_owner->user_id;
                $favourite = Favourite::where(['product_id' => $advert_id, 'user_id' => $user_id])
                    ->first();
                if ($favourite) {
                    $notificationData['product']['is_favourite'] = true;
                } else {
                    $notificationData['product']['is_favourite'] = false;
                }

                $like = like::where(['product_id' => $advert_id, 'user_id' => $user_id])
                    ->first();

                if ($like) {
                    $notificationData['product']['is_like'] = true;
                } else {

                    $notificationData['product']['is_like'] = false;
                }


                $likecount = like::where(['product_id' => $advert_id])->get()->count();


                $notificationData['product']['likes_count'] = $likecount;


                $userofadvert = User::find($user_id);

                // return $userofadvert;

                $notificationData['product']['user']['id'] = $userofadvert->id;
                $notificationData['product']['user']['name'] = $userofadvert->name;
                $notificationData['product']['user']['phone'] = $userofadvert->phone;
                $notificationData['product']['user']['photo'] = URL::to('uploads/users_photos/' . $userofadvert->photo);
                $notificationData['product']['user']['longitude'] = $userofadvert->longitude;
                $notificationData['product']['user']['latitude'] = $userofadvert->latitude;


                $notificationData['product']['images'] = [];
                foreach ($notification->advert->images as $images) {

                    $img['id'] =$images->id;
                    $img['name'] = URL::to('uploads/product_images/' . $images->name);

                    $notificationData['product']['images'][] = $img;

                }


                $notificationD =json_encode($notificationData);

              //  return  $notificationD;


                // here we write the code to send notification through firebase.....

                if ($device_token && ($user->id != $advert_owner->user_id)) {

                    //here we sort the data which will send through fcm.......
                    $notificationD =json_encode($notificationData);

                    // return  $notificationD;
                    $data = [
                        'title' => 'tjjed',
                        'message' => $notification->notifyFrom->name .'comment on your advert',
                        'type' => 'comment',
                        'data' => $notificationD
                    ];

                    // Sending a Downstream Message to a Device

                    $optionBuilder = new OptionsBuilder();
                    $optionBuilder->setTimeToLive(60 * 20);

                    $notificationBuilder = new PayloadNotificationBuilder('tjjed');
                    $notificationBuilder->setBody($data)
                        ->setSound('default');

                    $dataBuilder = new PayloadDataBuilder();
                    $dataBuilder->addData( $data);

                    $option = $optionBuilder->build();
                    $notification = null;
                    $data = $dataBuilder->build();

                    $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);


                    //this if you want to check the notification sent to device or not ....
                    // $downstreamResponse->numberSuccess();
                    // $downstreamResponse->numberFailure();
                    //   $downstreamResponse->numberModification();

                }



            }

            if ($comment) {

                return response()->json([
                    'status' => true
                ]);

            } else {
                return response()->json([
                    'status' => false,
                ]);
            }


        } else{

            $msg='not allow';
            return response()->json([
                'status'=>false,
                'msg'=>$msg,
            ]);
        }

    }

    public function getComments(Request $request)
    {
        // $token=$request['api_token'];
        $advert_id=$request['advert_id'];

        // $user=User::where('token',$token)->first();
        $advert=Product::find($advert_id);
        if( $advert ){
            //$user_id =$user->id;
            $comments=Comment::where(['product_id'=>$advert_id])
                ->with('user')
                ->orderBy('created_at','desc')
                ->get();
            if($comments){
                $commentsData=[];
                $data=[];
                foreach($comments as $comment){
                    $commentsData['id']=$comment->id;
                    $commentsData['comment']=$comment->comment;
                    $commentsData['created_at']=$comment->created_at;
                    $commentsData['user']['id']=$comment->user->id;
                    $commentsData['user']['name']=$comment->user->name;

                    if($comment->user->photo !=null){
                        $commentsData['user']['photo']=URL::to('uploads/users_photos/' .$comment->user->photo);

                    }else{
                        $commentsData['user']['photo']='';

                    }

                    $data[]= $commentsData;

                }

                $page = $request->get('page', 1);
                // default number per page
                $perPage = 10;
                // offset results
                $offset = ($page * $perPage) - $perPage;


                $count = count($data) ;


                return new LengthAwarePaginator($data,  $count , $perPage  , $offset );

            }else{

                return response([
                    'status' =>true ,
                    'current_page' =>1,
                    'total_pages_count'=>1,
                    'data' => [ ]
                ]);

            }



        }else{

            $msg='not allow';
            return response()->json([
                'status'=>false,
                'msg'=>$msg,
            ]);

        }


    }







}

<?php

namespace App\Http\Controllers\API;

use App\Article;
use App\article_description;
use App\Department;
use App\Http\Controllers\Controller;
use App\Language;
use App\Product;
use App\ProductImages;
use App\View;
use App\like;

use App\User;
use App\Favourite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\URL;
use Validator;
use File;
class apiAdvertsController extends Controller
{
    //


    public function getMainDepartments(Request $request)
    {

        if($request['lang']){
            $lang =$request['lang'];
            $language=Language::where('label',$lang)->first();
            $lang_id=$language->id;
        }else{
            $lang_id=1;
        }

        $departments=Department::where('parent_id',null)
            ->with('description')
            ->get();
        $data=[];
        foreach($departments as $department ){
            $arr['id']=$department->id;

            foreach($department->description as $description ){
                if($description->language_id==$lang_id){
                    $arr['name']=$description->name;

                }

            }
            $arr['photo']=URL::to('uploads/payment_photos/'.$department->photo);


            $ids=Department::where('parent_id',$department->id)->get()->pluck('id');



            $countProducts=0;
            foreach ($ids as $id){

                $productcount=Product::where('cat_id',$id)->get()->count();


                $countProducts=$countProducts+$productcount;
            }

            $arr['adsCount']= $countProducts ;

            $data[]= $arr;
        }

        //return $data;

        if($data){
            return response([
                'status' =>true ,

                'data' =>$data
            ]);
        }else{
            return response([
                'status' =>true ,

                'data' => [ ]
            ]);

        }



    }

    public function getSubDepartments(Request $request)
    {

        if($request['lang']){
            $lang =$request['lang'];
            $language=Language::where('label',$lang)->first();
            $lang_id=$language->id;
        }else{
            $lang_id=1;
        }

        $department_id=$request['department_id'] ;

        $departments=Department::where('parent_id',$department_id)
            ->with('description')
            ->get();

        if($departments){
            $data=[];
            foreach($departments as $department ){
                $arr['id']=$department->id;

                foreach($department->description as $description ){
                    if($description->language_id==$lang_id){
                        $arr['name']=$description->name;

                    }

                }

                $arr['photo']=URL::to('uploads/payment_photos/'.$department->photo);

                $id=Department::where('id',$department->id)->first()->id;


                $productcount=Product::where('cat_id',$id)->get()->count();



                $arr['adsCount']= $productcount;

                $data[]= $arr;
            }

            return response([
                'status' =>true ,
                'data' => $data
            ]);

        }else{
            return response([
                'status' =>true ,

                'data' => [ ]
            ]);

        }



    }

    public function getadverts(Request $request)
    {

        $department_id=$request['subDepartment_id'] ;

        $api_token=$request['api_token'];
        $user=User::where('token',$api_token)->first();



        $adverts=Product::where('cat_id',$department_id)
            // ->with('description')
            ->with('user')
            ->orderBy('created_at','desc')
            // ->with('viewCount')
            ->get();


        //  return $adverts;

        if($adverts){
            $data=[];
            foreach($adverts as $advert ){
                $arr['id']=$advert->id;
                $arr['title']=$advert->title;
                $arr['description']=$advert->description;
                $arr['price']=$advert->price;
                $arr['longitude']=$advert->longitude;
                $arr['latitude']=$advert->latitude;
                $arr['created_at']=Carbon::parse($advert->created_at)->timestamp;
                $arr['views']=View::where('product_id',$advert->id)->count();

                if($user)
                {
                    $user_id=$user->id;
                    $favourite = Favourite::where(['product_id' => $advert->id, 'user_id' => $user_id])
                        ->first();
                    if($favourite){
                        $arr['is_favourite']=true;
                    }else{
                        $arr['is_favourite']=false;
                    }

                   $like=like::where(['product_id' => $advert->id, 'user_id' => $user_id])
                       ->first();

                    if($like){
                        $arr['is_like']=true;
                    }else{

                        $arr['is_like']=false;
                    }


                }else{
                    $arr['is_favourite']= false;
                    $arr['is_like']= false;

                }

                $likecount=like::where(['product_id' => $advert->id])->get()->count();
                $arr['likes_count']= $likecount;


                $arr['images']=[];
                foreach($advert->images as $images ){

                    $img['id']=$images->id;
                    $img['name']=URL::to('uploads/product_images/' . $images->name);
                    $arr['images'][]= $img;

                }


                $user_id=$advert->user_id;
                $userofadvert=User::find($user_id);

                // return $userofadvert;

                $arr['user']['id']=$userofadvert->id;
                $arr['user']['name']=$userofadvert->name;
                $arr['user']['phone']=$userofadvert->phone;
                $arr['user']['photo']=URL::to('uploads/users_photos/' . $userofadvert->photo);
                $arr['user']['longitude']=$userofadvert->longitude;
                $arr['user']['latitude']=$userofadvert->latitude;





                $data[]= $arr;
            }

            //  return $data;


            $page = $request->get('page', 1);
            // default number per page
            $perPage = 10;
            // offset results
            $offset = ($page * $perPage) - $perPage;


            $count = count($data) ;


            return new LengthAwarePaginator($data,  $count , $perPage  , $offset );

        }else{
            return response([
                'status' =>true ,
                'current_page' =>1,
                'total_pages_count'=>1,
                'data' => [ ]
            ]);

        }



    }

    public function addAdvert(Request $request) {


        $user_token=$request['api_token'];

        $user=User::where('token',$user_token)->first();

        if($user){

            $rules = array(

                'title' => 'required|min:3|max:255|string',
                'description' => 'required|min:3|string',
                'longitude' => 'required',
                'latitude' => 'required',
                'Photos' => 'required',
                'subDepartment_id' => 'required',
                'price' => 'required',


            );

            $validation = Validator::make($request->all(), $rules);

            if ($validation->passes()) {

                // convert base64 to an image

                $product= new Product();
                $product->title=$request->title;
                $product->price=$request->price;
                $product->cat_id=$request->subDepartment_id;
                $product->user_id=$user->id;
                $product->longitude=$request->longitude;
                $product->latitude=$request->latitude;
                $product->description=$request->description;
                $product->save();



                $photos = json_decode($request->Photos , true);

                // $photos=$request->Photos;
                // return $photos;
                foreach ($photos as $photo) {

                    $imageName = md5(date("u") . rand(1, 1000)) . '.jpg';
                    $path ='../public/uploads/product_images/';

                    $image = base64_decode($photo); // decode the image
                    file_put_contents($path . $imageName, $image);

                    $images= new ProductImages();
                    $images->product_id=$product->id;
                    $images->name=$imageName;
                    $images->save();


                }


                $arr['id']=$product->id;
                $arr['title']=$product->title;
                $arr['price']=$product->price;
                $arr['description']=$product->description;
                $arr['longitude']=$product->longitude;
                $arr['latitude']=$product->latitude;

                $images= ProductImages::where('product_id',$product->id)
                    ->get();
                foreach($images as $image ){

                    $img['id']=$image->id;
                    $img['name']=URL::to('uploads/product_images/' . $image->name);

                    $arr['images'][]= $img;

                }





                $msg="advert add successfully" ;
                return response()->json([

                    'key' =>true,
                    'value'=>1,
                    'msg'=>$msg,
                    'advert'=>$arr
                ] );




            } else {


                $errors =$validation->errors();

                foreach($errors as $error){

                    if(isset($error['title'])){
                        $msg='invalid name' ;
                        return response()->json([
                            'key'=>false,
                            'value'=>'2',
                            'msg'=>$msg,
                            'advert'=>[ ]
                        ]);
                    }elseif(isset($error['description'])){
                        $msg='invalid email' ;
                        return response()->json([
                            'key'=>false,
                            'value'=>'3',
                            'msg'=>$msg,
                            'advert'=>[ ]
                        ]);
                    }else{
                        $msg='not added';
                        return response()->json([
                            'key'=>false,
                            'value'=>'4',
                            'msg'=>$msg,
                            'advert'=>[ ]
                        ]);
                    }


                }
            }



        }else{

            $msg='not allow';
            return response()->json([
                'key'=>false,
                'value'=>'0',
                'msg'=>$msg,
                'user'=>[ ]

            ]);

        }

    }

    public function search(Request $request)
    {

        $name=$request['name'] ;

        $user_token=$request['api_token'];

        $user=User::where('token',$user_token)->first();

        $adverts=Product::where('title','like','%'.$name.'%')
            // ->with('description')
            ->with('user')
            // ->with('viewCount')
            ->get();


        //  return $adverts;

        if($adverts){
            $data=[];
            foreach($adverts as $advert ){
                $arr['id']=$advert->id;
                $arr['title']=$advert->title;
                $arr['description']=$advert->description;
                $arr['price']=$advert->price;
                $arr['longitude']=$advert->longitude;
                $arr['latitude']=$advert->latitude;
                $arr['created_at']=Carbon::parse($advert->created_at)->timestamp;
                $arr['views']=View::where('product_id',$advert->id)->count();

                if($user)
                {
                    $user_id=$user->id;
                    $favourite = Favourite::where(['product_id' => $advert->id, 'user_id' => $user_id])
                        ->first();
                    if($favourite){
                        $arr['is_favourite']=true;
                    }else{
                        $arr['is_favourite']=false;
                    }

                    $like=like::where(['product_id' => $advert->id, 'user_id' => $user_id])
                        ->first();

                    if($like){
                        $arr['is_like']=true;
                    }else{

                        $arr['is_like']=false;
                    }


                }else{
                    $arr['is_favourite']= false;
                    $arr['is_like']= false;

                }

                $likecount=like::where(['product_id' => $advert->id])->get()->count();
                $arr['likes_count']= $likecount;


                //$arr['images']=[];
                foreach($advert->images as $images ){

                    $img['id']=$images->id;
                    $img['name']=URL::to('uploads/product_images/' . $images->name);

                    $arr['images'][]= $img;

                }

                $arr['user']['id']=$advert->user->id;
                $arr['user']['name']=$advert->user->name;
                $arr['user']['photo']=URL::to('uploads/users_photos/' . $advert->user->photo);
                $arr['user']['longitude']=$advert->user->longitude;
                $arr['user']['latitude']=$advert->user->latitude;





                $data[]= $arr;
            }

            //  return $data;


            $page = $request->get('page', 1);
            // default number per page
            $perPage = 10;
            // offset results
            $offset = ($page * $perPage) - $perPage;


            $count = count($data) ;


            return new LengthAwarePaginator($data,  $count , $perPage  , $offset );

        }else{
            return response([
                'status' =>true ,
                'current_page' =>1,
                'total_pages_count'=>1,
                'data' => [ ]
            ]);

        }



    }

    public function delete_advert(Request $request)
    {
       $advert_id=$request->advert_id;
        $user_token=$request['token'];

        $user=User::where('token',$user_token)->first();
         if($user) {
             $advert = Product::where('id', $advert_id)
                 ->where('user_id', $user->id)
                 ->first();

             if($advert){

                 $advert->delete();

                 return response([
                     'status' =>true ,
                     'msg' =>'advert deleted successfully'
                  ]);

             }else{

                 return response([
                     'status' =>false ,
                     'msg' =>'not allow'
                 ]);

             }

         }else{

             return response([
                 'status' =>false ,
                 'msg' =>'not allow'
             ]);

         }

    }

    public function edit_advert(Request $request) {


        $user_token=$request['api_token'];
        $advert_id=$request->advert_id;

        $user=User::where('token',$user_token)->first();
        $advert=Product::find($advert_id);
        if($user && $advert){


            if($request->has('title') && $request->title!=''){
                $advert->title=$request->title;
            }
            if($request->has('price') && $request->price!=''){
                $advert->price=$request->price;
            }
            if($request->has('subDepartment_id') && $request->subDepartment_id !=''){
                $advert->cat_id=$request->subDepartment_id;
            }
            if($request->has('longitude') && $request->longitude!=''){
                $advert->longitude=$request->longitude;
            }
            if($request->has('latitude') && $request->latitude!=''){
                $advert->latitude=$request->latitude;
            }
            if($request->has('title') && $request->title!=''){
                $advert->description=$request->description;
            }
            $advert->save();



                $photos = json_decode($request->Photos , true);

                // $photos=$request->Photos;
                // return $photos;
                foreach ($photos as $photo) {

                    $imageName = md5(date("u") . rand(1, 1000)) . '.jpg';
                    $path ='../public/uploads/product_images/';

                    $image = base64_decode($photo); // decode the image
                    file_put_contents($path . $imageName, $image);

                    $images= new ProductImages();
                    $images->product_id=$advert->id;
                    $images->name=$imageName;
                    $images->save();


                }


                $arr['id']=$advert->id;
                $arr['title']=$advert->title;
                $arr['price']=$advert->price;
                $arr['description']=$advert->description;
                $arr['longitude']=$advert->longitude;
                $arr['latitude']=$advert->latitude;

                $images= ProductImages::where('product_id',$advert->id)
                    ->get();
                foreach($images as $image ){

                    $img['id']=$advert->id ;
                    $img['name']=URL::to('uploads/product_images/' . $image->name);

                    $arr['images'][]= $img;

                }


                $msg="advert updated  successfully" ;
                return response()->json([

                    'key' =>true,
                    'value'=>1,
                    'msg'=>$msg,
                    'advert'=>$arr
                ] );
            
        }else{

            $msg='not allow';
            return response()->json([
                'key'=>false,
                'value'=>'0',
                'msg'=>$msg,
                'user'=>[ ]

            ]);

        }

    }

    public function delete_single_img(Request $request){


        $photo_id=$request->photo_id;
        $advert_id=$request->advert_id;

        $product_images=ProductImages::where(['id'=>$photo_id ,'product_id'=>$advert_id])
            ->first();

        //return  $product_images;

        if($product_images){

            $path = public_path().'/uploads/product_images/'.$product_images->name;
            File::delete($path);
            $product_images->delete();

            return response([
                'status' =>true
            ]);

        }else{

            return response([
                'status' =>true
            ]);

        }


    }

    public function user_adverts(Request $request)
    {
        $api_token=$request['token'];
        $user=User::where('token',$api_token)->first();

        if($user) {
            $adverts = Product::where('user_id', $user->id)
                // ->with('description')
                ->with('user')
                ->orderBy('created_at', 'desc')
                // ->with('viewCount')
                ->get();


            //  return $adverts;

            if ($adverts) {
                $data = [];
                foreach ($adverts as $advert) {
                    $arr['id'] = $advert->id;
                    $arr['title'] = $advert->title;
                    $arr['description'] = $advert->description;
                    $arr['price'] = $advert->price;
                    $arr['longitude'] = $advert->longitude;
                    $arr['latitude'] = $advert->latitude;
                    $arr['created_at'] = Carbon::parse($advert->created_at)->timestamp;
                    $arr['views'] = View::where('product_id', $advert->id)->count();

                    if ($user) {
                        $user_id = $user->id;
                        $favourite = Favourite::where(['product_id' => $advert->id, 'user_id' => $user_id])
                            ->first();
                        if ($favourite) {
                            $arr['is_favourite'] = true;
                        } else {
                            $arr['is_favourite'] = false;
                        }

                        $like = like::where(['product_id' => $advert->id, 'user_id' => $user_id])
                            ->first();

                        if ($like) {
                            $arr['is_like'] = true;
                        } else {

                            $arr['is_like'] = false;
                        }

                    } else {
                        $arr['is_favourite'] = false;
                        $arr['is_like'] = false;

                    }

                    $likecount = like::where(['product_id' => $advert->id])->get()->count();
                    $arr['likes_count'] = $likecount;


                    $arr['images'] = [];
                    foreach ($advert->images as $images) {

                        $img['id'] = $images->id;
                        $img['name'] = URL::to('uploads/product_images/' . $images->name);
                        $arr['images'][] = $img;

                    }


                    $user_id = $advert->user_id;
                    $userofadvert = User::find($user_id);

                    // return $userofadvert;

                    $arr['user']['id'] = $userofadvert->id;
                    $arr['user']['name'] = $userofadvert->name;
                    $arr['user']['phone'] = $userofadvert->phone;
                    $arr['user']['photo'] = URL::to('uploads/users_photos/' . $userofadvert->photo);
                    $arr['user']['longitude'] = $userofadvert->longitude;
                    $arr['user']['latitude'] = $userofadvert->latitude;


                    $data[] = $arr;
                }

                //  return $data;


                $page = $request->get('page', 1);
                // default number per page
                $perPage = 10;
                // offset results
                $offset = ($page * $perPage) - $perPage;


                $count = count($data);


                return new LengthAwarePaginator($data, $count, $perPage, $offset);

            } else {
                return response([
                    'status' => true,
                    'current_page' => 1,
                    'total_pages_count' => 1,
                    'data' => []
                ]);

            }

        }else{

            return response([
                'status' => false,
                'current_page' => 1,
                'total_pages_count' => 1,
                'data' => []
            ]);
        }



    }

    public function republish(Request $request){

        $advert_id=$request->advert_id;
        $token =$request->token;
        $user=User::where('token',$token)->first();
        $advert=Product::find($advert_id);

        if($user && $advert){

            $advert->created_at=Carbon::now();
            $advert->updated_at=Carbon::now();
            $advert->save();


            return response([
                'status'=>true,
                'msg'=>'republish successfully'
            ]);

        }else{

            return response([
                'status'=>false,
                'msg'=>'not allow'
            ]);
        }

    }

    public function make_special(Request $request)
    {

        $token=$request['token'];
        $advert_id =$request['advert_id'];

        $user=User::where('token',$token)->first();
        $advert=Product::find($advert_id);

        if($user && $advert){


            $advert->status='special';
            $advert->save();


            return response([
                'status'=>true,
                'msg'=>'the advert became special '
            ]);

        }else{

            return response([
                'status'=>false,
                'msg'=>'not allow'
            ]);
        }

    }
    
    
    
    public function search_department(Request $request)
    {

        $name=$request['name'] ;
        $department = $request->sub_department;

        $user_token=$request['api_token'];

        $user=User::where('token',$user_token)->first();

        $adverts=Product::where('title','like','%'.$name.'%')
            ->where('cat_id',$department)
            // ->with('description')
            ->with('user')
            // ->with('viewCount')
            ->get();


        //  return $adverts;

        if($adverts){
            $data=[];
            foreach($adverts as $advert ){
                $arr['id']=$advert->id;
                $arr['title']=$advert->title;
                $arr['description']=$advert->description;
                $arr['price']=$advert->price;
                $arr['longitude']=$advert->longitude;
                $arr['latitude']=$advert->latitude;
                $arr['created_at']=Carbon::parse($advert->created_at)->timestamp;
                $arr['views']=View::where('product_id',$advert->id)->count();

                if($user)
                {
                    $user_id=$user->id;
                    $favourite = Favourite::where(['product_id' => $advert->id, 'user_id' => $user_id])
                        ->first();
                    if($favourite){
                        $arr['is_favourite']=true;
                    }else{
                        $arr['is_favourite']=false;
                    }

                    $like=like::where(['product_id' => $advert->id, 'user_id' => $user_id])
                        ->first();

                    if($like){
                        $arr['is_like']=true;
                    }else{

                        $arr['is_like']=false;
                    }


                }else{
                    $arr['is_favourite']= false;
                    $arr['is_like']= false;

                }

                $likecount=like::where(['product_id' => $advert->id])->get()->count();
                $arr['likes_count']= $likecount;


                //$arr['images']=[];
                foreach($advert->images as $images ){

                    $img['id']=$images->id;
                    $img['name']=URL::to('uploads/product_images/' . $images->name);

                    $arr['images'][]= $img;

                }

                $arr['user']['id']=$advert->user->id;
                $arr['user']['name']=$advert->user->name;
                $arr['user']['photo']=URL::to('uploads/users_photos/' . $advert->user->photo);
                $arr['user']['longitude']=$advert->user->longitude;
                $arr['user']['latitude']=$advert->user->latitude;





                $data[]= $arr;
            }

            //  return $data;


            $page = $request->get('page', 1);
            // default number per page
            $perPage = 10;
            // offset results
            $offset = ($page * $perPage) - $perPage;


            $count = count($data) ;


            return new LengthAwarePaginator($data,  $count , $perPage  , $offset );

        }else{
            return response([
                'status' =>true ,
                'current_page' =>1,
                'total_pages_count'=>1,
                'data' => [ ]
            ]);

        }



    }




}

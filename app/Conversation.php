<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    //
    protected $fillable= [
        'sender_id','receiver_id','id'
    ];

    protected $casts=['created_at'=>'timestamp' ];

    public function sender(){
        return $this->belongsTo('App\User' , 'sender_id')
            ->select('id','name','photo');
    }

    public function receiver(){
        return $this->belongsTo('App\User' , 'receiver_id')
            ->select('id','name','photo');
    }



    public function chatReciever(){
        return $this->belongsTo('App\User' , 'receiver_id')
            ->select('id','name','photo');
    }


    public function chatSender(){
        return $this->belongsTo('App\User' , 'sender_id')
            ->select('id','name','photo');
    }



    public function message(){
        return $this->hasMany('App\Message' , 'conversation_id');

    }


    public function lastMessage()
    {
        return  $this->hasOne('App\Message', 'conversation_id')
            ->select('id','conversation_id','sender_id','receiver_id','message','created_at','is_read','type')
            ->with('sender')
            ->with('receiver')
            ->orderBy('created_at','DESC');

    }
}

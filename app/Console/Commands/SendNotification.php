<?php

namespace App\Console\Commands;

use App\Favourite;
use App\like;
use App\NotificationPhone;
use App\User;
use App\View;
use Carbon\Carbon;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

class SendNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification every 14 day to user to republish his advert';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time=Carbon::now()->subDays(14);

        $adverts=DB::table('products')->where('created_at','<',$time)
            ->get();

        foreach($adverts as $advert){

            $user_id = $advert->user_id;
            $user=User::find($user_id);
            $device_token=$user->device_token;
            $admin=DB::table('users')->where('role','admin')->first();

            $notification = new NotificationPhone();
            $notification->notifier_id = $user_id;
            $notification->notify_from = $admin->id;
            $notification->product_id= $advert->id;
            $notification->is_read = 0;
            $notification->type = 'republish';
            $notification->message =  'سيتم حذف اعلانك غدا اذا كنت لاتريد ذلك فقم باعاده نشر الأعلان  ' ;
            $notification->save();

            $notifications = NotificationPhone::where(['id' => $notification->id])
                ->with('notifyFrom')
                ->with('advert.images')
                ->first();

            //here the data should send with notification fcm.....

            $notificationData['id'] = $notification->id;
            $notificationData['notifier_id'] = $notification->notifier_id;
            $notificationData['message'] = null;

            $notificationData['notifyFrom']['id'] = $notification->notifyFrom->id;
            $notificationData['notifyFrom']['name'] = $notification->notifyFrom->name;


            if ($notification->notifyFrom->photo) {
                $notificationData['notifyFrom']['photo'] = URL::to('uploads/users_photos/' . $notification->notifyFrom->photo);
            } else {
                $notificationData['notifyFrom']['photo'] = "";
            }

            $notificationData['product']['id'] = $notification->advert->id;
            $notificationData['product']['title'] = $notification->advert->title;
            $notificationData['product']['description'] = $notification->advert->description;

            $notificationData['product']['price'] = $notification->advert->price;
            $notificationData['product']['longitude'] = $notification->advert->longitude;
            $notificationData['product']['latitude'] = $notification->advert->latitude;
            $notificationData['product']['created_at'] = Carbon::parse($notification->advert->created_at)->timestamp;
            $notificationData['product']['views'] = View::where('product_id', $notification->advert->id)->count();


            $user_id = $notification->advert->user_id;
            $favourite = Favourite::where(['product_id' => $notification->advert->id, 'user_id' => $user_id])
                ->first();
            if ($favourite) {
                $notificationData['product']['is_favourite'] = true;
            } else {
                $notificationData['product']['is_favourite'] = false;
            }

            $like = like::where(['product_id' => $notification->advert->id, 'user_id' => $user_id])
                ->first();

            if ($like) {
                $notificationData['product']['is_like'] = true;
            } else {

                $notificationData['product']['is_like'] = false;
            }


            $likecount = like::where(['product_id' => $notification->advert->id])->get()->count();


            $notificationData['product']['likes_count'] = $likecount;


            $userofadvert = User::find($user_id);

            // return $userofadvert;

            $notificationData['product']['user']['id'] = $userofadvert->id;
            $notificationData['product']['user']['name'] = $userofadvert->name;
            $notificationData['product']['user']['phone'] = $userofadvert->phone;
            $notificationData['product']['user']['photo'] = URL::to('uploads/users_photos/' . $userofadvert->photo);
            $notificationData['product']['user']['longitude'] = $userofadvert->longitude;
            $notificationData['product']['user']['latitude'] = $userofadvert->latitude;


            $notificationData['product']['images'] = [];
            foreach ($notification->advert->images as $images) {

                $img['id'] =$images->id;
                $img['name'] = URL::to('uploads/product_images/' . $images->name);

                $notificationData['product']['images'][] = $img;

            }


            $notificationD =json_encode($notificationData);

            // here we write the code to send notification through firebase.....
            if ($device_token && ($user->id != $user_id )) {

                //here we sort the data which will send through fcm.......
                $notificationD =json_encode($notificationData);

                // return  $notificationD;
                $data = [
                    'title' => 'Tagged',
                    'message' => 'سيتم حذف اعلانك غدا اذا كنت لاتريد ذلك فقم باعاده نشر الأعلان  ',
                    'type' => 'republish',
                    'data' => $notificationD
                ];

                // Sending a Downstream Message to a Device

                $optionBuilder = new OptionsBuilder();
                $optionBuilder->setTimeToLive(60 * 20);

                $notificationBuilder = new PayloadNotificationBuilder('Tagged');
                $notificationBuilder->setBody($data)
                    ->setSound('default');

                $dataBuilder = new PayloadDataBuilder();
                $dataBuilder->addData( $data);

                $option = $optionBuilder->build();
                $notification = null;
                $data = $dataBuilder->build();

                $downstreamResponse = FCM::sendTo($device_token, $option, $notification, $data);


                //this if you want to check the notification sent to device or not ....
                // $downstreamResponse->numberSuccess();
                // $downstreamResponse->numberFailure();
                //   $downstreamResponse->numberModification();

            }



        }

        }

    }


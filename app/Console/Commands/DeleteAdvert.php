<?php

namespace App\Console\Commands;

use App\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;

class DeleteAdvert extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:advert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete advert after 15 days if user didnot republish it';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $time = Carbon::now()->subDays(15);

        $adverts = Product::where('created_at', '<', $time)
            ->get();


        foreach ($adverts as $advert) {

            $advert->delete();
        }

    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About_description extends Model
{
    //
    protected $fillable = ['about_id' , 'language_id'] ;

    public function about()
    {

        return $this->belongsTo('App\About');
    }


    public function language()
    {
        return $this->belongsTo('App\Language','language_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDescription extends Model
{
    //

    public function product()
    {

        return $this->belongsTo('App\Product','product_id');

    }

    public function language()
    {

        return $this->belongsTo('App\Language','language_id');

    }
}

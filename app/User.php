<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
        'name', 'email', 'password','longitude','latitude' ,'photo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function views(){
        return $this->hasMany('App\View','addedBy_id');
    }

    public function comments(){
        return $this->hasMany('App\Comment','addedBy_id');
    }

    public function likes(){
        return $this->hasMany('App\Like','addedBy_id');
    }

    public function teamMember()
    {

        return $this->hasMany('App\TeamMember','member_id');
    }
    
     public function products(){
        return $this->hasMany('App\Product');
    }
    
    

    

}

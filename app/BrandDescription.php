<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BrandDescription extends Model
{
    //

    public function Brand(){


        return $this->hasMany('App\Brand','brand_id') ;
    }

    public function language()
    {

        return $this->belongsTo('App\Language','language_id');

    }
}

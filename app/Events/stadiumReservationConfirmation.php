<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class stadiumReservationConfirmation   implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public $stadiumReservation ;

    public $message ;

    public $username ;

    public $time ;

    public function __construct($message = null ,$username,$stadiumReservation , $time = null )
    {
        //
        // assigning campaign object inside constructor

        $this->message =$message ;
        $this->username = $username ;
        $this->stadiumReservation = $stadiumReservation ;
        $this->time = $time ;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('stadium_reservation');
    }
}

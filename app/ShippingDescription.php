<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingDescription extends Model
{
    //
    public function shipping()
    {

        return $this->belongsTo('App\Shipping');
    }


    public function language()
    {
        return $this->belongsTo('App\Language','language_id');
    }
}

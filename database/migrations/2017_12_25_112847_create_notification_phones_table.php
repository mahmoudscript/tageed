<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationPhonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_phones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('notifier_id')->unsigned();
            $table->foreign('notifier_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('notify_from')->unsigned();
            $table->foreign('notify_from')->references('id')->on('users')->onDelete('cascade');
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->tinyInteger('is_read');

            $table->enum('type',['public'])->nullale();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_phones');
    }
}

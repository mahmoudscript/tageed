<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//register &login ...

Route::post('/login', 'API\apiUserController@login');
Route::post('/register', 'API\apiUserController@register');
Route::post('/social_login','API\apiUserController@socialLogin');
Route::post('/reset_password','API\apiUserController@reset_password');
Route::post('/change_password','API\apiUserController@change_password');
Route::post('/updateDevicToken', 'API\apiUserController@updateDevicToken');
Route::post('/changeLang', 'API\apiUserController@changeLang');
Route::post('/update_profile','API\apiUserController@updateProfile');



// adverts ...
Route::get('/get_main_departments','API\apiAdvertsController@getMainDepartments');
Route::get('/get_sub_departments','API\apiAdvertsController@getSubDepartments');
Route::get('/get_adverts','API\apiAdvertsController@getadverts');
Route::post('/add_advert','API\apiAdvertsController@addAdvert');
Route::post('/search','API\apiAdvertsController@search');


//new services........
Route::post('/delete_advert','API\apiAdvertsController@delete_advert');
Route::post('/edit_advert','API\apiAdvertsController@edit_advert');
Route::post('/delete_single_img','API\apiAdvertsController@delete_single_img');
Route::post('/user_adverts','API\apiAdvertsController@user_adverts');
Route::post('/republish','API\apiAdvertsController@republish');
Route::post('/make_special','API\apiAdvertsController@make_special');
Route::post('/search_department','API\apiAdvertsController@search_department');





//about and countact us ...

Route::get('/about', 'API\apiAboutController@about');
Route::post('/contact_us', 'API\apiAboutController@contact_us');


 //views && favourite ...
Route::post('/view', 'API\apiCommentController@addView');
Route::post('/add_to_favourite', 'API\apiCommentController@addFavourite');
Route::get('/get_favourites', 'API\apiCommentController@getFavourite');

Route::post('/like','API\apiCommentController@addLike');
Route::post('/comment','API\apiCommentController@addComment');
Route::get('/get_comments','API\apiCommentController@getComments');




// chat API...................................

Route::get('/getConversations','API\apiChatController@getConversations');
Route::get('/getConversationsMessages','API\apiChatController@getConversationsMessages');
Route::post('/sendMessage','API\apiChatController@sendMessage');
Route::post('/readMessage','API\apiChatController@readMessage');
Route::post('/deleteConversation','API\apiChatController@deleteConversation');




//notifications...............
Route::get('/notifications', 'API\apiNotificationController@getAllNotifications');
Route::post('/notification/delete', 'API\apiNotificationController@deleteNotification');
Route::post('/notification/see', 'API\apiNotificationController@seeNotification');

Route::get('/notifications/count', 'API\apiNotificationController@getUnReadNotificationsCount');



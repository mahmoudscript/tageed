<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get ( '/redirect/{service}', 'Auth\RegisterController@redirect' );
Route::get ( '/callback/{service}', 'Auth\RegisterController@callback' );

Route::group(['prefix' => LaravelLocalization::setLocale() ], function()
{
    Route::post('register' , 'Auth\RegisterController@register');
    // login
    Route::post('loginUser' , 'Auth\LoginController@loginUser');
});


//-------------------------------------------------------------------//
//--------------------------ADMIN PANEL------------------------------//
//-------------------------------------------------------------------//

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware'=>['localeSessionRedirect', 'localizationRedirect', 'localeViewPath','web']],function() {
    
   Route::get('/', 'HomeController@index')->name('homepage');

   Route::get('/register', 'HomeController@register');
   Route::post('/registerform','HomeController@registerform');

   Route::get('loginpage','HomeController@loginpage');
   Route::post('loginform','HomeController@loginform');


   Route::get('/user_ads/{id}', 'HomeController@user_ads')->name('myadv');
   Route::get('/Favorits_ads', 'HomeController@Favorits_ads');
   Route::get('/category', 'HomeController@category');
   Route::get('/ad_details/{id}', 'HomeController@ad_details');

   Route::get('/advpage','HomeController@advpage');
   Route::post('/addadvert','HomeController@addadvert');

   Route::post('/contactus','HomeController@contactus');

   Route::get('/catadverts', 'HomeController@catadverts');

   Route::get('/catadvertsdetails/{id}','HomeController@catadvertsdetails');
   

   Route::post('/searchresult','HomeController@searchresult');
   


} );

  //-------------------------------------------------------------------//
 //--------------------------ADMIN PANEL------------------------------//
//-------------------------------------------------------------------//

Route::group(['prefix' => LaravelLocalization::setLocale(),'middleware'=>['localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ,'admin']],function() {

    Route::get('/adminPanel','backend\DashboardController@index')->name('adminPanel');


    Route::resource('languages','backend\LanguagesController');

    Route::resource('departments','backend\DepartmentsController');

    Route::post('add_sub/{id}','backend\DepartmentsController@addSub')->name('add_sub');
    Route::post('store_sub/{id}','backend\DepartmentsController@storeSub')->name('store_sub');

    Route::get('show_subs/{id}','backend\DepartmentsController@showSubs')->name('showSubs');

    Route::get('show_adverts/{id}','backend\DepartmentsController@showAdverts')->name('showAdverts');



    Route::resource('products','backend\ProductController');
    Route::get('products/image/{id}','backend\ProductController@deleteSingleImage');


    Route::resource('advices','backend\AdviceController');

    Route::resource('brands','backend\BrandController');

    Route::resource('agencies','backend\AgencyController');

    Route::resource('privacy','backend\PraivacyController');

    Route::resource('policy','backend\PolicyController');

    Route::resource('shipping','backend\ShippingController');

    Route::resource('replacement','backend\ReplacementController');

    Route::resource('payment','backend\PaymentController');

    Route::resource('orders','backend\OrderController');
    Route::post('orders/makeSpecial/{id}','backend\OrderController@makeSpecial')->name('make_special');


    //vistores message
    Route::resource('visitorsMessages','backend\visitorMessageController');
    Route::get('sendmessage','backend\UserController@msgAll');
    Route::get('sendEmailMessage','backend\visitorMessageController@replyMsg');
    //about
    Route::get('/about' , 'Backend\AboutController@index')->name('about') ;
    Route::put('/about/{id}' , 'Backend\AboutController@update')->name('about_update') ;
    // privacies
    Route::get('privacies','backend\PrivaciesController@index')->name('privacies');
    Route::get('/privacies/{id}' , 'backend\PrivaciesController@update')->name('privacies_update') ;
    // social media  :
    Route::resource('social-media', 'Backend\SocialMediaController');
    // users
    Route::resource('users','backend\UserController');
    Route::post('/sendMsg/{id}','backend\UserController@postMsg');



});



Auth::routes();


